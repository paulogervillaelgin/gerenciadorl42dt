/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FactoryConexaoBD {

    public static final int CONEXAO_LOCAL = 0;
    public static final int CONEXAO_CLIENTE = 1;

    public static AbstractConexaoBD getConexao(int tipo, String caminho) {
        switch (tipo) {
            case CONEXAO_LOCAL:
                return new ConexaoBDLocal("");
            case CONEXAO_CLIENTE:
                return new ConexaoBDCliente(caminho);
        }
        return null;
    }

}
