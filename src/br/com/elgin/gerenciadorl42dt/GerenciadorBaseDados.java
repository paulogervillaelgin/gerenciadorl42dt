/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GerenciadorBaseDados {

    private static ThreadServidorDados threadServidorDados;

    private static ThreadLocal<EntityManager> threadLocal;
    private static EntityManagerFactory factory;

    public static final int CONEXAO_LOCAL = 0;
    public static final int CONEXAO_CLIENTE = 1;

    public static final Path ARQUIVOLOGSERVIDOR = Paths.get("./logservidor.txt");

    public static void createEntitymanagerFactory(AbstractConexaoBD conexao) {
        Map properties = new HashMap();
        properties.put("javax.persistence.jdbc.url", conexao.getURLString());
        properties.put("javax.persistence.jdbc.driver", conexao.getDriverString());

        factory = Persistence.createEntityManagerFactory("GerenciadorL42DTPU" , properties);
        
        threadLocal = new ThreadLocal<>();
    }

    public static ThreadServidorDados getThreadServidorDados() {
        return threadServidorDados;
    }

    public static void setThreadServidorDados(ThreadServidorDados threadServidorDados) {
        GerenciadorBaseDados.threadServidorDados = threadServidorDados;
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return factory;
    }

    public static void log(String msg) {
        try {
            Files.write(ARQUIVOLOGSERVIDOR, (Utilidades.getCurrentLocalDateTimeStamp() + " : " + msg + System.lineSeparator()).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException ex) {
            Logger.getLogger(GerenciadorBaseDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que retorna uma EntityManager.
     *
     * @return
     */
    public static EntityManager getEntityManager() {
        if (threadLocal.get() == null) {
            threadLocal.set(factory.createEntityManager());
        }
        return threadLocal.get();
    }

    /**
     * Responsavel por fechar a EntityManager.
     */
    public static void closeEntityManager() {
        if (threadLocal != null) {

            EntityManager em = threadLocal.get();
            if (em != null) {
                threadLocal.remove();
                em.close();
            }
        }
    }

    public static boolean startThreadServidorDados(String dir, Integer porta) {
        setThreadServidorDados(new ThreadServidorDados(dir, porta));
        if (pingServidorDados()) {
            //se pingou antes de executar então devemos falhar
            return false;
        }
        getThreadServidorDados().start();
        return true;
    }

    public static boolean pingServidorDados() {

        if (getThreadServidorDados() != null) {
            try {
                getThreadServidorDados().ping();
                return true;
            } catch (Exception ex) {
                //Logger.getLogger(GerenciadorBaseDados.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public static AbstractConexaoBD getConexaoBD(int tipo, String caminho) {
        return FactoryConexaoBD.getConexao(tipo, caminho);
    }

}
