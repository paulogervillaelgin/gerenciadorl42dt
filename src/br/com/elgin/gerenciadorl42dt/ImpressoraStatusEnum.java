/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public enum ImpressoraStatusEnum {

    PRODUTO(1), LAYOUT(2);

    private int valorStatus;

    private ImpressoraStatusEnum(int valorStatus) {
        this.valorStatus = valorStatus;
    }

    public int getValorStatus() {
        return valorStatus;
    }

    public static int getTotalValue() {
        int res = 0;
        for (ImpressoraStatusEnum val : ImpressoraStatusEnum.values()) {
            res += val.getValorStatus();
        }
        return res;
    }

}
