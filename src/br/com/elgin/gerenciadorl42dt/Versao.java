/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Versao {

    private static final String VERSAO = "1.3.7";

    public static String getAtual() {
        return VERSAO;
    }
}

/*
    Paulo Gervilla 11/12/2019 - 1.3.5:
    - Criada opção de login automático para não pedir senha
    - Adicionada opção de Preço Total no código de barras por unidade

    Paulo Gervilla 05/02/2020 - 1.3.6:
    - Devido a problema no firmware vamos aceitar impressora sem número de série quando tiver somente uma cadastrada

    George 06/05/2020 - 1.3.7: Correção de importação arquivos Filizola
*/