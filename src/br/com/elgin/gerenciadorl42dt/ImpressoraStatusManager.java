/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ImpressoraStatusManager {

    private static final EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    private static final ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);

    public static void updateStatusImpressora(Impressora imp, ImpressoraStatusEnum campo, boolean value) {
        try {
            if (value) {
                imp.setFlagAtualizada(imp.getFlagAtualizacao() & (~campo.getValorStatus()));
            } else {
                imp.setFlagAtualizada(imp.getFlagAtualizacao() | campo.getValorStatus());
            }
            daoImpressora.edit(imp);
        } catch (Exception ex) {
            Logger.getLogger(ImpressoraStatusManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void updateStatusListaImpressora(Collection<Impressora> lista, ImpressoraStatusEnum campo, boolean valor) {
        if (lista != null) {
            lista.parallelStream().forEach((balanca) -> {
                updateStatusImpressora(balanca, campo, valor);
            });
        }
    }

    public static void updateImpressorasOf(Flexibarcode flexi, boolean atualizada) {
        Collection lista = daoImpressora.findImpressoraEntities();
        updateStatusListaImpressora(lista, ImpressoraStatusEnum.LAYOUT, atualizada);
    }

    public static void updateImpressorasOf(Produto prod, boolean atualizada) {
        Collection lista = daoImpressora.findImpressoraByProduto(prod);
        updateStatusListaImpressora(lista, ImpressoraStatusEnum.PRODUTO, atualizada);
    }

    public static void updateImpressorasOf(Produto prod, ImpressoraStatusEnum campo, boolean atualizada) {
        Collection lista = daoImpressora.findImpressoraByProduto(prod);
        updateStatusListaImpressora(lista, campo, atualizada);
    }

    public static void updateImpressorasOf(Grupoproduto grupo, boolean atualizada) {
        Collection lista = daoImpressora.findBalancaByProduto(grupo);
        updateStatusListaImpressora(lista, ImpressoraStatusEnum.PRODUTO, atualizada);
    }

    public static void updateImpressorasOf(ImpressoraStatusEnum campo, boolean atualizada) {
        Collection lista = daoImpressora.findImpressoraEntities();
        updateStatusListaImpressora(lista, campo, atualizada);
    }

}
