/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmConexaoBancoController;
import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmJanelaController;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import java.io.File;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class GerenciadorL42DT extends Application {

    private Stage stage;
    private final FXMLLoader loader = new FXMLLoader();

    private AbstractConexaoBD conexaoBD = null;

    private FrmJanelaController janela = null;
    private FrmConexaoBancoController frmConexao = null;

    @Override
    public void start(Stage stage) throws Exception {

        System.setProperty("jna.library.path", new File(".").getCanonicalPath());
        
        Configuracao.carregar();
        //* Paulo Gervilla - 1.3.5
        Configuracao.setLoginautomaticoTmp(Configuracao.isLoginAutomatico());
        //*        
        conexaoBD = GerenciadorBaseDados.getConexaoBD(GerenciadorBaseDados.CONEXAO_LOCAL, "");

        janela = FormHelper.createJanelaController(stage);
        janela.setTitulo("Gerenciador L42DT " + Versao.getAtual());
        FormHelper.setFrmPrincipal(janela);

        frmConexao = FormHelper.gotoBancoDados();

        frmConexao.setConexaoBD(conexaoBD);

        if (!Configuracao.isServico()) {
            //janela.setSize(1024, 768);
            janela.getStage().show();
            janela.maximize();
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() {
        GerenciadorBaseDados.closeEntityManager();
        Platform.exit();
        System.exit(0);
    }
}
