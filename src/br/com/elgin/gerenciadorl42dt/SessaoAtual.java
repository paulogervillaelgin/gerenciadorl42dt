/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.model.Loja;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import javafx.scene.image.Image;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public final class SessaoAtual {

    public static final Image USUARIO_IMAGEM_ADMINISTRADOR = new Image(SessaoAtual.class.getResourceAsStream("/images/administrator-32.png"));
    public static final Image USUARIO_IMAGEM_OPERADOR = new Image(SessaoAtual.class.getResourceAsStream("/images/guest-32.png"));

    private static Usuario usuario;
    private static Loja loja;

    public static Usuario getUsuario() {
        return usuario;
    }

    public static void setUsuario(Usuario user) {
        usuario = user;
    }

    public static Loja getLoja() {
        return loja;
    }

    public static void setLoja(Loja loja) {
        SessaoAtual.loja = loja;
    }
    
    
}
