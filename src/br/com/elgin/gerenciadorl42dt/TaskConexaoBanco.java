/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class TaskConexaoBanco extends Task<AbstractConexaoBD> {

    private AbstractConexaoBD conexao;

    public AbstractConexaoBD getConexao() {
        return conexao;
    }

    public void setConexao(AbstractConexaoBD conexao) {
        this.conexao = conexao;
    }

    public TaskConexaoBanco(AbstractConexaoBD valor) {
        conexao = valor;
    }

    @Override
    protected AbstractConexaoBD call() throws Exception {
        updateMessage("Iniciando conexão ...");

        //trata conexao e verifica
        int aux = 0;
        while (!conexao.verificaConexao()) {
            updateMessage("Tentativa de conexão " + ++aux + " ...");
            Thread.sleep(5000);
            if (aux == 3) {
                updateMessage("Não foi possível realizar conexão!");

                return null;
            }
        }
        // se falhar a conexao entao pede dados e verifica de novo
        // oferecer opções de redundancia

        //verificar versao
        verificaVersaoBD(conexao.getConexao());

        updateMessage("Iniciando conexão JPA com o banco de dados.");
        GerenciadorBaseDados.createEntitymanagerFactory(conexao);
        updateMessage("OK!");

        return conexao;
    }

    private void verificaVersaoBD(Connection conn) throws SQLException {
        //* PRG - 1.3.5
        ResultSet res = null;
        //*
        Statement sql = conn.createStatement();
        //* PRG - 1.3.5
        try {
        //*
            try {
                updateMessage("Verificando versão do bando de dados...");
                //* PRG - 1.3.5
                //ResultSet res = sql.executeQuery("select bancodedados from versao");
                res = sql.executeQuery("select bancodedados from versao");
                /*
                if (res.next()) {
                    // Codigo abaixo é exemplo, não vai ser utilizado na primeira versao
                    switch (res.getInt("bancodedados")) {
                        case 0:
                            sql.execute("ALTER TABLE PRODUTO ADD COLUMN IMPRIMIRVALIDADE INT DEFAULT NULL");
                            sql.execute("UPDATE PRODUTO SET IMPRIMIRVALIDADE = 1 WHERE VALIDADEDIAS > 0");
                            sql.execute("UPDATE VERSAO SET BANCODEDADOS = 1");
                            break;
                        case 1:
                            sql.execute("INSERT INTO ELGIN.FLEXIBARCODE (INDICE, DESCRICAO, TIPO, FLAG, TAMANHOCODIGO, INFO1, TAMANHOINFO1, INFO2, TAMANHOINFO2, CHECKDIGIT) VALUES (6, '4 Dígitos / Peso', 1, 0, 4, 0, 7, 0, 0, 2)");
                            sql.execute("INSERT INTO ELGIN.FLEXIBARCODE (INDICE, DESCRICAO, TIPO, FLAG, TAMANHOCODIGO, INFO1, TAMANHOINFO1, INFO2, TAMANHOINFO2, CHECKDIGIT) VALUES (7, '5 Dígitos / Peso', 1, 0, 5, 0, 6, 0, 0, 2)");
                            sql.execute("ALTER TABLE ELGIN.FLEXIBARCODE ADD COLUMN PRODUTOQTD INT DEFAULT 0");
                            sql.execute("UPDATE ELGIN.FLEXIBARCODE SET PRODUTOQTD = 1 WHERE INDICE = 4 OR INDICE = 5");
                            sql.execute("UPDATE VERSAO SET BANCODEDADOS = 2");
                            break;
                    }
                }
                */
                //*
            } catch (SQLException ex) {
                //openCarregando();

                if (ex.getSQLState().equals("42X05") || ex.getSQLState().equals("42Y07")) {
                    try {
                        updateMessage("Criando estrutura do banco de dados...");
                        executeSQL(conn, getSQLStatementsFromFile(getClass().getResourceAsStream("/script/gerenciadorL42DT.sql")));
                    } catch (Exception exx) {
                        Logger.getLogger(this.getClass()
                                .getName()).log(Level.SEVERE, null, exx);
                    }
                }
            } 
            //* PRG - 1.3.5
              /*
              finally {
                if (conn != null) {
                    conn.close();
                }
            }
            */
            //*

            //* PRG - 1.3.5
            if (res == null) {
                res = sql.executeQuery("select bancodedados from versao");
            }
            
            if (res.next()) {
                int versao = res.getInt("bancodedados");
                
                if (versao == 2) {
                    sql.execute("INSERT INTO ELGIN.FLEXIBARCODE (INDICE, DESCRICAO, TIPO, FLAG, TAMANHOCODIGO, INFO1, TAMANHOINFO1, INFO2, TAMANHOINFO2, CHECKDIGIT, PRODUTOQTD) VALUES (8, '4 Dígitos / Preço total', 1, 0, 4, 4, 7, 0, 0, 2, 1)");
                    sql.execute("INSERT INTO ELGIN.FLEXIBARCODE (INDICE, DESCRICAO, TIPO, FLAG, TAMANHOCODIGO, INFO1, TAMANHOINFO1, INFO2, TAMANHOINFO2, CHECKDIGIT, PRODUTOQTD) VALUES (9, '5 Dígitos / Preço total', 1, 0, 5, 4, 6, 0, 0, 2, 1)");
                    sql.execute("UPDATE VERSAO SET BANCODEDADOS = 3");
                    versao = 3;
                }
            }
            //*
        //* PRG - 1.3.5
        } finally {
            conn.close();
        }        
        //*
    }

    //retorna array de string com os sql's de um arquivo
    public static String[] getSQLStatementsFromFile(InputStream input)
            throws IOException {
        StringWriter buffer = new StringWriter();
        Reader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));

        ArrayList statements = new ArrayList();

        int semi_colon = 59;
        for (int read = reader.read(); read != -1; read = reader.read()) {
            if (read == 59) {
                statements.add(buffer.toString());
                buffer.getBuffer().delete(0, buffer
                        .getBuffer().length());
            } else {
                buffer.write(read);
            }
        }
        return (String[]) statements.toArray(new String[statements.size()]);
    }

    //executa uma lista de sql's na conexão citada
    public static int executeSQL(Connection conn, String[] statements)
            throws SQLException {

        int count = 0;
        conn.setAutoCommit(false);

        Statement stmt = conn.createStatement();
        for (String sql_statement : statements) {
            System.out.println(sql_statement);
            count += stmt.executeUpdate(sql_statement);

        }
        conn.commit();
        conn.setAutoCommit(true);

        return count;
    }
}
