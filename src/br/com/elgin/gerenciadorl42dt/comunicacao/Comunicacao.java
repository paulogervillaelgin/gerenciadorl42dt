/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.comunicacao;

import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Comunicacao {

    public static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private static final EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    private static final ProdutoJpaController daoProduto = new ProdutoJpaController(emf);

    public static TaskComunicacao getTaskSerial() {
        Comando cmd = GeradorComando.getComandoLeituraSerial();
        TaskComunicacao task = new TaskComunicacao();
        task.setListaComandos(Arrays.asList(cmd));
        return task;
    }

    public static TaskComunicacao getTaskPesagemUnitaria(Produto produto, int quantidade, int copias) {
        TaskComunicacao task = new TaskComunicacao();
        List<Comando> lista = new LinkedList<>();
        for (int i = 0; i < copias; i++) {
            lista.add(GeradorComando.getComandoPesagemUnitario(produto, quantidade));
        }
        task.setListaComandos(lista);
        return task;
    }

    public static TaskComunicacao getTaskTransferencia(Impressora imp, boolean apagar_produtos, boolean enviar_layouts, boolean enviar_datahora) {
        TaskComunicacao task = new TaskComunicacao();
        List<Comando> lista = new LinkedList<>();
        List<Produto> lista_prod = daoProduto.findProdutoEntitiesByDepartamento(imp.getDepartamento());
        
        //Configura BAUD da serial da impressora
        lista.add(GeradorComando.getComandoBaudRateImpressora());
        //Configura intensidade de impressão
        lista.add(GeradorComando.getComandoIntensidadeImpressao(Configuracao.getIntensidadeImpressao()));
        
        if (apagar_produtos) {
            lista.add(GeradorComando.getComandoApagarArquivos());
        }
        lista.add(GeradorComando.getComandoProdutos(lista_prod));
        if (enviar_layouts) {
            lista.addAll(GeradorComando.getComandoEtiquetas(lista_prod, imp));
        }
        if (enviar_datahora) {
            lista.add(GeradorComando.getComandoDataHora());
        }
        task.setListaComandos(lista);
        return task;
    }

}
