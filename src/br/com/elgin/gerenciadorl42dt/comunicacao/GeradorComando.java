/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.comunicacao;

import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class GeradorComando {

    private static final HashSet<String> PREFIXOS_COMANDOSIMPRESSORA = new HashSet<>();
    private static final HashSet<String> VARIAVEIS = new HashSet<>();

    static {
        PREFIXOS_COMANDOSIMPRESSORA.add("BAR");
        PREFIXOS_COMANDOSIMPRESSORA.add("TLC39");
        PREFIXOS_COMANDOSIMPRESSORA.add("BITMAP");
        PREFIXOS_COMANDOSIMPRESSORA.add("BOX");
        PREFIXOS_COMANDOSIMPRESSORA.add("ELLIPSE");
        PREFIXOS_COMANDOSIMPRESSORA.add("CODABLOCK");
        PREFIXOS_COMANDOSIMPRESSORA.add("DMATRIX");
        PREFIXOS_COMANDOSIMPRESSORA.add("MAXICODE");
        PREFIXOS_COMANDOSIMPRESSORA.add("PDF417");
        PREFIXOS_COMANDOSIMPRESSORA.add("AZTEC");
        PREFIXOS_COMANDOSIMPRESSORA.add("MPDF417");
        PREFIXOS_COMANDOSIMPRESSORA.add("PUTBMP");
        PREFIXOS_COMANDOSIMPRESSORA.add("PUTPCX");
        PREFIXOS_COMANDOSIMPRESSORA.add("QRCODE");
        PREFIXOS_COMANDOSIMPRESSORA.add("RSS");
        PREFIXOS_COMANDOSIMPRESSORA.add("REVERSE");
        PREFIXOS_COMANDOSIMPRESSORA.add("TEXT");
        PREFIXOS_COMANDOSIMPRESSORA.add("BLOCK");

        VARIAVEIS.add("<codigo>");
        VARIAVEIS.add("<descricao1>");
        VARIAVEIS.add("<descricao2>");
        VARIAVEIS.add("<descricao3>");
        VARIAVEIS.add("<validade>");
        VARIAVEIS.add("<preco>");
        VARIAVEIS.add("<tara>");
        VARIAVEIS.add("<pesoliquido>");
        VARIAVEIS.add("<precototal>");
        VARIAVEIS.add("<quantidade>");
        VARIAVEIS.add("<ean13>");
        VARIAVEIS.add("<porcao>");
        VARIAVEIS.add("<valorenergetico>");
        VARIAVEIS.add("<carboidratos>");
        VARIAVEIS.add("<proteinas>");
        VARIAVEIS.add("<gordurastotais>");
        VARIAVEIS.add("<gordurassaturadas>");
        VARIAVEIS.add("<gordurastrans>");
        VARIAVEIS.add("<fibraalimentar>");
        VARIAVEIS.add("<sodio>");
        VARIAVEIS.add("<valorenergetico_porcentagem>");
        VARIAVEIS.add("<carboidratos_porcentagem>");
        VARIAVEIS.add("<proteinas_porcentagem>");
        VARIAVEIS.add("<gordurastotais_porcentagem>");
        VARIAVEIS.add("<gordurassaturadas_porcentagem>");
        VARIAVEIS.add("<fibraalimentar_porcentagem>");
        VARIAVEIS.add("<sodio_porcentagem>");
        VARIAVEIS.add("<nomeloja>");
        VARIAVEIS.add("<imprimirvalidade>");
        VARIAVEIS.add("<continuo>");

    }
            
    public static Comando getComandoIntensidadeImpressao(int Intensidade) {
        String comando = String.format("DENSITY %s\r\n", Intensidade);
        return new Comando(comando.getBytes(), "Configurando intensidade de impressão");
    }
    
    public static Comando getComandoBaudRateImpressora() {
        // Paulo Gervilla 
        //A partir de agora (05/08/2019) o Baud Rate da impressora deverá ser alterado para 19200. Isto pode ser feito através da seguinte linha de comado:
        return new Comando(("SET COM1 19,N,8,1\r\n").getBytes(), "Configurando BAUD RATE");
    }

    public static Comando getComandoDataHora() {
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        return new Comando(("@YEAR=\"" + String.valueOf(date.getYear()).substring(2) + "\"\r\n@MONTH=\"" + date.getMonthValue() + "\"\r\n@DATE=\"" + date.getDayOfMonth() + "\"\r\n@HOUR=\"" + time.getHour() + "\"\r\n@MINUTE=\"" + time.getMinute() + "\"\r\n").getBytes(), "Enviando data e hora...");
    }

    public static ComandoLeitura getComandoLeituraSerial() {
        byte[] cmd = ("OUT GETSETTING$(\"SYSTEM\",\"INFORMATION\",\"SERIAL\")\r\n\r\n").getBytes();
        ComandoLeitura comando = new ComandoLeitura(cmd, "Lendo serial da impressora...");
        comando.setBufferLeitura(new byte[14]);
        return comando;
    }

    public static Comando getComandoApagarArquivos() {
        return new Comando("KILL \"*\"\r\nKILL F,\"*\"\r\n".getBytes(), "Apagando arquivos...");
    }

    public static Comando getComandoPesagemUnitario(Produto produto, int quantidade) {
        Comando cmd = new Comando() ;
        cmd.setDados((
                  "TP$=\""+new DecimalFormat("###0.00").format(produto.getPreco().doubleValue()*quantidade).replace(".", ",")+"\"\r\n"
                + "TPBC$=\""+new DecimalFormat("0000.00").format(produto.getPreco().doubleValue()*quantidade).replace(".", "")+"\"\r\n"
                + "UP$=\""+ new DecimalFormat("###0.00").format(produto.getPreco()).replace(".", ",")+"\"\r\n"
                + "QT$=\""+ quantidade +"\"\r\n"
                + "QTBC$=\"0"+new DecimalFormat("00000").format(quantidade)+"\"\r\n"
                + new DecimalFormat("00000").format(produto.getCodigo())+"\r\n"
                ).getBytes());
        return cmd;
    }
    
    public static Comando getComandoProdutos(List<Produto> produtos) {
        try (ByteArrayOutputStream bOutput = new ByteArrayOutputStream(); DataOutputStream out = new DataOutputStream(bOutput)) {
            if (!produtos.isEmpty()) {
                boolean first = true;
                out.write("DOWNLOAD F,\"SEARCH.BAS\"\r\n".getBytes());
                for (Produto Produto : produtos) {
                    if (first) {
                        out.write("IF PRCODE$ = ".getBytes());
                        first = false;
                    } else {
                        out.write("ELSEIF PRCODE$ = ".getBytes());
                    }
                    out.write(("\"" + new DecimalFormat("00000").format(Produto.getCodigo()) + "\" THEN\r\n").getBytes());
                    out.write(("OUT \"" + new DecimalFormat("0000.00").format(Produto.getPreco()) + ";" + (Produto.isPesado() ? "k" : "u") + "\"\r\n").getBytes());
                }
                out.write("ELSE\r\n".getBytes());
                out.write("OUT \" NG\"\r\n".getBytes());
                out.write("ENDIF\r\n".getBytes());
                out.write("EOP\r\n".getBytes());
                out.write("\r\n".getBytes());
            }
            return new Comando(bOutput.toByteArray(), "Enviando lista de produtos...");
        } catch (Exception ex) {
            Logger.getLogger(GeradorComando.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    //
    // Etiquetas
    //
    private static String getValorEan13(Produto produto) {
        Flexibarcode ean13 = produto.getCodigoBarras();

        String ret = "\"" + String.valueOf(produto.getFlag());

        ret = ret + String.format("%0" + String.valueOf(ean13.getTamanhocodigo()) + "d", produto.getCodigo());
        if (ean13.getTamanhocodigo() == 4) {
            ret = ret + "0";
        }
        ret=ret + "\"+";
        //Se a informação for "peso" então temos que formatar a informação enviada pela balança
        // e retirar o "." do peso
        if(ean13.getInfo1()==0){
            ret = ret + "\"0\"+LEFT$(NW$,INSTR(NW$,\",\")-1)+RIGHT$(NW$,LEN(NW$)-INSTR(NW$,\",\"))";
        } else {
            ret = ret + Flexibarcode.INFOS_VARIABLES.get(ean13.getInfo1());
        }
        

        return ret;
    }

    private static String getValorDaVariavel(Produto produto, Impressora impressora, String variavel) {
        String ret;

        if (variavel.matches("<ingredientes\\d*>")) {
            String straux;
            Integer linha_ingredientes;

            straux = variavel.split("<ingredientes")[1];
            straux = straux.split(">")[0];

            linha_ingredientes = Integer.valueOf(straux);

            if (produto.getIngredientes() != null && produto.getIngredientesAsList().size() >= linha_ingredientes) {
                ret = produto.getIngredientesAsList().get(linha_ingredientes - 1);
            } else {
                ret = "\"\"";
            }
        } else if (variavel.matches("<msgespecial\\d*>")) {
            String straux;
            Integer linha_msg;

            straux = variavel.split("<msgespecial")[1];
            straux = straux.split(">")[0];

            linha_msg = Integer.valueOf(straux);
            if (impressora.isDefinirMsgEspecial() == true && impressora.getMsgEspecial() != null && impressora.getMsgEspecialAsList().size() >= linha_msg) {
                ret = impressora.getMsgEspecialAsList().get(linha_msg - 1);
            } else if (produto.getMsgEspecial() != null && produto.getMsgEspecialAsList().size() >= linha_msg) {
                ret = produto.getMsgEspecialAsList().get(linha_msg - 1);
            } else {
                ret = "\"\"";
            }

        } else {
            switch (variavel) {
                case "<codigo>":
                    ret = String.valueOf(produto.getCodigo());
                    break;
                case "<ean13>":
                    ret = getValorEan13(produto);
                    break;
                case "<descricao1>":
                    ret = produto.getDescricao();
                    break;
                case "<descricao2>":
                    ret = produto.getDescricao2();
                    break;
                /*
                case "<descricao3>":
                    ret = produto.getDescricao3();
                    break;
                 */
                case "<validade>":
                    ret = String.valueOf(produto.getValidadeDias());
                    break;
                case "<preco>":
                    ret = "UP$";
                    break;
                case "<tara>":
                    ret = "TA$";
                    break;
                case "<pesoliquido>":
                    ret = "NW$";
                    break;
                case "<precototal>":
                    ret = "TP$";
                    break;
                case "<quantidade>":
                    ret = "QT$";
                    break;
                case "<porcao>":
                    ret = produto.getPorcao() != null ? produto.getPorcao() : "";
                    break;
                case "<valorenergetico>":
                    ret = String.valueOf(produto.getCalorias()) + " kcal";
                    break;
                case "<carboidratos>":
                    ret = String.valueOf(produto.getCarboidratos()) + " g";
                    break;
                case "<proteinas>":
                    ret = String.valueOf(produto.getProteinas()) + " g";
                    break;
                case "<gordurastotais>":
                    ret = String.valueOf(produto.getGordurastotais()) + " g";
                    break;
                case "<gordurassaturadas>":
                    ret = String.valueOf(produto.getGordurassaturadas()) + " g";
                    break;
                case "<gordurastrans>":
                    ret = String.valueOf(produto.getGordurastrans()) + " g";
                    break;
                case "<fibraalimentar>":
                    ret = String.valueOf(produto.getFibraalimentar()) + " g";
                    break;
                case "<sodio>":
                    ret = String.valueOf(produto.getSodio()) + " mg";
                    break;
                case "<valorenergetico_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getCalorias() / 2000d) * 100)) + "%";
                    break;
                case "<carboidratos_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getCarboidratos().floatValue() / 300d) * 100)) + "%";
                    break;
                case "<proteinas_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getProteinas().floatValue() / 75d) * 100)) + "%";
                    break;
                case "<gordurastotais_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getGordurastotais().floatValue() / 55d) * 100)) + "%";
                    break;
                case "<gordurassaturadas_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getGordurassaturadas().floatValue() / 22d) * 100)) + "%";
                    break;
                case "<fibraalimentar_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getFibraalimentar().floatValue() / 25d) * 100)) + "%";
                    break;
                case "<sodio_porcentagem>":
                    ret = String.valueOf(Math.round((produto.getSodio().floatValue() / 2400d) * 100)) + "%";
                    break;
                case "<nomeloja>":
                    ret = SessaoAtual.getLoja().getDescricaoetiqueta();
                    break;
                case "<imprimirvalidade>":
                    ret = produto.isImprimirValidade()?"1":"0";
                    break;
                case "<continuo>":
                    ret = Configuracao.isModoContinuo()?"1":"0";
                    break;
                default:
                    ret = "\"\"";
            }
        }
        return ret;
    }

    private static String SubstituiVariaveis(String linha, Produto produto, Impressora impressora) {

        String variavel_aux, variavel_mask, linhaaux;
        int iaux = 0;

        linhaaux = linha;

        try {

            for (String variavel : VARIAVEIS) {
                while (linhaaux.contains(variavel)) {
                    linhaaux = linhaaux.replace(variavel, getValorDaVariavel(produto, impressora, variavel));
                }

                //Procurar por <ingredientesXXX> e substituir os valores das linhas
                //Isto foi feito para não precisar adicionar uma variável para cada linha de ingrediente
                //em VARIAVEIS e consequentemente não precisar recompilar o programa a cada nova linha adicionada
                variavel_aux = "<ingredientes";
                variavel_mask = variavel_aux + "\\d*>";

                while (iaux <= linhaaux.length() && linhaaux.substring(iaux).contains(variavel_aux)) {
                    iaux = linhaaux.indexOf(variavel_aux);

                    variavel_aux = linhaaux.substring(linhaaux.indexOf(variavel_aux));

                    if (variavel_aux.contains(">")) {
                        iaux = iaux + variavel_aux.indexOf(">");

                        variavel_aux = variavel_aux.substring(variavel_aux.indexOf(variavel_aux), variavel_aux.indexOf(">") + 1);

                        if (variavel_aux.matches(variavel_mask)) {
                            linhaaux = linhaaux.replace(variavel_aux, getValorDaVariavel(produto, impressora, variavel_aux));
                        }
                    } else {
                        iaux = iaux + variavel_aux.length();
                    }
                }

                //Procurar por <msgespecialXXX> e substituir os valores das linhas
                //Isto foi feito para não precisar adicionar uma variável para cada linha de mensagem especial
                //em VARIAVEIS e consequentemente não precisar recompilar o programa a cada nova linha adicionada
                variavel_aux = "<msgespecial";
                variavel_mask = variavel_aux + "\\d*>";

                while (iaux <= linhaaux.length() && linhaaux.substring(iaux).contains(variavel_aux)) {
                    iaux = linhaaux.indexOf(variavel_aux);

                    variavel_aux = linhaaux.substring(linhaaux.indexOf(variavel_aux));

                    if (variavel_aux.contains(">")) {
                        iaux = iaux + variavel_aux.indexOf(">");

                        variavel_aux = variavel_aux.substring(variavel_aux.indexOf(variavel_aux), variavel_aux.indexOf(">") + 1);

                        if (variavel_aux.matches(variavel_mask)) {
                            linhaaux = linhaaux.replace(variavel_aux, getValorDaVariavel(produto, impressora, variavel_aux));
                        }
                    } else {
                        iaux = iaux + variavel_aux.length();
                    }
                }

            }

            linhaaux = linhaaux + "\r\n";
        } catch (Exception ex) {
            System.out.println(linha);
            Logger.getLogger(GeradorComando.class.getName()).log(Level.SEVERE, null, ex);
        }

        return linhaaux;
    }

    public static List<Comando> getComandoEtiquetas(List<Produto> produtos, Impressora impressora) {

        List<Comando> ret = new ArrayList<>();

        produtos.forEach((produto) -> {
            StringBuffer listaux = new StringBuffer();
            //System.out.println(produto.getIndice());
            FormatoEtiqueta etiqueta;

            if(produto.isPesado()){
                 etiqueta = impressora.isDefinirEtiqueta() ? impressora.getEtiquetaPesado() : produto.getEtiqueta();
            } else {
                 etiqueta = impressora.isDefinirEtiqueta() ? impressora.getEtiquetaUnitario() : produto.getEtiqueta();
            }
            
            if (etiqueta != null) {

                listaux.append("DOWNLOAD F,\"").append(new DecimalFormat("00000").format(produto.getCodigo())).append(".BAS\"\r\n");

                etiqueta.getConteudoAsList().forEach(linha -> {
                    listaux.append(SubstituiVariaveis(linha, produto, impressora))/*.append("\r\n")*/;
                });

                listaux.append("EOP\r\n");
            }

            ret.add(new Comando(String.valueOf(listaux).getBytes(Charset.forName("ISO-8859-1")), "Transferindo etiqueta produto " + produto.getCodigo()));

            //* Salvar os arquivos processados caso queira investigar posteriormente
            //try {
            //    Files.write(Paths.get("./layouts/out/" + String.valueOf(produto.getCodigo()) + ".out"), String.join("", listaux).getBytes());
            //} catch (IOException ex) {
            //    Logger.getLogger(GeradorComando.class.getName()).log(Level.SEVERE, null, ex);
            //}
            //*
        });

        return ret;
    }

}
