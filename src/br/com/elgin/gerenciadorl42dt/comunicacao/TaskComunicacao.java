/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.comunicacao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class TaskComunicacao extends Task<Boolean> {

    private static final byte[] VID = "2D84".getBytes();
    private static final byte[] PID = "7589".getBytes();

    private static int usb_handle = 0;

    private List<Comando> listaComandos = null;

    public List<Comando> getListaComandos() {
        return listaComandos;
    }

    public void setListaComandos(List<Comando> listaComandos) {
        this.listaComandos = listaComandos;
    }

    public boolean conectar() {
        try {
            if (usb_handle != 1) {
                usb_handle = JsPrinterDLL.INSTANCE.OpenUsb(VID, PID);
                System.out.println("usb handle:" + usb_handle);
            }
            return (usb_handle == 1);
        } catch (Exception ex) {
            usb_handle = -15;
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void desconectar() {
        JsPrinterDLL.INSTANCE.CloseUsb(usb_handle);
        usb_handle = -15;
    }

    public boolean enviar(byte[] buffer) {
        int ret = JsPrinterDLL.INSTANCE.WriteUsb(usb_handle, buffer, buffer.length);
        return ret == buffer.length;
    }

    public void receber(byte[] buffer) {
        JsPrinterDLL.INSTANCE.ReadUsb(usb_handle, buffer, buffer.length);
    }

    @Override
    protected Boolean call() throws Exception {
        if (listaComandos != null && listaComandos.size() > 0) {
            try {
                //desconectar();
                //Thread.sleep(500);
                int tentativa = 1;
                updateMessage("Tentando conectar ...");
                while (!conectar()) {
                    updateMessage("Tentando conectar ...(" + tentativa + "/5)");
                    Thread.sleep(1500);
                    if (++tentativa > 5) {
                        updateMessage("Erro ao conectar");
                        updateProgress(1, 1);
                        return false;
                    }
                }
                if (conectar()) {

                    int contador = 0;

                    for (Comando comando : listaComandos) {

                        //System.out.println("conectou");
                        updateMessage(comando.getMensagem() + " (" + ++contador + "/" + listaComandos.size() + ")");
                        if (enviar(comando.getDados())) {
                            if (comando instanceof ComandoLeitura) {

                                //System.out.println("enviou");
                                byte[] retorno = ((ComandoLeitura) comando).getBufferLeitura();
                                receber(retorno);
                                ((ComandoLeitura) comando).setBufferLeitura(retorno);

                                System.out.println(new String(retorno));
                            }
                        } else {
                            desconectar();
                            updateMessage("Erro ao enviar");
                            //updateProgress(1, 1);
                            return false;
                        }
                        updateProgress(contador, listaComandos.size());
                    }
                    updateMessage("Concluído!");
                    updateProgress(1, 1);
                    return true;
                } else {
                    updateMessage("Erro ao conectar");
                    desconectar();
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
}
