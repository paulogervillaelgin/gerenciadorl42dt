/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.comunicacao;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ComandoLeitura extends Comando {
    
    private byte[] bufferLeitura;

    public byte[] getBufferLeitura() {
        return bufferLeitura;
    }

    public void setBufferLeitura(byte[] bufferLeitura) {
        this.bufferLeitura = bufferLeitura;
    }
    
    public ComandoLeitura(byte[] dados, String mensagem) {
        super(dados,mensagem);        
    }
    
}
