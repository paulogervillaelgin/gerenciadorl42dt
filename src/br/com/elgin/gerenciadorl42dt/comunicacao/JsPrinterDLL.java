package br.com.elgin.gerenciadorl42dt.comunicacao;

import com.sun.jna.Library;
import com.sun.jna.Native;

public interface JsPrinterDLL extends Library {

    public static JsPrinterDLL INSTANCE = (JsPrinterDLL) Native.loadLibrary("JsPrinterDll", JsPrinterDLL.class);

    int OpenUsb(byte[] vid, byte[] pid);

    int WriteUsb(int fs, byte[] SendBuf, int SendBufSize);

    int ReadUsb(int fs, byte[] ReadBuf, int ReadBufSize);

    boolean CloseUsb(int fs);
}
