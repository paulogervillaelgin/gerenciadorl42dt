/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.DAO;

import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ProdutoJpaController implements Serializable {

    public ProdutoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Produto produto) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(produto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Produto produto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            produto = em.merge(produto);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = produto.getIndice();
                if (findProduto(id) == null) {
                    throw new NonexistentEntityException("The produto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Produto produto;
            try {
                produto = em.getReference(Produto.class, id);
                produto.getIndice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The produto with id " + id + " no longer exists.", enfe);
            }
            em.remove(produto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Produto> findProdutoEntities() {
        return findProdutoEntities(true, -1, -1);
    }

    public List<Produto> findProdutoEntities(int maxResults, int firstResult) {
        return findProdutoEntities(false, maxResults, firstResult);
    }

    private List<Produto> findProdutoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Produto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Map<Integer, Integer> getMapOfIndiceCodigo() {
        Map<Integer, Integer> mapa = new TreeMap<>();
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Tuple> cq = cb.createTupleQuery();
            // write the Root, Path elements as usual
            Root<Produto> root = cq.from(Produto.class);
            cq.multiselect(root.get("indice"), root.get("codigo"));  //using metamodel
            List<Tuple> tupleResult = em.createQuery(cq).getResultList();
            tupleResult.forEach((t) -> {
                mapa.put((int) t.get(1), (int) t.get(0));
            });
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return mapa;
    }

    public void updateEtiquetaPesado(FormatoEtiqueta etiqueta) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();

            Query query = em.createNamedQuery("Produto.updateEtiquetaPesado");
            query.setParameter("etiqueta", etiqueta);
            query.executeUpdate();

            em.getTransaction().commit();

        } finally {
            em.close();
        }
    }

    public void updateEtiquetaUnitario(FormatoEtiqueta etiqueta) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();

            Query query = em.createNamedQuery("Produto.updateEtiquetaUnitario");
            query.setParameter("etiqueta", etiqueta);
            query.executeUpdate();

            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void updateCodigoBarra(Flexibarcode codigo, Integer flag, boolean pesado) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();

            Query query = em.createNamedQuery("Produto.updateCodigoBarras");
            query.setParameter("codigobarra", codigo);
            query.setParameter("flag", flag);
            query.setParameter("pesado", pesado ? 1 : 0);
            query.executeUpdate();

            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public List<Produto> findProdutoEntitiesByDepartamento(Departamento depto) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Produto> query
                    = em.createNamedQuery("Produto.findByDepartamento", Produto.class);
            query.setParameter("departamento", depto.getIndice());
            return query.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Produto> findProdutoUnitariosEntitiesByDepartamento(Departamento depto) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Produto> query
                    = em.createNamedQuery("Produto.findByUnitariosDepartamento", Produto.class);
            query.setParameter("departamento", depto.getIndice());
            return query.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    public int countProdutoEntitiesByDepartamento(Departamento depto) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Long> query
                    = em.createNamedQuery("Produto.countByDepartamento", Long.class);
            query.setParameter("departamento", depto.getIndice());
            return (query.getSingleResult()).intValue();
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    public Produto findProdutoByCodigo(Integer codigo) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Produto> query
                    = em.createNamedQuery("Produto.findByCodigo", Produto.class);

            query.setParameter("codigo", codigo);
            return query.getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Produto findProduto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Produto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProdutoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Produto> rt = cq.from(Produto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
