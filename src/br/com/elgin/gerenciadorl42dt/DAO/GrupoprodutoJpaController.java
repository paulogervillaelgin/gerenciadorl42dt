/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.DAO;

import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class GrupoprodutoJpaController implements Serializable {

    public GrupoprodutoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Grupoproduto grupoproduto) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(grupoproduto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Grupoproduto grupoproduto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            grupoproduto = em.merge(grupoproduto);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = grupoproduto.getIndice();
                if (findGrupoproduto(id) == null) {
                    throw new NonexistentEntityException("The grupoproduto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Grupoproduto grupoproduto;
            try {
                grupoproduto = em.getReference(Grupoproduto.class, id);
                grupoproduto.getIndice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The grupoproduto with id " + id + " no longer exists.", enfe);
            }
            em.remove(grupoproduto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Grupoproduto> findGrupoprodutoEntities() {
        return findGrupoprodutoEntities(true, -1, -1);
    }

    public List<Grupoproduto> findGrupoprodutoEntities(int maxResults, int firstResult) {
        return findGrupoprodutoEntities(false, maxResults, firstResult);
    }

    private List<Grupoproduto> findGrupoprodutoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Grupoproduto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Grupoproduto findGrupoproduto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Grupoproduto.class, id);
        } finally {
            em.close();
        }
    }

    public List<Grupoproduto> findGrupoprodutoEntitiesByDescricao(Grupoproduto grupo) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Grupoproduto> query
                    = em.createNamedQuery("Grupoproduto.findByDescricao", Grupoproduto.class);
            query.setParameter("descricao", grupo.getDescricao());
            return query.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Grupoproduto findGrupoprodutoEntitiesByPadrao() {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Grupoproduto> query
                    = em.createNamedQuery("Grupoproduto.findByPadrao", Grupoproduto.class);
            query.setParameter("padrao", 1);
            return query.getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public int getGrupoprodutoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Grupoproduto> rt = cq.from(Grupoproduto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getGrupoprodutoProdutoCount(Integer id) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Produto> rt = cq.from(Produto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            cq.where(cb.equal(rt.get("grupoproduto"), id));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
