/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.DAO;

import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.PreexistingEntityException;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FlexibarcodeJpaController implements Serializable {

    public FlexibarcodeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Flexibarcode flexibarcode) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(flexibarcode);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFlexibarcode(flexibarcode.getIndice()) != null) {
                throw new PreexistingEntityException("Flexibarcode " + flexibarcode + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Flexibarcode flexibarcode) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            flexibarcode = em.merge(flexibarcode);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = flexibarcode.getIndice();
                if (findFlexibarcode(id) == null) {
                    throw new NonexistentEntityException("The flexibarcode with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Flexibarcode flexibarcode;
            try {
                flexibarcode = em.getReference(Flexibarcode.class, id);
                flexibarcode.getIndice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The flexibarcode with id " + id + " no longer exists.", enfe);
            }
            em.remove(flexibarcode);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    
    public List<Flexibarcode> findFlexibarcodeByProdutoQtd(boolean isQtd) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Flexibarcode> query
                    = em.createNamedQuery("Flexibarcode.findByProdutoQtd", Flexibarcode.class);
            query.setParameter("produtoqtd", isQtd?1:0);
            return query.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public List<Flexibarcode> findFlexibarcodeEntities() {
        return findFlexibarcodeEntities(true, -1, -1);
    }

    public List<Flexibarcode> findFlexibarcodeEntities(int maxResults, int firstResult) {
        return findFlexibarcodeEntities(false, maxResults, firstResult);
    }

    private List<Flexibarcode> findFlexibarcodeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Flexibarcode.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Flexibarcode findFlexibarcode(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Flexibarcode.class, id);
        } finally {
            em.close();
        }
    }

    public int getFlexibarcodeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Flexibarcode> rt = cq.from(Flexibarcode.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
