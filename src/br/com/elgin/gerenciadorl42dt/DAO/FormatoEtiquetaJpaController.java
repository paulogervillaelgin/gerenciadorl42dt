/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.DAO;

import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.PreexistingEntityException;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FormatoEtiquetaJpaController implements Serializable {

    public FormatoEtiquetaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(FormatoEtiqueta formatoEtiqueta) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(formatoEtiqueta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFormatoEtiqueta(formatoEtiqueta.getIndice()) != null) {
                throw new PreexistingEntityException("FormatoEtiqueta " + formatoEtiqueta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(FormatoEtiqueta formatoEtiqueta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            formatoEtiqueta = em.merge(formatoEtiqueta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = formatoEtiqueta.getIndice();
                if (findFormatoEtiqueta(id) == null) {
                    throw new NonexistentEntityException("The formatoEtiqueta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FormatoEtiqueta formatoEtiqueta;
            try {
                formatoEtiqueta = em.getReference(FormatoEtiqueta.class, id);
                formatoEtiqueta.getIndice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The formatoEtiqueta with id " + id + " no longer exists.", enfe);
            }
            em.remove(formatoEtiqueta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<FormatoEtiqueta> findFormatoEtiquetaEntities() {
        return findFormatoEtiquetaEntities(true, -1, -1);
    }

    public List<FormatoEtiqueta> findFormatoEtiquetaEntities(int maxResults, int firstResult) {
        return findFormatoEtiquetaEntities(false, maxResults, firstResult);
    }

    private List<FormatoEtiqueta> findFormatoEtiquetaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FormatoEtiqueta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FormatoEtiqueta findFormatoEtiqueta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FormatoEtiqueta.class, id);
        } finally {
            em.close();
        }
    }

    public List<FormatoEtiqueta> findFormatoEtiquetaByTipo(Integer tipo) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<FormatoEtiqueta> query
                    = em.createNamedQuery("FormatoEtiqueta.findByTipo", FormatoEtiqueta.class);
            query.setParameter("tipo", tipo);
            return query.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public int getFormatoEtiquetaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FormatoEtiqueta> rt = cq.from(FormatoEtiqueta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
