/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.DAO;

import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ImpressoraJpaController implements Serializable {

    public ImpressoraJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Impressora impressora) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(impressora);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Impressora impressora) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            impressora = em.merge(impressora);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = impressora.getIndice();
                if (findImpressora(id) == null) {
                    throw new NonexistentEntityException("The impressora with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Impressora impressora;
            try {
                impressora = em.getReference(Impressora.class, id);
                impressora.getIndice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The impressora with id " + id + " no longer exists.", enfe);
            }
            em.remove(impressora);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Impressora> findImpressoraEntities() {
        return findImpressoraEntities(true, -1, -1);
    }

    public List<Impressora> findImpressoraEntities(int maxResults, int firstResult) {
        return findImpressoraEntities(false, maxResults, firstResult);
    }

    private List<Impressora> findImpressoraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Impressora.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Impressora findImpressora(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Impressora.class, id);
        } finally {
            em.close();
        }
    }

    public List<Impressora> findImpressoraByProduto(Produto produto) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Impressora> query
                    = em.createNamedQuery("Impressora.findByProduto", Impressora.class);
            query.setParameter("grupo", produto.getGrupoproduto());
            return query.getResultList();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Impressora findImpressoraBySerial(String serial) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Impressora> query
                    = em.createNamedQuery("Impressora.findBySerial", Impressora.class);
            query.setParameter("numserial", serial);
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Impressora> findBalancaByProduto(Grupoproduto grupo) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Impressora> query
                    = em.createNamedQuery("Impressora.findByProduto", Impressora.class);
            query.setParameter("grupo", grupo);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public int getImpressoraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Impressora> rt = cq.from(Impressora.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
