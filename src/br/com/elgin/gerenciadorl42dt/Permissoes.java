/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.model.Usuario;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Permissoes {
    
    //TODO criar enums aqui para cada formulario
    
    public static boolean verificarAdm(/*inserir aqui valor do tipo enum a ser verificado*/){
        /*verificar aqui se o usuario tem permissao do enum repassado como parametro*/
        return SessaoAtual.getUsuario().getAdm() == Usuario.USUARIO_TIPO_ADM;
    }
    
}
