/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public abstract class AbstractConexaoBD {

    public abstract String getDriverString();

    public abstract String getURLString();

    private Connection conexao;
    protected String caminho;

    public AbstractConexaoBD(String _caminho) {
        caminho = _caminho;
    }

    public Connection getConexao() {
        return conexao;
    }

    public boolean verificaConexao() {

        try {
            Class.forName(getDriverString());
            Properties prop = new Properties();
            prop.setProperty("derby.ui.charset", "utf-8");
            conexao = DriverManager.getConnection(getURLString(), prop);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

}
