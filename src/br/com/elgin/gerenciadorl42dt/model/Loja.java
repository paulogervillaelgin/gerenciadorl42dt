/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "LOJA")
@NamedQueries({
    @NamedQuery(name = "Loja.findAll", query = "SELECT l FROM Loja l")
    , @NamedQuery(name = "Loja.findByIndice", query = "SELECT l FROM Loja l WHERE l.indice = :indice")
    , @NamedQuery(name = "Loja.findByDescricao", query = "SELECT l FROM Loja l WHERE l.descricao = :descricao")
    , @NamedQuery(name = "Loja.findByFlagUnitario", query = "SELECT l FROM Loja l WHERE l.flagUnitario = :flagUnitario")
    , @NamedQuery(name = "Loja.findByFlagPesado", query = "SELECT l FROM Loja l WHERE l.flagPesado = :flagPesado")
    , @NamedQuery(name = "Loja.findByDescricaoetiqueta", query = "SELECT l FROM Loja l WHERE l.descricaoetiqueta = :descricaoetiqueta")
    , @NamedQuery(name = "Loja.findByDataImportacao", query = "SELECT l FROM Loja l WHERE l.dataImportacao = :dataImportacao")
    , @NamedQuery(name = "Loja.findByDataTransmissao", query = "SELECT l FROM Loja l WHERE l.dataTransmissao = :dataTransmissao")})
public class Loja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "FLAG_UNITARIO")
    private Integer flagUnitario;
    @Column(name = "FLAG_PESADO")
    private Integer flagPesado;
    @Column(name = "DESCRICAOETIQUETA")
    private String descricaoetiqueta;
    @Column(name = "DATA_IMPORTACAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataImportacao;
    @Column(name = "DATA_TRANSMISSAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataTransmissao;
    @JoinColumn(name = "CODIGOBARRAS_PESADO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private Flexibarcode codigobarrasPesado;
    @JoinColumn(name = "CODIGOBARRAS_UNITARIO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private Flexibarcode codigobarrasUnitario;
    @JoinColumn(name = "ETIQUETA_PESADO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private FormatoEtiqueta etiquetaPesado;
    @JoinColumn(name = "ETIQUETA_UNITARIO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private FormatoEtiqueta etiquetaUnitario;

    public Loja() {
    }

    public Loja(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getFlagUnitario() {
        return flagUnitario;
    }

    public void setFlagUnitario(Integer flagUnitario) {
        this.flagUnitario = flagUnitario;
    }

    public Integer getFlagPesado() {
        return flagPesado;
    }

    public void setFlagPesado(Integer flagPesado) {
        this.flagPesado = flagPesado;
    }

    public String getDescricaoetiqueta() {
        return descricaoetiqueta;
    }

    public void setDescricaoetiqueta(String descricaoetiqueta) {
        this.descricaoetiqueta = descricaoetiqueta;
    }

    public Date getDataImportacao() {
        return dataImportacao;
    }

    public void setDataImportacao(Date dataImportacao) {
        this.dataImportacao = dataImportacao;
    }

    public Date getDataTransmissao() {
        return dataTransmissao;
    }

    public void setDataTransmissao(Date dataTransmissao) {
        this.dataTransmissao = dataTransmissao;
    }

    public Flexibarcode getCodigobarrasPesado() {
        return codigobarrasPesado;
    }

    public void setCodigobarrasPesado(Flexibarcode codigobarrasPesado) {
        this.codigobarrasPesado = codigobarrasPesado;
    }

    public Flexibarcode getCodigobarrasUnitario() {
        return codigobarrasUnitario;
    }

    public void setCodigobarrasUnitario(Flexibarcode codigobarrasUnitario) {
        this.codigobarrasUnitario = codigobarrasUnitario;
    }

    public FormatoEtiqueta getEtiquetaPesado() {
        return etiquetaPesado;
    }

    public void setEtiquetaPesado(FormatoEtiqueta etiquetaPesado) {
        this.etiquetaPesado = etiquetaPesado;
    }

    public FormatoEtiqueta getEtiquetaUnitario() {
        return etiquetaUnitario;
    }

    public void setEtiquetaUnitario(FormatoEtiqueta etiquetaUnitario) {
        this.etiquetaUnitario = etiquetaUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loja)) {
            return false;
        }
        Loja other = (Loja) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Loja[ indice=" + indice + " ]";
    }
    
}
