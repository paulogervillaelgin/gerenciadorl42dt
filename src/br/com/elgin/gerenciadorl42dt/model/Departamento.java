/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "DEPARTAMENTO")
@NamedQueries({
    @NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d")
    , @NamedQuery(name = "Departamento.findByIndice", query = "SELECT d FROM Departamento d WHERE d.indice = :indice")
    , @NamedQuery(name = "Departamento.findByDescricao", query = "SELECT d FROM Departamento d WHERE d.descricao = :descricao")})
public class Departamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;

    @Column(name = "PADRAO")
    private Integer padrao;

    @Column(name = "DESCRICAO")
    private String descricao;

    @JoinTable(name = "DEPARTAMENTOGRUPOPRODUTO", joinColumns = {
        @JoinColumn(name = "DEPARTAMENTO", referencedColumnName = "INDICE")}, inverseJoinColumns = {
        @JoinColumn(name = "GRUPOPRODUTO", referencedColumnName = "INDICE")})
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Grupoproduto> grupoprodutoSet;

    @OneToMany(mappedBy = "departamento", fetch = FetchType.EAGER)
    private Set<Impressora> impressoraSet;

    public Departamento() {
    }

    public Departamento(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean isPadrao() {
        return padrao != null && padrao == 1;
    }

    public void setPadrao(Boolean padrao) {
        this.padrao = padrao ? 1 : 0;
    }

    public Set<Grupoproduto> getGrupoprodutoSet() {
        return grupoprodutoSet;
    }

    public void setGrupoprodutoSet(Set<Grupoproduto> grupoprodutoSet) {
        this.grupoprodutoSet = grupoprodutoSet;
    }

    public Set<Impressora> getImpressoraSet() {
        return impressoraSet;
    }

    public void setImpressoraSet(Set<Impressora> impressoraSet) {
        this.impressoraSet = impressoraSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Departamento)) {
            return false;
        }
        Departamento other = (Departamento) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Departamento[ indice=" + indice + " ]";
    }

}
