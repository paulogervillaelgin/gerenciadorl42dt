/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "GRUPOPRODUTO")
@NamedQueries({
    @NamedQuery(name = "Grupoproduto.findAll", query = "SELECT g FROM Grupoproduto g")
    , @NamedQuery(name = "Grupoproduto.findByIndice", query = "SELECT g FROM Grupoproduto g WHERE g.indice = :indice")
    , @NamedQuery(name = "Grupoproduto.findByDescricao", query = "SELECT g FROM Grupoproduto g WHERE g.descricao = :descricao")
    , @NamedQuery(name = "Grupoproduto.findByPadrao", query = "SELECT g FROM Grupoproduto g WHERE g.padrao = :padrao")})
public class Grupoproduto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "PADRAO")
    private Integer padrao;

    public Grupoproduto() {
    }

    public Grupoproduto(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean isPadrao() {
        return padrao!=null&&padrao == 1;
    }

    public void setPadrao(Boolean padrao) {
        this.padrao = padrao ? 1 : 0;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupoproduto)) {
            return false;
        }
        Grupoproduto other = (Grupoproduto) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Grupoproduto[ indice=" + indice + " ]";
    }

    
}
