/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "IMPRESSORA")
@NamedQueries({
    @NamedQuery(name = "Impressora.findAll", query = "SELECT i FROM Impressora i")
    , @NamedQuery(name = "Impressora.findByIndice", query = "SELECT i FROM Impressora i WHERE i.indice = :indice")
    , @NamedQuery(name = "Impressora.findByDescricao", query = "SELECT i FROM Impressora i WHERE i.descricao = :descricao")
    , @NamedQuery(name = "Impressora.findByProduto", query = "SELECT i FROM Impressora i WHERE :grupo MEMBER OF i.departamento.grupoprodutoSet")
    , @NamedQuery(name = "Impressora.findByGrupoproduto", query = "SELECT i FROM Impressora i WHERE :grupo MEMBER OF i.departamento.grupoprodutoSet")
    , @NamedQuery(name = "Impressora.findBySerial", query = "SELECT i FROM Impressora i WHERE i.serial = :numserial")})
public class Impressora implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "SERIAL")
    private String serial;
    @Column(name = "ATUALIZADA")
    private Integer atualizada;
    @Column(name = "MSGESPECIAL")
    private String msgEspecial;
    @Column(name = "DEFINIRETIQUETA")
    private Integer definirEtiqueta;
    @Column(name = "DEFINIRMSGESPECIAL")
    private Integer definirMsgEspecial;
    @JoinColumn(name = "DEPARTAMENTO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private Departamento departamento;

    @JoinColumn(name = "ETIQUETA_PESADO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private FormatoEtiqueta etiquetaPesado;

    @JoinColumn(name = "ETIQUETA_UNITARIO", referencedColumnName = "INDICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private FormatoEtiqueta etiquetaUnitario;

    public Impressora() {
    }

    public Impressora(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean isAtualizada() {
        return atualizada != null & atualizada == 0;
    }

    //TODO função não usada e provavelmente errada
    public void setAtualizada(boolean atualizada) {
        this.atualizada = atualizada ? 1 : 0;
    }

    public int getFlagAtualizacao() {
        return atualizada;
    }

    public void setFlagAtualizada(int valor) {
        atualizada = valor;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public FormatoEtiqueta getEtiquetaPesado() {
        return etiquetaPesado;
    }

    public void setEtiquetaPesado(FormatoEtiqueta etiquetaPesado) {
        this.etiquetaPesado = etiquetaPesado;
    }

    public FormatoEtiqueta getEtiquetaUnitario() {
        return etiquetaUnitario;
    }

    public void setEtiquetaUnitario(FormatoEtiqueta etiquetaUnitario) {
        this.etiquetaUnitario = etiquetaUnitario;
    }

    public String getMsgEspecial() {
        return msgEspecial;
    }

    public List<String> getMsgEspecialAsList() {
        return Arrays.asList(msgEspecial.split("\n"));
    }

    public void setMsgEspecial(String msgEspecial) {
        this.msgEspecial = msgEspecial;
    }

    public boolean isDefinirEtiqueta() {
        return definirEtiqueta != null && definirEtiqueta == 1;
    }

    public void setDefinirEtiqueta(boolean definirEtiqueta) {
        this.definirEtiqueta = definirEtiqueta ? 1 : 0;
    }

    public boolean isDefinirMsgEspecial() {
        return definirMsgEspecial != null && definirMsgEspecial == 1;
    }

    public void setDefinirMsgEspecial(boolean definirMsgEspecial) {
        this.definirMsgEspecial = definirMsgEspecial ? 1 : 0;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Impressora)) {
            return false;
        }
        Impressora other = (Impressora) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Impressora[ indice=" + indice + " ]";
    }

}
