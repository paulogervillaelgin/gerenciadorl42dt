/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "PRODUTO")
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findByIndice", query = "SELECT p FROM Produto p WHERE p.indice = :indice")
    , @NamedQuery(name = "Produto.findByCodigo", query = "SELECT p FROM Produto p WHERE p.codigo = :codigo")
    , @NamedQuery(name = "Produto.findByPesado", query = "SELECT p FROM Produto p WHERE p.pesado = :pesado")
    , @NamedQuery(name = "Produto.findByPreco", query = "SELECT p FROM Produto p WHERE p.preco = :preco")
    , @NamedQuery(name = "Produto.findByTara", query = "SELECT p FROM Produto p WHERE p.tara = :tara")
    , @NamedQuery(name = "Produto.findByDescricao", query = "SELECT p FROM Produto p WHERE p.descricao = :descricao")
    , @NamedQuery(name = "Produto.findByDescricao2", query = "SELECT p FROM Produto p WHERE p.descricao2 = :descricao2")
    , @NamedQuery(name = "Produto.findByItemcodigo", query = "SELECT p FROM Produto p WHERE p.itemcodigo = :itemcodigo")
    , @NamedQuery(name = "Produto.findByValidadedias", query = "SELECT p FROM Produto p WHERE p.validadedias = :validadedias")
    , @NamedQuery(name = "Produto.findByIngredientes", query = "SELECT p FROM Produto p WHERE p.ingredientes = :ingredientes")
    , @NamedQuery(name = "Produto.findByMsgespecial", query = "SELECT p FROM Produto p WHERE p.msgespecial = :msgespecial")
    , @NamedQuery(name = "Produto.findByDepartamento", query = "SELECT p from Produto p,Departamento d WHERE d.indice = :departamento AND p.grupoproduto member of d.grupoprodutoSet ORDER BY p.descricao")
    , @NamedQuery(name = "Produto.findByUnitariosDepartamento", query = "SELECT p from Produto p,Departamento d WHERE d.indice = :departamento AND p.grupoproduto member of d.grupoprodutoSet AND p.pesado = 0 ORDER BY p.descricao")
    , @NamedQuery(name = "Produto.updateEtiquetaPesado", query = "UPDATE Produto p SET p.etiqueta = :etiqueta WHERE p.pesado = 1 or p.pesado = null")
    , @NamedQuery(name = "Produto.updateEtiquetaUnitario", query = "UPDATE Produto p SET p.etiqueta = :etiqueta WHERE p.pesado = 0")
    , @NamedQuery(name = "Produto.updateCodigoBarras", query = "UPDATE Produto p SET p.codigoBarras = :codigobarra, p.flag = :flag WHERE p.pesado = :pesado")

    , @NamedQuery(name = "Produto.countByDepartamento", query = "SELECT COUNT(p) from Produto p,Departamento d WHERE d.indice = :departamento AND p.grupoproduto member of d.grupoprodutoSet")
})
public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private int codigo;
    @Column(name = "PESADO")
    private Integer pesado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRECO")
    private BigDecimal preco;
    @Column(name = "TARA")
    private BigDecimal tara;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "DESCRICAO2")
    private String descricao2;
    @Column(name = "ITEMCODIGO")
    private Integer itemcodigo;

    @Column(name = "PORCAO")
    private String porcao;

    @Column(name = "CALORIAS")
    private Integer calorias;

    @Column(name = "CARBOIDRATOS")
    private BigDecimal carboidratos;

    @Column(name = "PROTEINAS")
    private BigDecimal proteinas;

    @Column(name = "GORDURASTOTAIS")
    private BigDecimal gordurastotais;

    @Column(name = "GORDURASSATURADAS")
    private BigDecimal gordurassaturadas;

    @Column(name = "GORDURASTRANS")
    private BigDecimal gordurastrans;

    @Column(name = "FIBRAALIMENTAR")
    private BigDecimal fibraalimentar;

    @Column(name = "SODIO")
    private BigDecimal sodio;

    @Column(name = "VALIDADEDIAS")
    private Integer validadedias;
    @Column(name = "INGREDIENTES")
    private String ingredientes;
    @Column(name = "MSGESPECIAL")
    private String msgespecial;
    @Column(name = "FLAG")
    private Integer flag;
    @Column(name = "IMPRIMIRVALIDADE")
    private Integer imprimirvalidade;

    @JoinColumn(name = "CODIGOBARRAS", referencedColumnName = "INDICE")
    @ManyToOne
    private Flexibarcode codigoBarras;

    @JoinColumn(name = "ETIQUETA", referencedColumnName = "INDICE")
    @ManyToOne
    private FormatoEtiqueta etiqueta;

    @JoinColumn(name = "GRUPOPRODUTO", referencedColumnName = "INDICE")
    @ManyToOne
    private Grupoproduto grupoproduto;

    public Produto() {
    }

    public Produto(Integer indice) {
        this.indice = indice;
    }

    public Produto(Integer indice, int codigo) {
        this.indice = indice;
        this.codigo = codigo;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isPesado() {
        return pesado == null || pesado == 1;
    }

    public void setPesado(boolean valor) {
        this.pesado = valor ? 1 : 0;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getTara() {
        return tara != null ? tara : BigDecimal.ZERO;
    }

    public void setTara(BigDecimal tara) {
        this.tara = tara;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao2() {
        return descricao2;
    }

    public void setDescricao2(String descricao2) {
        this.descricao2 = descricao2;
    }

    public Integer getItemCodigo() {
        return itemcodigo;
    }

    public void setItemCodigo(Integer itemcodigo) {
        this.itemcodigo = itemcodigo;
    }

    public Integer getValidadeDias() {
        return validadedias;
    }

    public void setValidadeDias(Integer validadedias) {
        this.validadedias = validadedias;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public List<String> getIngredientesAsList() {

        return Arrays.asList(ingredientes.split("\n"));
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getMsgEspecial() {
        return msgespecial;
    }

    public List<String> getMsgEspecialAsList() {
        return Arrays.asList(msgespecial.split("\n"));
    }

    public void setMsgEspecial(String msgespecial) {
        this.msgespecial = msgespecial;
    }

    public Grupoproduto getGrupoproduto() {
        return grupoproduto;
    }

    public void setGrupoproduto(Grupoproduto grupoproduto) {
        this.grupoproduto = grupoproduto;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getPorcao() {
        return porcao;
    }

    public void setPorcao(String porcao) {
        this.porcao = porcao;
    }

    public Integer getCalorias() {
        return calorias != null ? calorias : 0;
    }

    public void setCalorias(Integer calorias) {
        this.calorias = calorias;
    }

    public BigDecimal getCarboidratos() {
        return carboidratos != null ? carboidratos : BigDecimal.ZERO;
    }

    public void setCarboidratos(BigDecimal carboidratos) {
        this.carboidratos = carboidratos;
    }

    public BigDecimal getProteinas() {
        return proteinas != null ? proteinas : BigDecimal.ZERO;
    }

    public void setProteinas(BigDecimal proteinas) {
        this.proteinas = proteinas;
    }

    public BigDecimal getGordurastotais() {
        return gordurastotais != null ? gordurastotais : BigDecimal.ZERO;
    }

    public void setGordurastotais(BigDecimal gordurastotais) {
        this.gordurastotais = gordurastotais;
    }

    public BigDecimal getGordurassaturadas() {
        return gordurassaturadas != null ? gordurassaturadas : BigDecimal.ZERO;
    }

    public void setGordurassaturadas(BigDecimal gordurassaturadas) {
        this.gordurassaturadas = gordurassaturadas;
    }

    public BigDecimal getGordurastrans() {
        return gordurastrans != null ? gordurastrans : BigDecimal.ZERO;
    }

    public void setGordurastrans(BigDecimal gordurastrans) {
        this.gordurastrans = gordurastrans;
    }

    public BigDecimal getFibraalimentar() {
        return fibraalimentar != null ? fibraalimentar : BigDecimal.ZERO;
    }

    public void setFibraalimentar(BigDecimal fibraalimentar) {
        this.fibraalimentar = fibraalimentar;
    }

    public BigDecimal getSodio() {
        return sodio != null ? sodio : BigDecimal.ZERO;
    }

    public void setSodio(BigDecimal sodio) {
        this.sodio = sodio;
    }

    public Flexibarcode getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(Flexibarcode codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public FormatoEtiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(FormatoEtiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    public boolean isImprimirValidade() {
        return imprimirvalidade == null || imprimirvalidade == 1;
    }

    public void setImprimirValidade(boolean valor) {
        this.imprimirvalidade = valor ? 1 : 0;
    }

    public boolean hasNutriData() {
        return (getPorcao() != null && !getPorcao().isEmpty())
                || (getCalorias() > 0)
                || (getCarboidratos() != null && getCarboidratos().doubleValue() > 0)
                || (getProteinas() != null && getProteinas().doubleValue() > 0)
                || (getGordurastotais() != null && getGordurastotais().doubleValue() > 0)
                || (getGordurassaturadas() != null && getGordurassaturadas().doubleValue() > 0)
                || (getGordurastrans() != null && getGordurastrans().doubleValue() > 0)
                || (getFibraalimentar() != null && getFibraalimentar().doubleValue() > 0)
                || (getSodio() != null && getSodio().doubleValue() > 0);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Produto[ indice=" + indice + " ]";
    }

}
