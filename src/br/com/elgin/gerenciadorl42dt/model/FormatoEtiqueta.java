/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "FORMATOETIQUETA")
@NamedQueries({
    @NamedQuery(name = "FormatoEtiqueta.findAll", query = "SELECT f FROM FormatoEtiqueta f")
    , @NamedQuery(name = "FormatoEtiqueta.findByIndice", query = "SELECT f FROM FormatoEtiqueta f WHERE f.indice = :indice")
    , @NamedQuery(name = "FormatoEtiqueta.findByDescricao", query = "SELECT f FROM FormatoEtiqueta f WHERE f.descricao = :descricao")
        , @NamedQuery(name = "FormatoEtiqueta.findByTipo", query = "SELECT f FROM FormatoEtiqueta f WHERE f.tipo = :tipo")
})
public class FormatoEtiqueta implements Serializable {

    public static final int TIPOPESADO = 0;
    public static final int TIPOUNIDADE = 1;
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "TIPO")
    private Integer tipo;
    @Column(name = "CONTEUDO")
    private String conteudo;

    public FormatoEtiqueta() {
    }

    public FormatoEtiqueta(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getConteudo() {
        return conteudo;
    }

    public List<String> getConteudoAsList() {
        return new ArrayList<>(Arrays.asList(conteudo.split("\n")));
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }        

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormatoEtiqueta)) {
            return false;
        }
        FormatoEtiqueta other = (FormatoEtiqueta) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.gerenciadorl42dt.model.Formatoetiqueta[ indice=" + indice + " ]";
    }

}
