/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "IMPORTACAO")
@NamedQueries({
    @NamedQuery(name = "Importacao.findAll", query = "SELECT i FROM Importacao i")
    , @NamedQuery(name = "Importacao.findByIndice", query = "SELECT i FROM Importacao i WHERE i.indice = :indice")
    , @NamedQuery(name = "Importacao.findByAutomatica", query = "SELECT i FROM Importacao i WHERE i.automatica = :automatica")
    , @NamedQuery(name = "Importacao.findByEndereco", query = "SELECT i FROM Importacao i WHERE i.endereco = :endereco")
    , @NamedQuery(name = "Importacao.findByExecpos", query = "SELECT i FROM Importacao i WHERE i.execpos = :execpos")
    , @NamedQuery(name = "Importacao.findByExecprograma", query = "SELECT i FROM Importacao i WHERE i.execprograma = :execprograma")
    , @NamedQuery(name = "Importacao.findByHashs", query = "SELECT i FROM Importacao i WHERE i.hashs = :hashs")
    , @NamedQuery(name = "Importacao.findByLimparbase", query = "SELECT i FROM Importacao i WHERE i.limparbase = :limparbase")
    , @NamedQuery(name = "Importacao.findByParametros", query = "SELECT i FROM Importacao i WHERE i.parametros = :parametros")
    , @NamedQuery(name = "Importacao.findByPosdiretorio", query = "SELECT i FROM Importacao i WHERE i.posdiretorio = :posdiretorio")
    , @NamedQuery(name = "Importacao.findByPrearquivo", query = "SELECT i FROM Importacao i WHERE i.prearquivo = :prearquivo")
    , @NamedQuery(name = "Importacao.findByPrograma", query = "SELECT i FROM Importacao i WHERE i.programa = :programa")
    , @NamedQuery(name = "Importacao.findByTipo", query = "SELECT i FROM Importacao i WHERE i.tipo = :tipo")
    , @NamedQuery(name = "Importacao.findByTipopos", query = "SELECT i FROM Importacao i WHERE i.tipopos = :tipopos")
    , @NamedQuery(name = "Importacao.findByTipopre", query = "SELECT i FROM Importacao i WHERE i.tipopre = :tipopre")
    , @NamedQuery(name = "Importacao.findByTouppercase", query = "SELECT i FROM Importacao i WHERE i.touppercase = :touppercase")})
public class Importacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;


    @Column(name = "AUTOMATICA")
    private Integer automatica;
    @Column(name = "ENDERECO")
    private String endereco;
    @Column(name = "EXECPOS")
    private Integer execpos;
    @Column(name = "EXECPROGRAMA")
    private Integer execprograma;
    @Column(name = "HASHS")
    private String hashs;
    @Column(name = "LIMPARBASE")
    private Integer limparbase;
    @Column(name = "PARAMETROS")
    private String parametros;
    @Column(name = "POSDIRETORIO")
    private String posdiretorio;
    @Column(name = "PREARQUIVO")
    private String prearquivo;
    @Column(name = "PROGRAMA")
    private String programa;
    @Column(name = "TIPO")
    private Integer tipo;
    @Column(name = "TIPOPOS")
    private Integer tipopos;
    @Column(name = "TIPOPRE")
    private Integer tipopre;
    @Column(name = "TOUPPERCASE")
    private Integer touppercase;

    public Importacao() {
    }

    public Importacao(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Boolean isAutomatica() {
        return automatica != null ? automatica == 1 : false;
    }

    public void setAutomatica(Boolean automatica) {
        this.automatica = automatica ? 1 : 0;
    }

    public String getEndereco() {
        return endereco!=null?endereco:"";
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Boolean isExecPos() {
        return execpos != null ? execpos == 1 : false;
    }

    public void setExecPos(Boolean execpos) {
        this.execpos = execpos ? 1 : 0;
    }

    public Boolean isExecPrograma() {
        return execprograma != null ? execprograma == 1 : false;
    }

    public void setExecPrograma(Boolean execprograma) {
        this.execprograma = execprograma ? 1 : 0;
    }

    public String getHashs() {
        return hashs!=null?hashs:"";
    }

    public void setHashs(String hashs) {
        this.hashs = hashs;
    }


    public Boolean isLimparBase() {
        return limparbase != null ? limparbase == 1 : false;
    }

    public void setLimparBase(Boolean limparbase) {
        this.limparbase = limparbase ? 1 : 0;
    }

    public String getParametros() {
        return parametros!=null?parametros:"";
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getPosDiretorio() {
        return posdiretorio!=null?posdiretorio:"";
    }

    public void setPosDiretorio(String posdiretorio) {
        this.posdiretorio = posdiretorio;
    }

    public String getPreArquivo() {
        return prearquivo!=null?prearquivo:"";
    }

    public void setPreArquivo(String prearquivo) {
        this.prearquivo = prearquivo;
    }

    public String getPrograma() {
        return programa!=null?programa:"";
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public Integer getTipo() {
        return tipo!=null?tipo:0;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getTipoPos() {
        return tipopos!=null?tipopos:0;
    }

    public void setTipoPos(Integer tipopos) {
        this.tipopos = tipopos;
    }

    public Integer getTipoPre() {
        return tipopre!=null?tipopre:0;
    }

    public void setTipoPre(Integer tipopre) {
        this.tipopre = tipopre;
    }

    public Boolean isToUpperCase() {
        return touppercase != null ? touppercase == 1 : false;
    }

    public void setTouppercase(Boolean touppercase) {
        this.touppercase = touppercase ? 1 : 0;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Importacao)) {
            return false;
        }
        Importacao other = (Importacao) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.labelnetx.model.Importacao[ indice=" + indice + " ]";
    }

}
