/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
@Entity
@Table(name = "FLEXIBARCODE")
@NamedQueries({
    @NamedQuery(name = "Flexibarcode.findAll", query = "SELECT f FROM Flexibarcode f")
    , @NamedQuery(name = "Flexibarcode.findByIndice", query = "SELECT f FROM Flexibarcode f WHERE f.indice = :indice")
    , @NamedQuery(name = "Flexibarcode.findByDescricao", query = "SELECT f FROM Flexibarcode f WHERE f.descricao = :descricao")
    , @NamedQuery(name = "Flexibarcode.findByTipo", query = "SELECT f FROM Flexibarcode f WHERE f.tipo = :tipo")
    , @NamedQuery(name = "Flexibarcode.findByFlag", query = "SELECT f FROM Flexibarcode f WHERE f.flag = :flag")
    , @NamedQuery(name = "Flexibarcode.findByTamanhocodigo", query = "SELECT f FROM Flexibarcode f WHERE f.tamanhocodigo = :tamanhocodigo")
    , @NamedQuery(name = "Flexibarcode.findByInfo1", query = "SELECT f FROM Flexibarcode f WHERE f.info1 = :info1")
    , @NamedQuery(name = "Flexibarcode.findByTamanhoinfo1", query = "SELECT f FROM Flexibarcode f WHERE f.tamanhoinfo1 = :tamanhoinfo1")
    , @NamedQuery(name = "Flexibarcode.findByInfo2", query = "SELECT f FROM Flexibarcode f WHERE f.info2 = :info2")
    , @NamedQuery(name = "Flexibarcode.findByTamanhoinfo2", query = "SELECT f FROM Flexibarcode f WHERE f.tamanhoinfo2 = :tamanhoinfo2")
    , @NamedQuery(name = "Flexibarcode.findByProdutoQtd", query = "SELECT f FROM Flexibarcode f WHERE f.produtoQtd = :produtoqtd")
    , @NamedQuery(name = "Flexibarcode.findByCheckdigit", query = "SELECT f FROM Flexibarcode f WHERE f.checkdigit = :checkdigit")})
public class Flexibarcode implements Serializable {


    public static final List<String> TIPOS = new LinkedList<String>(Arrays.asList("ITF", "EAN"));
    public static final List<String> FLAGS = new LinkedList<String>(Arrays.asList("F2", "F1F2", "Sem Flag"));
    public static final List<String> CODIGO = new LinkedList<String>(Arrays.asList("Sem Código", "1 Dígito", "2 Dígitos", "3 Dígitos", "4 Dígitos", "5 Dígitos", "6 Dígitos", "7 Dígitos", "8 Dígitos", "9 Dígitos"));
    public static final List<String> TAMANHO_INFO = new LinkedList<String>(Arrays.asList("Sem informação", "1 Dígito", "2 Dígitos", "3 Dígitos", "4 Dígitos", "5 Dígitos", "6 Dígitos", "7 Dígitos", "8 Dígitos", "9 Dígitos"));
    public static final List<String> INFOS = new LinkedList<String>(Arrays.asList("Peso", "Quantidade", "Preço Unitário", "Peso/Quantidade", "Preço Total", "Preço Original", "Uso Programado"));
    public static final List<String> CHECKDIGIT = new LinkedList<String>(Arrays.asList("Não verificar", "Meio", "Fim", "Meio e Fim"));
    public static final List<String> INFOS_VARIABLES = new LinkedList<String>(Arrays.asList("NW$", "QTBC$", "UP$", "", "TPBC$"));

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "INDICE")
    private Integer indice;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "TIPO")
    private Integer tipo;
    @Column(name = "FLAG")
    private Integer flag;
    @Column(name = "TAMANHOCODIGO")
    private Integer tamanhocodigo;
    @Column(name = "INFO1")
    private Integer info1;
    @Column(name = "TAMANHOINFO1")
    private Integer tamanhoinfo1;
    @Column(name = "INFO2")
    private Integer info2;
    @Column(name = "TAMANHOINFO2")
    private Integer tamanhoinfo2;
    @Column(name = "CHECKDIGIT")
    private Integer checkdigit;
    //capo usado para filtrar os codigos de barras nos produtos pesados/unitarios
    @Column(name="PRODUTOQTD")
    private Integer produtoQtd;
    
    public Flexibarcode() {
    }

    public Flexibarcode(Integer indice) {
        this.indice = indice;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getTamanhocodigo() {
        return tamanhocodigo;
    }

    public void setTamanhocodigo(Integer tamanhocodigo) {
        this.tamanhocodigo = tamanhocodigo;
    }

    public Integer getInfo1() {
        return info1;
    }

    public void setInfo1(Integer info1) {
        this.info1 = info1;
    }

    public Integer getTamanhoinfo1() {
        return tamanhoinfo1;
    }

    public void setTamanhoinfo1(Integer tamanhoinfo1) {
        this.tamanhoinfo1 = tamanhoinfo1;
    }

    public Integer getInfo2() {
        return info2;
    }

    public void setInfo2(Integer info2) {
        this.info2 = info2;
    }

    public Integer getTamanhoinfo2() {
        return tamanhoinfo2;
    }

    public void setTamanhoinfo2(Integer tamanhoinfo2) {
        this.tamanhoinfo2 = tamanhoinfo2;
    }

    public Integer getCheckdigit() {
        return checkdigit;
    }

    public void setCheckdigit(Integer checkdigit) {
        this.checkdigit = checkdigit;
    }
    
    
    //campo usado para filtrar os codigos de barras nos produtos pesados/unitarios    
    public boolean isProdutoQtd() {
        return produtoQtd == null || produtoQtd == 1;
    }
    
    //campo usado para filtrar os codigos de barras nos produtos pesados/unitarios
    public void setProdutoQtd(boolean valor) {
        this.produtoQtd = valor ? 1 : 0;
    }
    
    
    public String getStringFormato() {
        String formato = "";
        if (getFlag() >= 0 && getFlag() < 2) {
            formato = formato + new String(new char[getFlag() + 1]).replace('\0', 'F');
        }
        if (getTamanhocodigo() > 0) {
            formato = formato + new String(new char[getTamanhocodigo()]).replace('\0', 'C');
        }
        if (getCheckdigit() == 1 || getCheckdigit() == 3) {
            formato = formato + "D";
        }
        if (getTamanhoinfo1() > 0) {
            formato = formato + new String(new char[getTamanhoinfo1()]).replace('\0', 'A');
        }
        if (getTamanhoinfo2() > 0) {
            formato = formato + new String(new char[getTamanhoinfo2()]).replace('\0', 'B');
        }
        if (getCheckdigit() == 2 || getCheckdigit() == 3) {
            formato = formato + "D";
        }
        return formato;

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indice != null ? indice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Flexibarcode)) {
            return false;
        }
        Flexibarcode other = (Flexibarcode) object;
        if ((this.indice == null && other.indice != null) || (this.indice != null && !this.indice.equals(other.indice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.elgin.labelnetx.model.Flexibarcode[ indice=" + indice + " ]";
    }

}
