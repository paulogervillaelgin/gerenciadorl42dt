/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.derby.drda.NetworkServerControl;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public final class ThreadServidorDados extends Thread {

    NetworkServerControl servidor;

    public Integer porta;
    public String diretorio;

    public Integer getPorta() {
        return porta;
    }

    public void setPorta(Integer porta) {
        this.porta = porta;
    }

    public String getDiretorio() {
        return diretorio;
    }

    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }
    
    public ThreadServidorDados(String dir,Integer port) {
        setDiretorio(dir);
        setPorta(port);
        try {
            System.setProperty("derby.system.home", getDiretorio());
            servidor = new NetworkServerControl(InetAddress.getByName("0.0.0.0"), getPorta());
        } catch (UnknownHostException ex) {
            Logger.getLogger(ThreadServidorDados.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ThreadServidorDados.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void ping() throws Exception {
        servidor.ping();
    }
    
    @Override
    public void run() {
        
        try {
            System.out.println("Iniciando servidor de dados");
            servidor.start(new PrintWriter(System.out));
        } catch (Exception ex) {
            Logger.getLogger(ThreadServidorDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
