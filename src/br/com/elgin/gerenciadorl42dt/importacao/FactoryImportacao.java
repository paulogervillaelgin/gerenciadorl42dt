/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FactoryImportacao {

    public static final List<String> LISTA_TIPOS_IMPORTACAO = Arrays.asList("Formato Filizola","Formato Toledo");
    
public static AbstractImportacao getImportacaoByType(Importacao importacao,Usuario user,boolean forcar) {
        if(importacao.getTipo()==0){
            return new ImportacaoFilizola(importacao,user,forcar);
        } else {
            return new ImportacaoToledo(importacao,user,forcar);
        }
    }
    
}
