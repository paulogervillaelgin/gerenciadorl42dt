
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.DAO.DepartamentoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ImportacaoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.LojaJpaController;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.model.Loja;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public final class ImportacaoFilizola extends AbstractImportacao {

    private static final String ARQUIVO_PRODUTO = "CADTXT.TXT";
    private static final String ARQUIVO_SETOR = "SETORTXT.TXT";
    private static final String ARQUIVO_RECEITA = "REC_ASS.TXT";
    private static final String ARQUIVO_NUTRICIONAL = "NUTRI.TXT";
    private static final String ARQUIVO_FORNECEDOR = "RECFOR.TXT";

    //TODO tratar os nomes originais e colocar o .old    
    private static final String ARQUIVO_PRODUTO_OLD = "CADTXT.OLD";
    private static final String ARQUIVO_SETOR_OLD = "SETORTXT.OLD";
    private static final String ARQUIVO_RECEITA_OLD = "REC_ASS.OLD";
    private static final String ARQUIVO_NUTRICIONAL_OLD = "NUTRI.OLD";
    private static final String ARQUIVO_FORNECEDOR_OLD = "RECFOR.OLD";

    static GrupoprodutoJpaController daoGrupoProduto = new GrupoprodutoJpaController(emf);
    static DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);
    static ImportacaoJpaController daoImportacao = new ImportacaoJpaController(emf);
    static LojaJpaController daoLoja = new LojaJpaController(emf);

    public ImportacaoFilizola(Importacao imp, Usuario user, boolean forcar) {
        super(imp, user, forcar);
    }

    public ImportacaoFilizola(Importacao imp) {
        super(imp);
    }

    @Override
    boolean executarImportacao() {

        // TODO : verificar se existe tambem ?
        updateMessage("Iniciando processamento");
        if (getImportacao().getEndereco() != null && Files.isDirectory(Paths.get(importacao.getEndereco()), LinkOption.NOFOLLOW_LINKS)) {

            File arq_prod = getArquivoCaseInsensitive(ARQUIVO_PRODUTO);
            if (arq_prod != null) {

                Map<Integer, String> mapa_rec = new TreeMap<>();
                Map<Integer, String> mapa_for = new TreeMap<>();
                Map<Integer, String> mapa_nutri = new TreeMap<>();
                Map<Integer, Grupoproduto> mapa_grupo = new TreeMap<>();

                //procura a loja
                final Loja loja = daoLoja.findLoja(SessaoAtual.getLoja().getIndice());

                //processamento do arquivo de setores
                File arq_setor = getArquivoCaseInsensitive(ARQUIVO_SETOR);
                if (arq_setor != null) {
                    try {
                        updateMessage("Processando arquivo de setores ...");
                        List<String> linhas = Utilidades.readAllLines(arq_setor);
                        //List<Departamento> deptos = daoDepartamento.findDepartamentoEntities();
                        List<Grupoproduto> grupos = daoGrupoProduto.findGrupoprodutoEntities();
                        linhas.forEach((linha) -> {
                            String nome = linha.substring(0, 12).trim();
                            Integer codigo = Integer.valueOf(linha.substring(12, 18));

                            //verifica se já existe um departamento com o nome
                            Optional<Grupoproduto> grupo = grupos.parallelStream().filter((grp) -> (grp.getDescricao().equals(nome))).findAny();
                            if (!grupo.isPresent()) {
                                //caso não exista então cria um departamento e um grupo de produto
                                Grupoproduto valor = new Grupoproduto();
                                valor.setDescricao(nome.trim());
                                daoGrupoProduto.create(valor);

                                Departamento depto = new Departamento();
                                depto.setDescricao(nome.trim());
                                depto.setGrupoprodutoSet(new LinkedHashSet<>(Arrays.asList(valor)));
                                daoDepartamento.create(depto);

                                grupos.add(valor);
                                mapa_grupo.put(codigo, valor);
                            } else {
                                mapa_grupo.put(codigo, grupo.get());
                            }
                        });
                        linhas.clear();
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        updateMessage("Ocorreu um erro processando o arquivo de setores.");
                        updateProgress(1, 1);
                        return false;
                    }
                }

                //processamento do arquivo de receitas
                File arq_rec = getArquivoCaseInsensitive(ARQUIVO_RECEITA);
                if (arq_rec != null) {
                    try {
                        updateMessage("Processando arquivo de ingredientes...");
                        getMapOfReceitaFornecedor(mapa_rec, arq_rec);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        updateMessage("Ocorreu um erro processando o arquivo de ingredientes.");
                        updateProgress(1, 1);
                        return false;
                    }
                }

                //processamento do arquivo de fornecedor
                File arq_for = getArquivoCaseInsensitive(ARQUIVO_FORNECEDOR);
                if (arq_for != null) {
                    try {
                        updateMessage("Processando arquivo de fornecedores...");
                        getMapOfReceitaFornecedor(mapa_for, arq_for);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        updateMessage("Ocorreu um erro processando o arquivo de fornecedores.");
                        updateProgress(1, 1);
                        return false;
                    }
                }

                //processamento do arquivo de nutricionais                
                File arq_nutri = getArquivoCaseInsensitive(ARQUIVO_NUTRICIONAL);
                if (arq_nutri != null) {
                    try {
                        updateMessage("Processando arquivo de nutricionais...");
                        List<String> linhas = Utilidades.readAllLines(arq_nutri);
                        linhas.forEach((linha) -> {
                            if (linha.length() == 131) {
                                mapa_nutri.put(Integer.valueOf(linha.substring(0, 6)), linha.substring(6));
                            }
                        });
                        linhas.clear();
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        updateMessage("Ocorreu um erro processando o arquivo de nutricionais.");
                        updateProgress(1, 1);
                        return false;
                    }
                }

                List<Produto> lista = new LinkedList<>();
                if (arq_prod.exists()) {
                    updateMessage("Processando arquivo de produtos...");
                    try {
                        Grupoproduto grupo_padrao = daoGrupoProduto.findGrupoprodutoEntitiesByPadrao();
                        List<String> linhas = Utilidades.readAllLines(arq_prod);
                        for (String linha_prod : linhas) {
                            //desconsidera o DIVERSOS
                            if (linha_prod.length() >= 36) {
                                if (!linha_prod.substring(0, 7).equals("1000000")) {

                                    if (linha_prod.length() == 169 && linha_prod.endsWith("0000")) {
                                        linha_prod = linha_prod.substring(0, 165).trim();
                                    }

                                    Produto prod = new Produto();

                                    prod.setCodigo(Integer.valueOf(linha_prod.substring(0, 6)));
                                    if (mapa_grupo.containsKey(prod.getCodigo())) {
                                        prod.setGrupoproduto(mapa_grupo.get(prod.getCodigo()));
                                    } else {
                                        prod.setGrupoproduto(grupo_padrao);
                                    }

                                    prod.setPesado(linha_prod.substring(6, 7).toUpperCase().equals("P"));

                                    if (prod.isPesado()) {
                                        prod.setCodigoBarras(loja.getCodigobarrasPesado());
                                        prod.setEtiqueta(loja.getEtiquetaPesado());
                                        prod.setFlag(loja.getFlagPesado());
                                    } else {
                                        prod.setCodigoBarras(loja.getCodigobarrasUnitario());
                                        prod.setEtiqueta(loja.getEtiquetaUnitario());
                                        prod.setFlag(loja.getFlagUnitario());
                                    }

                                    //faz verificacao do itemcode com o tamanho do codigo no formato de codigo de barras                            
                                    if (((long) Math.log10(prod.getCodigo())) + 1 > prod.getCodigoBarras().getTamanhocodigo()) {
                                        updateMessage("Erro. Código Item excede o tamanho no código de barras(" + prod.getCodigo() + ")");
                                        break;
                                        //return false;
                                    }
                                    prod.setItemCodigo(prod.getCodigo());

                                    /*
                                    //TODO verificar defaults                                    
                                    prod.setImprimirPacotePorData(true);
                                    prod.setImprimirPacotePorHora(true);                                    
                                    prod.setImprimirVendaPorData(true);
                                    prod.setImprimirVendaPorHora(true);
                                    prod.setRtcVendaPorHora(true);
                                    prod.setRtcPacotePorHora(true);
                                     */
                                    prod.setDescricao(linha_prod.substring(7, 29).trim());
                                    //TODO verificar descrição2 no formato filizola (primeira linha da receita)
                                    prod.setPreco(BigDecimal.valueOf(Double.valueOf(linha_prod.substring(29, 36)) / 100));

                                    if (linha_prod.length() > 36) {
                                        if (linha_prod.charAt(36) != '@') {
                                            prod.setValidadeDias(Integer.valueOf(linha_prod.substring(36, 39)));
                                            //verifica nutricional
                                            if (linha_prod.length() > 39 && linha_prod.charAt(39) == '@') {
                                                capturarNutricional(prod, linha_prod.substring(40));
                                            }
                                        } else {
                                            capturarNutricional(prod, linha_prod.substring(37));
                                        }
                                    }

                                    //validade selecionada somente se for diferente de 0
                                    prod.setImprimirValidade(prod.getValidadeDias()!=null&&prod.getValidadeDias()>0);
                                    //verifica arq nutri
                                    capturarNutricional(prod, mapa_nutri.get(prod.getCodigo()));

                                    //verifica receita
                                    if (mapa_rec.size() > 0) {
                                        prod.setIngredientes(mapa_rec.get(prod.getCodigo()));
                                    }

                                    //verifica fornecedor
                                    if (mapa_for.size() > 0) {
                                        prod.setMsgEspecial(mapa_for.get(prod.getCodigo()));
                                    }

                                    lista.add(prod);
                                }
                            }
                        }
                        linhas.clear();
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        updateMessage("Ocorreu um erro processando o arquivo de produtos.");
                        return false;
                    }
                } else {
                    updateMessage("Arquivo de produtos não encontrado!");
                    updateProgress(1, 1);
                    return false;
                }

                if (!persistListaProduto(lista)) {
                    return false;
                }

                lista.clear();
                mapa_for.clear();
                mapa_nutri.clear();
                mapa_rec.clear();
            } else {
                updateMessage("Arquivo de produtos não encontrado!");
                updateProgress(1, 1);
                return false;
            }
            updateMessage("Importação concluída!");
            return true;
        } else {
            updateMessage("Diretorio não existe!");
            updateProgress(1, 1);
            return false;
        }
    }

    private void capturarNutricional(final Produto prod, String nutri) {
        if (prod != null) {
            if (nutri != null && nutri.length() == 125) {
                prod.setPorcao(nutri.substring(0, 20).trim());
                prod.setCalorias(Integer.valueOf(nutri.substring(35, 40)));
                prod.setCarboidratos(BigDecimal.valueOf(Double.valueOf(nutri.substring(44, 49)) / 10));
                prod.setProteinas(BigDecimal.valueOf(Double.valueOf(nutri.substring(53, 58)) / 10));
                prod.setGordurastotais(BigDecimal.valueOf(Double.valueOf(nutri.substring(62, 67)) / 10));
                prod.setGordurassaturadas(BigDecimal.valueOf(Double.valueOf(nutri.substring(71, 76)) / 10));
                prod.setGordurastrans(BigDecimal.valueOf(Double.valueOf(nutri.substring(80, 85)) / 10));
                prod.setFibraalimentar(BigDecimal.valueOf(Double.valueOf(nutri.substring(89, 94)) / 10));
                //98-103 calcio
                //107-112 ferro
                prod.setSodio(BigDecimal.valueOf(Double.valueOf(nutri.substring(116, 121)) / 10));
                //Não estou apagando flag
                //} else {
                //   prod.setImprimirNutri(false);
            }
        }
    }

    private void getMapOfReceitaFornecedor(final Map<Integer, String> resposta, File arquivo) throws Exception {
        try {
            if (arquivo.exists()) {
                List<String> linhas = Utilidades.readAllLines(arquivo);
                int codigo = 0;
                String receita = "";
                boolean acumulando = false;
                for (String linha_rec : linhas) {
                    if ((!acumulando)/*&&linha_rec.length() > 25*/) {
                        codigo = Integer.valueOf(linha_rec.substring(12, 18));
                        if (linha_rec.indexOf('@') == -1) {
                            receita = Utilidades.insertPeriodically(linha_rec.substring(24).trim(), "\n", 56);
                            acumulando = true;
                        } else {
                            receita = Utilidades.insertPeriodically(linha_rec.substring(24, linha_rec.indexOf('@')).trim(), "\n", 56);
                            if (importacao.isToUpperCase()) {
                                receita = receita.toUpperCase();
                            }
                            resposta.put(codigo, receita);
                            acumulando = false;
                        }
                    } else {
                        if (linha_rec.indexOf('@') == -1) {
                            receita = receita + "\n" + Utilidades.insertPeriodically(linha_rec.trim(), "\n", 56);
                        } else {
                            if (linha_rec.length() > 1) {
                                receita = receita + "\n" + Utilidades.insertPeriodically(linha_rec.substring(0, linha_rec.indexOf('@')).trim(), "\n", 56);
                            }
                            if (importacao.isToUpperCase()) {
                                receita = receita.toUpperCase();
                            }
                            resposta.put(codigo, receita);
                            acumulando = false;
                        }
                    }
                }
                linhas.clear();
            }
        } catch (Exception e) {
            throw e;
        }

    }

    @Override
    boolean verificarAlteracoesArquivos() {
        if (!importacao.getHashs().isEmpty()) {
            String[] hashs = importacao.getHashs().split(";", 5);
            if (hashs.length == 5) {

                File arq_prod = getArquivoCaseInsensitive(ARQUIVO_PRODUTO);
                if (arq_prod != null) {
                    if (hashs[0].isEmpty() || !Utilidades.getMD5Checksum(arq_prod.getAbsolutePath()).equals(hashs[0])) {
                        return true;
                    }
                }

                File arq_setor = getArquivoCaseInsensitive(ARQUIVO_SETOR);
                if (arq_setor != null) {
                    if (hashs[1].isEmpty() || !Utilidades.getMD5Checksum(arq_setor.getAbsolutePath()).equals(hashs[1])) {
                        return true;
                    }
                }

                File arq_receita = getArquivoCaseInsensitive(ARQUIVO_RECEITA);
                if (arq_receita != null) {
                    if (hashs[2].isEmpty() || !Utilidades.getMD5Checksum(arq_receita.getAbsolutePath()).equals(hashs[2])) {
                        return true;
                    }
                }

                File arq_nutri = getArquivoCaseInsensitive(ARQUIVO_NUTRICIONAL);
                if (arq_nutri != null) {
                    if (hashs[3].isEmpty() || !Utilidades.getMD5Checksum(arq_nutri.getAbsolutePath()).equals(hashs[3])) {
                        return true;
                    }
                }

                File arq_forn = getArquivoCaseInsensitive(ARQUIVO_FORNECEDOR);
                if (arq_forn != null) {
                    if (hashs[4].isEmpty() || !Utilidades.getMD5Checksum(arq_forn.getAbsolutePath()).equals(hashs[4])) {
                        return true;
                    }
                }

                return false;
            }
        }
        //Se os hashs não existem mas o arquivo de produto sim, então retornar true
        return getArquivoCaseInsensitive(ARQUIVO_PRODUTO) != null;
    }

    @Override
    void moverArquivos(File diretorio) {
        moveFileTo(ARQUIVO_PRODUTO, importacao.getPosDiretorio());
        moveFileTo(ARQUIVO_SETOR, importacao.getPosDiretorio());
        moveFileTo(ARQUIVO_RECEITA, importacao.getPosDiretorio());
        moveFileTo(ARQUIVO_NUTRICIONAL, importacao.getPosDiretorio());
        moveFileTo(ARQUIVO_FORNECEDOR, importacao.getPosDiretorio());
    }

    @Override
    void renomearArquivos() {
        renameFileTo(ARQUIVO_PRODUTO, ARQUIVO_PRODUTO_OLD);
        renameFileTo(ARQUIVO_SETOR, ARQUIVO_SETOR_OLD);
        renameFileTo(ARQUIVO_RECEITA, ARQUIVO_RECEITA_OLD);
        renameFileTo(ARQUIVO_NUTRICIONAL, ARQUIVO_NUTRICIONAL_OLD);
        renameFileTo(ARQUIVO_FORNECEDOR, ARQUIVO_FORNECEDOR_OLD);
    }

    @Override
    void salvarHashs() {
        String hashs = "";

        File arq = getArquivoCaseInsensitive(ARQUIVO_PRODUTO);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQUIVO_SETOR);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQUIVO_RECEITA);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQUIVO_NUTRICIONAL);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQUIVO_FORNECEDOR);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        importacao.setHashs(hashs);
        try {
            daoImportacao.edit(importacao);
        } catch (Exception ex) {
            Logger.getLogger(ImportacaoFilizola.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
