/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GeradorLog;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public abstract class AbstractImportacao extends Task<Boolean> {

    Importacao importacao;
    Usuario usuario;
    boolean executar, forcar_execucao = false;

    static final EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    static final ProdutoJpaController daoProduto = new ProdutoJpaController(emf);
    static final ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);

    public AbstractImportacao(Importacao imp, Usuario user, boolean forcar_execucao) {
        //TODO alterar parametro importacao para ser passado a chave e nao o objeto,
        //  para então buscar o objeto no banco com as informações atualizadas        
        this.forcar_execucao = forcar_execucao;
        executar = forcar_execucao;
        setImportacao(imp);
        setUsuario(user);
    }

    public AbstractImportacao(Importacao imp) {
        setImportacao(imp);
    }

    public Importacao getImportacao() {
        return importacao;
    }

    public void setImportacao(Importacao importacao) {
        this.importacao = importacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    abstract boolean executarImportacao();

    abstract boolean verificarAlteracoesArquivos();

    abstract void salvarHashs();

    abstract void moverArquivos(File diretorio);

    abstract void renomearArquivos();

    protected File getArquivoCaseInsensitive(String arquivo) {
        return Utilidades.getfileCaseInsensitive(Paths.get(importacao.getEndereco() + File.separator + arquivo), arquivo);
    }

    protected void renameFileTo(String file, String old_file) {
        File arq_file = Utilidades.getfileCaseInsensitive(Paths.get(importacao.getEndereco() + File.separator + file), file);
        if (arq_file != null) {
            File arq_old = new File(importacao.getEndereco() + File.separator + old_file);
            if (arq_old.exists()) {
                arq_old.delete();
            }
            arq_file.renameTo(arq_old);
        }
    }

    protected void moveFileTo(String file, String novodir) {
        File arq_file = Utilidades.getfileCaseInsensitive(Paths.get(importacao.getEndereco() + File.separator + file), file);
        if (arq_file != null) {
            try {
                Files.move(arq_file.toPath(), Paths.get(novodir + File.separator + file), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                Logger.getLogger(AbstractImportacao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected boolean persistListaProduto(List<Produto> lista) {
        //Persiste os produtos na base de dados, criando ou alterando os mesmos
        try {

            //cria um treemap com os indices e codigos dos produtos
            Map<Integer, Integer> listaP = daoProduto.getMapOfIndiceCodigo();

            //cria um Set para guardar os grupos e atualizar status das balanças
            Set<Grupoproduto> grupos = new HashSet<>();

            EntityManager em = null;
            try {
                try {
                    em = emf.createEntityManager();
                    em.getTransaction().begin();

                    //percorre a lista adicionando ou alterando os produtos
                    int total = 0;
                    for (Produto produto : lista) {
                        Integer indice = listaP.get(produto.getCodigo());

                        if (indice != null) {
                            produto.setIndice(indice);
                            em.merge(produto);

                            //remove o produto do mapa para no final da operação
                            // só sobrarem os produtos não atualizados/adicionados
                            // alem de melhorar a performance da procura
                            listaP.remove(produto.getCodigo());
                        } else {
                            em.persist(produto);
                        }

                        //adiciona grupo no Set
                        grupos.add(produto.getGrupoproduto());

                        updateProgress(++total, lista.size());
                        updateMessage("Adicionando produto ... (" + total + "/" + lista.size() + ")");
                    }

                    if (importacao.isLimparBase()) {
                        updateMessage("Limpando base ...");

                        if (listaP.size() > 0) {

                            Query delete = em.createQuery("DELETE FROM Produto p WHERE p.codigo <> 0 AND p.indice IN :indice");

                            delete.setParameter("indice", listaP.values());
                            //delete.setParameter("loja", importacao.getLoja());

                            delete.executeUpdate();

                            //TODO atualizar somente as balanças que tem produto excluido
                            //atualizando todas as balanças porque excluiu items
                            ImpressoraStatusManager.updateImpressorasOf(ImpressoraStatusEnum.PRODUTO, false);

                        } else {
                            //se não tiver configurado para excluir produtos fora da import:
                            //Atualiza o status das balanças cujos departamentos contem um grupo com produto alterado
                            updateMessage("Atualizando status das balanças...");
                            grupos.parallelStream().forEach((grupo) -> {
                                ImpressoraStatusManager.updateImpressorasOf(grupo, false);
                            });
                        }
                    } else {

                        //se não tiver configurado para excluir produtos fora da import:
                        //Atualiza o status das balanças cujos departamentos contem um grupo com produto alterado
                        updateMessage("Atualizando status das balanças...");
                        grupos.parallelStream().forEach((grupo) -> {
                            ImpressoraStatusManager.updateImpressorasOf(grupo, false);
                        });

                    }
                    updateMessage("Persistindo alterações ...");
                    em.getTransaction().commit();
                } catch (Exception e) {
                    if (em != null) {
                        em.getTransaction().rollback();
                    }
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    updateMessage("Ocorreu um erro no processo de inserção no banco de dados.");
                    return false;
                }
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            updateMessage("Ocorreu um erro no processo de inserção no banco de dados.");
            return false;
        }
        return true;
    }

    @Override
    protected Boolean call() throws Exception {
        try {
            updateMessage("Iniciando rotina de importação");
            if (!executar) {
                if (importacao.getTipoPre() == 0) {
                    executar = verificarAlteracoesArquivos();

                    //TESTE TODO para verificação das alterações/na versao final trocar por log
                    if (!executar) {
                        updateMessage("Nenhuma alteração detectada nos arquivos.");
                        updateProgress(1, 1);
                        return true;
                    }

                } else {
                    File arquivoPre = new File(importacao.getPreArquivo());
                    executar = arquivoPre.exists();

                    //TESTE TODO para verificação das alterações/na versao final trocar por log
                    if (!executar) {
                        updateMessage("Arquivo de sinalização não encontrado.");
                        updateProgress(1, 1);
                        return true;
                    }

                }
            }
            if (executar) {

                //registra o log de inicio do processo
                if (getUsuario() != null) {
                    GeradorLog.logMensagemUsuario("Iniciando importação de dados manual...");
                } else {
                    GeradorLog.logMensagemSistema("Iniciando importação de dados automática...");
                }

                //comentado pois agora iremos exibir outro componente
                if (!forcar_execucao) {
                    //executar bind na tela de Menu somente na importacao automatica
                    FormHelper.getFormMenuController().showPainelProgress();
                }
                //executar programa externo
                if (importacao.isExecPrograma()) {
                    Process p = Runtime.getRuntime().exec(importacao.getPrograma() + " " + importacao.getParametros() + " workdir=\""+ importacao.getEndereco() +"\"");                    
                    updateMessage("Aguardando programa externo terminar...");
                    if (p.waitFor() != 0) {
                        updateMessage("Programa externo retornou erro!");
                        updateProgress(0, 0);
                        updateValue(false);
                        return false;
                    }
                }

                //executar importacao
                if (executarImportacao()) {

                    //salvar hashs dos arquivos antes de executar rotinas de pós importação
                    if (importacao.getTipoPre() == 0) {
                        salvarHashs();
                    } else {
                        //exclui arquivo de pré importação,sinalizando o final
                        File arquivoPre = new File(importacao.getPreArquivo());
                        arquivoPre.delete();
                    }

                    //verifica se está configurado para executar pos importacao
                    if (importacao.isExecPos()) {
                        //executar ações de pós importacao
                        if (importacao.getTipoPos() == 0) {
                            renomearArquivos();
                        } else {
                            moverArquivos(new File(importacao.getPosDiretorio()));
                        }
                    }

                    //registra o log de sucesso
                    if (getUsuario() != null) {
                        GeradorLog.logMensagemUsuario("Importação manual realizada com sucesso!");
                    } else {
                        GeradorLog.logMensagemSistema("Importação automática realizada com sucesso!");
                    }

                    /*
                    loja.setDataImportacao(new Date());
                    daoLoja.edit(loja);
                     */
                    /*
                    //Perguntar antes se quer fazer uma transmissao depois de uma importacao manual
                    if (importacao.isAutomatica() && !forcar_execucao) {
                        System.out.println("importacao com sucesso, convocando transmissao automatica loja " + importacao.getLoja().getDescricao());
                        TransmissaoAutomatica.processar(importacao.getLoja());
                    }
                     */
                    return true;
                } else {
                    //seta as balanças como desatualizada quando a importação não ocorre normalmente
                    ImpressoraStatusManager.updateStatusListaImpressora(daoImpressora.findImpressoraEntities(), ImpressoraStatusEnum.PRODUTO, false);

                    //registra o log de erro
                    if (getUsuario() != null) {
                        GeradorLog.logMensagemUsuario("[ERRO] Ocorreu um erro durante a importação manual!");
                    } else {
                        GeradorLog.logMensagemSistema("[ERRO] Ocorreu um erro durante a importação automática!");
                    }

                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            ImpressoraStatusManager.updateStatusListaImpressora(daoImpressora.findImpressoraEntities(), ImpressoraStatusEnum.PRODUTO, false);
            updateMessage("Ocorreu um erro durante o processo.");
            updateProgress(0, 0);
        }
        return false;
    }

}
