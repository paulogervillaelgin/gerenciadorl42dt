/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.model.Importacao;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ServiceImportacao extends ScheduledService<Boolean> {

    private static class ServiceImportacaoHelper {

        private static final ServiceImportacao INSTANCE = new ServiceImportacao();
    }

    private Boolean habilitado = false;
    private Importacao importacao;

    public Importacao getImportacao() {
        return importacao;
    }

    public void setImportacao(Importacao importacao) {
        this.importacao = importacao;
        if(importacao != null) {
            setHabilitado(importacao.isAutomatica());
        }
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean value) {
        habilitado = value;
        if (habilitado) {
            setPeriod(Duration.minutes(1));
            restart();
        } else {
            setPeriod(Duration.INDEFINITE);
            cancel();
        }
    }

    public static ServiceImportacao getInstance() {
        return ServiceImportacaoHelper.INSTANCE;
    }

    @Override
    protected Task<Boolean> createTask() {
        return FactoryImportacao.getImportacaoByType(importacao, null, false);
    }

}
