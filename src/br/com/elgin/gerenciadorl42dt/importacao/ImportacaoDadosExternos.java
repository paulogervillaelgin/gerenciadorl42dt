/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.DAO.FormatoEtiquetaJpaController;
import br.com.elgin.gerenciadorl42dt.GeradorLog;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ImportacaoDadosExternos {

    private static final Path DIRETORIO_IMPORT = FileSystems.getDefault().getPath("import");

    final EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    final FormatoEtiquetaJpaController daoFormato = new FormatoEtiquetaJpaController(emf);

    private static ImportacaoDadosExternos instancia = null;

    public static ImportacaoDadosExternos getInstance() {
        if (instancia == null) {
            instancia = new ImportacaoDadosExternos();
        }
        return instancia;
    }

    public void verificaImportacoes() {
        //Criar diretorio caso não exista
        if (!Files.exists(DIRETORIO_IMPORT)) {
            try {
                Files.createDirectory(DIRETORIO_IMPORT);
            } catch (IOException ex) {
                Logger.getLogger(ImportacaoDadosExternos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        CarregaFormatosEtiqueta(DIRETORIO_IMPORT);
    }

    public void CarregaFormatosEtiqueta(Path diretorio) {

        try {

            Files.newDirectoryStream(diretorio, path -> path.toString().endsWith(".l42dt")).forEach(arquivo -> {

                int id = Integer.parseInt(arquivo.getFileName().toString().split("\\.")[0]);

                try {
                    List<String> lista = Utilidades.readAllLines(arquivo.toFile());

                    FormatoEtiqueta formato = daoFormato.findFormatoEtiqueta(id);
                    if (formato != null) {
                        formato.setDescricao(lista.remove(0));
                        formato.setTipo(formato.getDescricao().toLowerCase().contains("peso") ? FormatoEtiqueta.TIPOPESADO : FormatoEtiqueta.TIPOUNIDADE);
                        formato.setConteudo(lista.stream().collect(Collectors.joining("\n")));
                        daoFormato.edit(formato);
                    } else {
                        formato = new FormatoEtiqueta(id);
                        formato.setDescricao(lista.remove(0));
                        formato.setTipo(formato.getDescricao().toLowerCase().contains("peso") ? FormatoEtiqueta.TIPOPESADO : FormatoEtiqueta.TIPOUNIDADE);
                        formato.setConteudo(lista.stream().collect(Collectors.joining("\n")));
                        daoFormato.create(formato);
                    }
                    //atualizar impressoras para layout desatualizados
                    ImpressoraStatusManager.updateImpressorasOf(ImpressoraStatusEnum.LAYOUT, false);
                    GeradorLog.logMensagemSistema("Importação de layout realizada com sucesso. Arquivo: "+arquivo.getFileName());
                } catch (IOException ex) {
                    Logger.getLogger(ImportacaoDadosExternos.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ImportacaoDadosExternos.class.getName()).log(Level.SEVERE, null, ex);
                }
                arquivo.toFile().delete();
            });
        } catch (IOException ex) {
            Logger.getLogger(ImportacaoDadosExternos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
