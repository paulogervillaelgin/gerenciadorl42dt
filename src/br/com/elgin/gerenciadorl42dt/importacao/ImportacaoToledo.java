/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.importacao;

import br.com.elgin.gerenciadorl42dt.DAO.DepartamentoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ImportacaoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.LojaJpaController;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.model.Loja;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public final class ImportacaoToledo extends AbstractImportacao {

    private static final String ARQ_NUTRI_TOLEDO = "infnutri.txt";
    private static final String ARQ_ITENS_TOLEDO = "itensmgv.txt";
    private static final String ARQ_ITENS_TOLEDO_2 = "txitens.txt";
    private static final String ARQ_INFOITENS_TOLEDO = "txinfo.txt";
    private static final String ARQ_FORN_TOLEDO = "txforn.txt";
    //private static final String ARQ_FORN_FIL_TOLEDO = "recfor.txt";

    //TODO tratar os originais para concatenar o .old
    private static final String ARQ_NUTRI_TOLEDO_OLD = "infnutri.old";
    private static final String ARQ_ITENS_TOLEDO_OLD = "itensmgv.old";
    private static final String ARQ_ITENS_TOLEDO_2_OLD = "txitens.old";
    private static final String ARQ_INFOITENS_TOLEDO_OLD = "txinfo.old";
    private static final String ARQ_FORN_TOLEDO_OLD = "txforn.old";
    //private static final String ARQ_FORN_FIL_TOLEDO_OLD = "recfor.old";

    static GrupoprodutoJpaController daoGrupoProduto = new GrupoprodutoJpaController(emf);
    static DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);
    static ImportacaoJpaController daoImportacao = new ImportacaoJpaController(emf);
    static LojaJpaController daoLoja = new LojaJpaController(emf);

    public ImportacaoToledo(Importacao imp, Usuario user, boolean forcar) {
        super(imp, user, forcar);
    }

    public ImportacaoToledo(Importacao imp) {
        super(imp);
    }

    @Override
    boolean executarImportacao() {

        updateMessage("Iniciando processamento");
        if (getImportacao().getEndereco() != null && Files.isDirectory(Paths.get(importacao.getEndereco()), LinkOption.NOFOLLOW_LINKS)) {

            List<Produto> lista_prod = new LinkedList<>();

            Map<Integer, String> mapa_nutri = new TreeMap<>();
            Map<Integer, String> mapa_receita = new TreeMap<>();
            Map<Integer, String> mapa_forn = new TreeMap<>();

            //procura a loja
            Loja loja;
            loja = daoLoja.findLoja(SessaoAtual.getLoja().getIndice());
            //codigo desnecessario, feito somente para garantir que não tenha loja nula
            if (loja == null) {
                loja = SessaoAtual.getLoja();
            }

            //processamento do arquivo de nutricionais                
            File arq_nutri = getArquivoCaseInsensitive(ARQ_NUTRI_TOLEDO);
            if (arq_nutri != null) {
                try {
                    updateMessage("Processando arquivo de nutricionais...");
                    List<String> linhas = Utilidades.readAllLines(arq_nutri);
                    for (String linha : linhas) {
                        //desconsiderando linhas fora do padrao
                        switch (linha.length()) {
                            case 45:
                                mapa_nutri.put(Integer.valueOf(linha.substring(1, 7)), linha);
                                break;

                            //TODO lançamento sem rdc 39 40    
                            /*
                            case 69:
                            //mapa_nutri.put(Integer.valueOf(linha.substring(0, 4)), linha);
                            //break;
                             */
                            //caso queira invalidar o arquivo porque de linhas fora do padrao
                            //descomentar as linhas abaixo :
                            default:
                                updateMessage("Erro no arquivo de nutricionais!");
                                updateProgress(1, 1);
                                return false;

                        }
                    }
                    linhas.clear();
                    updateMessage("Nutricionais ok!");
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    updateMessage("Ocorreu um erro processando o arquivo de nutricionais.");
                    updateProgress(1, 1);
                    return false;
                }
            }

            //processamento do arquivo de receitas       
            File arq_txinfo = getArquivoCaseInsensitive(ARQ_INFOITENS_TOLEDO);
            if (arq_txinfo != null) {
                try {
                    updateMessage("Processando arquivo de receitas...");
                    List<String> linhas_info = Utilidades.readAllLines(arq_txinfo);
                    for (String linha_info : linhas_info) {
                        Integer codigo = Integer.valueOf(linha_info.substring(0, 6));
                        String receita = "";
                        for (int i = 0; i < 15; i++) {
                            if ((106 + (i * 56) + 56) <= linha_info.length()) {
                                String dado = linha_info.substring(106 + (i * 56), 106 + (i * 56) + 56);
                                if (!dado.trim().isEmpty()) {
                                    if (!receita.isEmpty()) {
                                        receita = receita + "\n";
                                    }
                                    receita = receita + dado.trim();
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        if (importacao.isToUpperCase()) {
                            receita = receita.toUpperCase();
                        }
                        mapa_receita.put(codigo, receita);
                    }
                    linhas_info.clear();
                    updateMessage("Receitas OK!");
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    updateMessage("Ocorreu um erro processando o arquivo de receitas.");
                    updateProgress(1, 1);
                    return false;
                }
            }

            //processamento do arquivo de fornecedores
            File arq_forn = getArquivoCaseInsensitive(ARQ_FORN_TOLEDO);
            if (arq_forn != null) {
                try {
                    updateMessage("Processando arquivo de fornecedores...");
                    List<String> linhas_forn = Utilidades.readAllLines(arq_forn);
                    for (String linha_forn : linhas_forn) {
                        Integer codigo = Integer.valueOf(linha_forn.substring(0, 4));
                        String fornecedor = "";
                        for (int i = 0; i < 5; i++) {
                            if ((104 + (i * 56) + 56) <= linha_forn.length()) {
                                String dado = linha_forn.substring(104 + (i * 56), 104 + (i * 56) + 56);
                                if (!dado.trim().isEmpty()) {
                                    if (!fornecedor.isEmpty()) {
                                        fornecedor = fornecedor + "\n";
                                    }
                                    fornecedor = fornecedor + dado.trim();
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        //TODO somente receita como uppercase ou fornecedores tambem ?
                        /*
                        if (Configuracao.isImportacaoUppercase()) {
                            fornecedor = fornecedor.toUpperCase();
                        }
                         */
                        mapa_forn.put(codigo, fornecedor);
                    }
                    linhas_forn.clear();
                    updateMessage("Fornecedores OK!");
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    updateMessage("Ocorreu um erro processando o arquivo de fornecedores.");
                    updateProgress(1, 1);
                    return false;
                }
            }

            //caso exista o itensmgv, utiliza-lo, senao procurar pelo txitens
            File arq_prod = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO);
            if (arq_prod == null) {
                arq_prod = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO_2);
            }
            if (arq_prod != null) {
                try {
                    updateMessage("Processando arquivo de produtos...");

                    List<Grupoproduto> grupos = daoGrupoProduto.findGrupoprodutoEntities();

                    List<String> linhas = Utilidades.readAllLines(arq_prod);

                    for (String linha_prod : linhas) {
                        //desconsidera linhas em branco
                        if (linha_prod.trim().length() > 0) {

                            Produto produto = new Produto();

                            //inicialização
                            int codfornecedor = 0;
                            int codnutricional = 0;
                            int codsetor;
                            Integer itemcode = null;

                            //ItensMGV
                            if (arq_prod.getName().equalsIgnoreCase(ARQ_ITENS_TOLEDO)) {

                                codsetor = Integer.valueOf(linha_prod.substring(0, 2));

                                produto.setPesado(linha_prod.substring(2, 3).equals("0"));
                                produto.setCodigo(Integer.valueOf(linha_prod.substring(3, 9)));
                                produto.setPreco(BigDecimal.valueOf(Double.valueOf(linha_prod.substring(9, 15)) / 100));
                                produto.setValidadeDias(Integer.valueOf(linha_prod.substring(15, 18)));
                                produto.setDescricao(linha_prod.substring(18, 43).trim());
                                produto.setDescricao2(linha_prod.substring(43, 68).trim());

                                if (mapa_receita.size() > 0) {
                                    produto.setIngredientes(mapa_receita.get(Integer.valueOf(linha_prod.substring(68, 74))));
                                }

                                //verificar qual a versao 1,2,3
                                switch (linha_prod.length()) {
                                    case 113:
                                        //versao 1
                                        codnutricional = Integer.valueOf(linha_prod.substring(77, 81));
                                        codfornecedor = Integer.valueOf(linha_prod.substring(83, 87));
                                        try {
                                            itemcode = Integer.valueOf(linha_prod.substring(99, 110));
                                        } catch (NumberFormatException e) {
                                            itemcode = produto.getCodigo();
                                        }
                                        break;
                                    case 150:
                                    case 156:
                                        //versao 2
                                        codnutricional = Integer.valueOf(linha_prod.substring(78, 84));
                                        codfornecedor = Integer.valueOf(linha_prod.substring(85, 89));
                                        try {
                                            itemcode = Integer.valueOf(linha_prod.substring(101, 112));
                                        } catch (NumberFormatException e) {
                                            itemcode = produto.getCodigo();
                                        }
                                        break;
                                    case 224:
                                        //versao3
                                        codnutricional = Integer.valueOf(linha_prod.substring(78, 84));
                                        codfornecedor = Integer.valueOf(linha_prod.substring(85, 89));
                                        try {
                                            itemcode = Integer.valueOf(linha_prod.substring(101, 112));
                                        } catch (NumberFormatException e) {
                                            itemcode = produto.getCodigo();
                                        }
                                        break;
                                    default:
                                        break;
                                }

                                if (mapa_nutri.size() > 0 && codnutricional != 0) {
                                    capturarNutricional(produto, mapa_nutri.get(codnutricional));
                                }

                                if (mapa_forn.size() > 0 && codfornecedor != 0) {
                                    produto.setMsgEspecial(mapa_forn.get(codfornecedor));
                                }

                            } else {

                                //TXITENS
                                codsetor = Integer.valueOf(linha_prod.substring(0, 2));

                                produto.setPesado(linha_prod.substring(4, 5).equals("0"));
                                produto.setCodigo(Integer.valueOf(linha_prod.substring(5, 11)));
                                produto.setPreco(BigDecimal.valueOf(Double.valueOf(linha_prod.substring(11, 17)) / 100));
                                produto.setValidadeDias(Integer.valueOf(linha_prod.substring(17, 20)));
                                produto.setDescricao(linha_prod.substring(20, 45).trim());
                                //Utilizando substring seguro da Utilidades
                                produto.setDescricao2(Utilidades.substring(linha_prod, 45, 70).trim());

                                //alterando para aceitar arquivos que não completam com espaço o fim da linha
                                //if (linha_prod.length() == 320) {
                                produto.setIngredientes(Utilidades.substring(linha_prod, 70, 120).trim() + (!Utilidades.substring(linha_prod, 70, 120).trim().isEmpty() ? "\n" : "")
                                        + Utilidades.substring(linha_prod, 120, 170).trim() + (!Utilidades.substring(linha_prod, 120, 170).trim().isEmpty() ? "\n" : "")
                                        + Utilidades.substring(linha_prod, 170, 220).trim() + (!Utilidades.substring(linha_prod, 170, 220).trim().isEmpty() ? "\n" : "")
                                        + Utilidades.substring(linha_prod, 220, 270).trim() + (!Utilidades.substring(linha_prod, 220, 270).trim().isEmpty() ? "\n" : "")
                                        + Utilidades.substring(linha_prod, 270, 320).trim());

                                itemcode = produto.getCodigo();

                            }

                            if (produto.isPesado()) {
                                produto.setCodigoBarras(loja.getCodigobarrasPesado());
                                produto.setEtiqueta(loja.getEtiquetaPesado());
                                produto.setFlag(loja.getFlagPesado());
                            } else {
                                produto.setCodigoBarras(loja.getCodigobarrasUnitario());
                                produto.setEtiqueta(loja.getEtiquetaUnitario());
                                produto.setFlag(loja.getFlagUnitario());
                            }

                            //faz verificacao do itemcode com o tamanho do codigo no formato de codiga de barras                            
                            if (itemcode != null) {
                                if (((long) Math.log10(itemcode)) + 1 > produto.getCodigoBarras().getTamanhocodigo()) {
                                    updateMessage("Erro. Código Item excede o tamanho no código de barras(" + itemcode + ")");
                                    return false;
                                }
                                produto.setItemCodigo(itemcode);
                            }

                            /*
                            produto.setImprimirPacotePorData(true);
                            produto.setImprimirPacotePorHora(true);
                            produto.setImprimirVendaPorData(true);
                            produto.setImprimirVendaPorHora(true);
                            produto.setRtcVendaPorHora(true);
                            produto.setRtcPacotePorHora(true);

                             */
                            //validade selecionada somente se for diferente de 0
                            produto.setImprimirValidade(produto.getValidadeDias() != null && produto.getValidadeDias() > 0);

                            String nome = "Grupo " + codsetor;
                            Optional<Grupoproduto> grupo = grupos.parallelStream().filter((grp) -> (grp.getDescricao().equals(nome))).findAny();
                            if (!grupo.isPresent()) {
                                //cria grupo de produto
                                Grupoproduto valor = new Grupoproduto();
                                valor.setDescricao(nome.trim());
                                daoGrupoProduto.create(valor);

                                //cria departamento
                                Departamento depto = new Departamento();
                                depto.setDescricao("Departamento " + codsetor);
                                depto.setGrupoprodutoSet(new LinkedHashSet<>(Arrays.asList(valor)));
                                daoDepartamento.create(depto);

                                grupos.add(valor);
                                produto.setGrupoproduto(valor);
                            } else {
                                produto.setGrupoproduto(grupo.get());
                            }

                            //adiciona o produto na lista ou no map
                            lista_prod.add(produto);
                        }
                    }

                    linhas.clear();
                    grupos.clear();

                    updateMessage("Produtos ok!");
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    updateMessage("Ocorreu um erro processando arquivo de produtos.");
                    return false;
                }
            } else {
                updateMessage("Arquivo de produtos não encontrado!");
                updateProgress(1, 1);
                return false;
            }

            if (!persistListaProduto(lista_prod)) {
                return false;
            }

            lista_prod.clear();
            mapa_forn.clear();
            mapa_nutri.clear();
            mapa_receita.clear();

            updateMessage("Importação concluída !");
            return true;
        } else {
            updateMessage("Diretorio não existe.");
            updateProgress(1, 1);
            return false;
        }
    }

    private void capturarNutricional(final Produto prod, String linha) {
        if (prod != null && linha != null && linha.length() >= 38) {
            //descarte baseado no labelnet antigo
            if (linha.length() != 68) {
                if (linha.length() == 69) {
                    //formato rdc 39/40
                    //TODO atualizar indices de acordo com o labelnet antigo
                    /*
                    String porcao = Integer.valueOf(linha.substring(8, 11)).toString();
                    switch (linha.substring(11, 12)) {
                        case "0":
                            porcao = porcao + " g";
                            break;
                        case "1":
                            porcao = porcao + " ml";
                            break;
                        case "2":
                            porcao = porcao + " un";
                            break;
                    }
                    prod.setPorcao(porcao);
                    prod.setCalorias(Integer.valueOf(linha.substring(17, 21)));
                    prod.setCarboidratos(BigDecimal.valueOf(Double.valueOf(linha.substring(21, 25)) / 10));
                    prod.setProteinas(BigDecimal.valueOf(Double.valueOf(linha.substring(25, 28)) / 10));
                    prod.setGordurastotais(BigDecimal.valueOf(Double.valueOf(linha.substring(28, 31)) / 10));
                    prod.setGordurassaturadas(BigDecimal.valueOf(Double.valueOf(linha.substring(31, 34)) / 10));
                    prod.setGordurastrans(BigDecimal.valueOf(Double.valueOf(linha.substring(34, 37)) / 10));
                    prod.setFibraalimentar(BigDecimal.valueOf(Double.valueOf(linha.substring(37, 41)) / 10));
                    prod.setSodio(BigDecimal.valueOf(Double.valueOf(linha.substring(41, 46)) / 10));
                     */
                } else if (linha.length() == 45) {
                    //formato rdc 360
                    String porcao = Integer.valueOf(linha.substring(8, 11)).toString();
                    switch (linha.substring(11, 12)) {
                        case "0":
                            porcao = porcao + " g";
                            break;
                        case "1":
                            porcao = porcao + " ml";
                            break;
                        case "2":
                            porcao = porcao + " un";
                            break;
                    }
                    prod.setPorcao(porcao);
                    prod.setCalorias(Integer.valueOf(linha.substring(17, 21)));
                    prod.setCarboidratos(BigDecimal.valueOf(Double.valueOf(linha.substring(21, 25)) / 10));
                    prod.setProteinas(BigDecimal.valueOf(Double.valueOf(linha.substring(25, 28)) / 10));
                    prod.setGordurastotais(BigDecimal.valueOf(Double.valueOf(linha.substring(28, 31)) / 10));
                    prod.setGordurassaturadas(BigDecimal.valueOf(Double.valueOf(linha.substring(31, 34)) / 10));
                    prod.setGordurastrans(BigDecimal.valueOf(Double.valueOf(linha.substring(34, 37)) / 10));
                    prod.setFibraalimentar(BigDecimal.valueOf(Double.valueOf(linha.substring(37, 41)) / 10));
                    prod.setSodio(BigDecimal.valueOf(Double.valueOf(linha.substring(41, 45)) / 10));
                }
            }
        }
    }

    @Override
    boolean verificarAlteracoesArquivos() {
        if (!importacao.getHashs().isEmpty()) {
            String[] hashs = importacao.getHashs().split(";", 5);
            if (hashs.length == 5) {

                File arq_item = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO);
                if (arq_item != null) {
                    if (hashs[0].isEmpty() || !Utilidades.getMD5Checksum(arq_item.getAbsolutePath()).equals(hashs[0])) {
                        return true;
                    }
                }
                File arq_nutri = getArquivoCaseInsensitive(ARQ_NUTRI_TOLEDO);
                if (arq_nutri != null) {
                    if (hashs[1].isEmpty() || !Utilidades.getMD5Checksum(arq_nutri.getAbsolutePath()).equals(hashs[1])) {
                        return true;
                    }
                }
                File arq_itens2 = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO_2);
                if (arq_itens2 != null) {
                    if (hashs[2].isEmpty() || !Utilidades.getMD5Checksum(arq_itens2.getAbsolutePath()).equals(hashs[2])) {
                        return true;
                    }
                }

                File arq_info = getArquivoCaseInsensitive(ARQ_INFOITENS_TOLEDO);
                if (arq_info != null) {
                    if (hashs[3].isEmpty() || !Utilidades.getMD5Checksum(arq_info.getAbsolutePath()).equals(hashs[3])) {
                        return true;
                    }
                }

                File arq_forn = getArquivoCaseInsensitive(ARQ_FORN_TOLEDO);
                if (arq_forn != null) {
                    if (hashs[4].isEmpty() || !Utilidades.getMD5Checksum(arq_forn.getAbsolutePath()).equals(hashs[4])) {
                        return true;
                    }
                }

                return false;
            }
        }
        //caso o hash esteja vazio mas os arquivos existem,então retornar ok
        return (getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO) != null) || (getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO_2) != null);
    }

    @Override
    void moverArquivos(File diretorio) {
        moveFileTo(ARQ_ITENS_TOLEDO, importacao.getPosDiretorio());
        moveFileTo(ARQ_NUTRI_TOLEDO, importacao.getPosDiretorio());
        moveFileTo(ARQ_ITENS_TOLEDO_2, importacao.getPosDiretorio());
        moveFileTo(ARQ_INFOITENS_TOLEDO, importacao.getPosDiretorio());
        moveFileTo(ARQ_FORN_TOLEDO, importacao.getPosDiretorio());
    }

    @Override
    void renomearArquivos() {
        renameFileTo(ARQ_NUTRI_TOLEDO, ARQ_NUTRI_TOLEDO_OLD);
        renameFileTo(ARQ_ITENS_TOLEDO, ARQ_ITENS_TOLEDO_OLD);
        renameFileTo(ARQ_ITENS_TOLEDO_2, ARQ_ITENS_TOLEDO_2_OLD);
        renameFileTo(ARQ_INFOITENS_TOLEDO, ARQ_INFOITENS_TOLEDO_OLD);
        renameFileTo(ARQ_FORN_TOLEDO, ARQ_FORN_TOLEDO_OLD);
    }

    @Override
    void salvarHashs() {
        String hashs = "";

        File arq = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQ_NUTRI_TOLEDO);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQ_ITENS_TOLEDO_2);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQ_INFOITENS_TOLEDO);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        arq = getArquivoCaseInsensitive(ARQ_FORN_TOLEDO);
        if (arq != null) {
            hashs = hashs + Utilidades.getMD5Checksum(arq.getAbsolutePath());
        }
        hashs = hashs + ";";

        importacao.setHashs(hashs);
        try {
            daoImportacao.edit(importacao);
        } catch (Exception ex) {
            Logger.getLogger(ImportacaoToledo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
