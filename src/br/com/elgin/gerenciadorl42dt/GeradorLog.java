/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt;

import br.com.elgin.gerenciadorl42dt.DAO.LogJpaController;
import br.com.elgin.gerenciadorl42dt.model.Log;
import java.util.Date;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class GeradorLog {

    private static LogJpaController daoLog = new LogJpaController(GerenciadorBaseDados.getEntityManagerFactory());

    public static void logMensagemUsuario(String mensagem) {
        logMensagem(Log.TIPO_USUARIO, mensagem);
    }

    public static void logMensagemSistema(String mensagem) {
        logMensagem(Log.TIPO_SISTEMA, mensagem);
    }

    public static void logMensagem(Integer tipo, String mensagem) {
        Log log = new Log();
        log.setTipo(tipo);
        log.setMensagem(mensagem);
        log.setDate(new Date());
        if (tipo == Log.TIPO_USUARIO) {
            log.setUsuario(SessaoAtual.getUsuario().getUsuario());
        }
        daoLog.create(log);
    }

}
