/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Configuracao {

    public static final File ARQUIVOCONFIG = new File("./config.properties");
    private static final BooleanProperty SERVIDOR = new SimpleBooleanProperty();
    private static final BooleanProperty MULTILOJA = new SimpleBooleanProperty();
    private static final BooleanProperty SERVICO = new SimpleBooleanProperty();
    private static final StringProperty DESCLOJA = new SimpleStringProperty();
    private static final IntegerProperty CODLOJA = new SimpleIntegerProperty();
    private static final IntegerProperty TIMEOUTCONEXAO = new SimpleIntegerProperty();
    private static final BooleanProperty MODOCONTINUO = new SimpleBooleanProperty();
    private static final IntegerProperty INTENSIDADEIMPRESSAO = new SimpleIntegerProperty();
    
    //* Paulo Gervilla - 1.3.5
    private static final BooleanProperty LOGINAUTOMATICO = new SimpleBooleanProperty();    
    //Será carregado somente na inicializacao. Para poder comparar valor antigo com valor novo de LOGINAUTOMATICO
    private static final BooleanProperty LOGINAUTOMATICOTMP = new SimpleBooleanProperty();
    //*

    private static final byte[] KEY = new byte[]{0x4a, 0x2a, 0x1e, 0x62, 0x11, 0x02, 0x0d, 0x1b, 0x5a, 0x4a, 0x01, 0x2c, 0x3d, 0x2a, 0x13, 0x5b};
    private static final SecretKey SECRETKEY = new SecretKeySpec(KEY, "AES");

    public static boolean isServidor() {
        return SERVIDOR.get();
    }

    public static void setServidor(boolean value) {
        SERVIDOR.set(value);
    }

    public static BooleanProperty servidorProperty() {
        return SERVIDOR;
    }

    public static boolean isMultiloja() {
        return MULTILOJA.get();
    }

    public static void setMultiloja(boolean value) {
        MULTILOJA.set(value);
    }

    public static BooleanProperty multilojaProperty() {
        return MULTILOJA;
    }

    public static boolean isServico() {
        return SERVICO.get();
    }

    public static void setServico(boolean value) {
        SERVICO.set(value);
    }

    public static BooleanProperty servicoProperty() {
        return SERVICO;
    }

    public static String getDescLoja() {
        return DESCLOJA.get();
    }

    public static void setDescLoja(String value) {
        DESCLOJA.set(value);
    }

    public static StringProperty descLojaProperty() {
        return DESCLOJA;
    }

    public static int getTimeoutConexao() {
        return TIMEOUTCONEXAO.get();
    }

    public static void setTimeoutConexao(int value) {
        TIMEOUTCONEXAO.set(value);
    }

    public static IntegerProperty timeoutConexaoProperty() {
        return TIMEOUTCONEXAO;
    }

    public static int getCodLoja() {
        return CODLOJA.get();
    }

    public static void setCodLoja(int value) {
        CODLOJA.set(value);
    }    

    public static IntegerProperty CodLojaProperty() {
        return CODLOJA;
    }

    public static boolean isModoContinuo() {
        return MODOCONTINUO.get();
    }

    public static void setModoContinuo(boolean value) {
        MODOCONTINUO.set(value);
    }

    public static BooleanProperty modoContinuoProperty() {
        return MODOCONTINUO;
    }
    
    public static int getIntensidadeImpressao() {
        return INTENSIDADEIMPRESSAO.get();
    }

    public static void setIntensidadeImpressao(int value) {
        INTENSIDADEIMPRESSAO.set(value);
    }
    
    //------------------ Login Automatico
    //* Paulo Gervilla - 1.3.5
    public static boolean isLoginAutomatico() {
        return LOGINAUTOMATICO.get();
    }

    public static void setLoginautomatico(boolean value) {
        LOGINAUTOMATICO.set(value);
    }

    public static BooleanProperty loginautomaticoProperty() {
        return LOGINAUTOMATICO;
    }
    
    public static boolean isLoginAutomaticoTmp() {
        return LOGINAUTOMATICOTMP.get();
    }

    public static void setLoginautomaticoTmp(boolean value) {
        LOGINAUTOMATICOTMP.set(value);
    }

    public static BooleanProperty loginautomaticotmpProperty() {
        return LOGINAUTOMATICOTMP;
    }
    //*
    //---------------------------------

    public static void salvarProps(Properties properties) {
        try {
            Cipher cipher = Cipher.getInstance("AES", "SunJCE");
            cipher.init(Cipher.ENCRYPT_MODE, SECRETKEY);

            try (OutputStream out = new CipherOutputStream(Base64.getEncoder().wrap(new FileOutputStream(ARQUIVOCONFIG)), cipher)) {
                properties.store(out, "");
            }

            /*
            try (OutputStream out = new FileOutputStream(ARQUIVOCONFIG)) {
                properties.store(out, "");
            }
             */
        } catch (Exception ex) {
            Logger.getLogger(Configuracao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void salvar() {

        Properties props = new Properties();
        props.setProperty("Servidor", String.valueOf(servidorProperty().get()));
        props.setProperty("Multiloja", String.valueOf(multilojaProperty().get()));
        props.setProperty("LojaID", String.valueOf(CODLOJA.get()));
        props.setProperty("LojaDesc", String.valueOf(DESCLOJA.get()));
        props.setProperty("TimeoutConexao", String.valueOf(TIMEOUTCONEXAO.get()));
        props.setProperty("ModoContinuo", String.valueOf(MODOCONTINUO.get()));
        props.setProperty("IntensidadeImpressao", String.valueOf(INTENSIDADEIMPRESSAO.get()));
        //* Paulo Gervilla - 1.3.5
        props.setProperty("LoginAutomatico", String.valueOf(LOGINAUTOMATICO.get()));
        //*

        salvarProps(props);

    }

    public static void carregar() {
        try {
            Properties props = new Properties();

            Cipher cipher = Cipher.getInstance("AES", "SunJCE");
            cipher.init(Cipher.DECRYPT_MODE, SECRETKEY);

            InputStream is = new CipherInputStream(Base64.getDecoder().wrap(new FileInputStream(ARQUIVOCONFIG)), cipher);
            //InputStream is = new FileInputStream(ARQUIVOCONFIG);

            props.load(is);

            //aqui carrega todos os valores a partir do arquivo
            setServidor(Boolean.parseBoolean(props.getProperty("Servidor", String.valueOf(SERVIDOR.get()))));
            setMultiloja(Boolean.parseBoolean(props.getProperty("Multiloja", String.valueOf(MULTILOJA.get()))));
            setCodLoja(Integer.parseInt(props.getProperty("LojaID", "0")));
            setDescLoja(props.getProperty("LojaDesc", ""));
            setTimeoutConexao(Integer.parseInt(props.getProperty("TimeoutConexao", "5000")));
            setModoContinuo(Boolean.parseBoolean(props.getProperty("ModoContinuo", "false")));
            setIntensidadeImpressao(Integer.parseInt(props.getProperty("IntensidadeImpressao", "8")));
            //* Paulo Gervilla - 1.3.5
            setLoginautomatico(Boolean.parseBoolean(props.getProperty("LoginAutomatico", "false")));
            //*

        } catch (Exception ex) {
            Logger.getLogger(Configuracao.class.getName()).log(Level.SEVERE, null, ex);

            //aqui seta todos os valores padrao quando da erro
            //setServidor(true);
            //setMultiloja(true);
            //setEnderecoServidor("localhost");
            //setPortaServidor(1527);
            setModoContinuo(false);
            setIntensidadeImpressao(8);
            setTimeoutConexao(5000);

        }
    }

}
