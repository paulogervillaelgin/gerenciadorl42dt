/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Dialog
        extends Stage {

    protected String stacktrace;
    protected double originalWidth;
    protected double originalHeight;
    protected Scene scene;
    protected BorderPane borderPanel;
    protected ImageView icon;
    protected VBox messageBox;
    protected Label messageLabel;
    protected boolean stacktraceVisible;
    protected HBox stacktraceButtonsPanel;
    protected ToggleButton viewStacktraceButton;
    protected Button copyStacktraceButton;
    protected ScrollPane scrollPane;
    protected Label stackTraceLabel;
    protected HBox buttonsPanel;
    protected Button okButton;
    protected double initialX;
    protected double initialY;
    protected AnchorPane painelTitulo;
    protected Label labelTitulo;

    protected static class StacktraceExtractor {

        public String extract(Throwable t) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            return sw.toString();
        }
    }

    public static class Builder {

        protected static final int STACKTRACE_LABEL_MAXHEIGHT = 240;
        protected static final int MESSAGE_MIN_WIDTH = 180;
        protected static final int MESSAGE_MAX_WIDTH = 800;
        protected static final int BUTTON_WIDTH = 60;
        protected static final double MARGIN = 10.0D;
        protected static final String ICON_PATH = "/";
        protected Dialog stage;

        public Builder create() {
            this.stage = new Dialog();
            this.stage.setResizable(false);
            this.stage.initStyle(StageStyle.TRANSPARENT);
            this.stage.initModality(Modality.APPLICATION_MODAL);
            this.stage.setIconified(false);
            this.stage.centerOnScreen();
            this.stage.setAlwaysOnTop(true);
            this.stage.borderPanel = new BorderPane();
            stage.borderPanel.getStyleClass().add("frame_dialog");
            stage.borderPanel.getStyleClass().add("frame_radius_all");

            stage.borderPanel.setOnMousePressed((MouseEvent me) -> {
                if (me.getButton() != MouseButton.MIDDLE) {
                    stage.initialX = me.getSceneX();
                    stage.initialY = me.getSceneY();
                }
            });

            stage.borderPanel.setOnMouseDragged((MouseEvent me) -> {
                if (me.getButton() != MouseButton.MIDDLE) {
                    stage.borderPanel.getScene().getWindow().setX(me.getScreenX() - stage.initialX);
                    stage.borderPanel.getScene().getWindow().setY(me.getScreenY() - stage.initialY);
                }
            });

            this.stage.icon = new ImageView();
            this.stage.borderPanel.setLeft(this.stage.icon);
            BorderPane.setMargin(this.stage.icon, new Insets(20D,10D,10D,15D));

            this.stage.messageBox = new VBox();
            this.stage.messageBox.setAlignment(Pos.CENTER_LEFT);

            this.stage.messageLabel = new Label();
            this.stage.messageLabel.setWrapText(true);
            this.stage.messageLabel.setMinWidth(180.0D);
            this.stage.messageLabel.setMaxWidth(800.0D);
            stage.messageLabel.setTextFill(Paint.valueOf("black"));

            this.stage.painelTitulo = new AnchorPane();
            stage.painelTitulo.setPrefHeight(25);
            stage.painelTitulo.getStyleClass().add("frame_dialog_titulo");
            stage.borderPanel.setTop(stage.painelTitulo);
            
            stage.labelTitulo = new Label();
            stage.labelTitulo.textProperty().bind(stage.titleProperty());
            stage.labelTitulo.setTextFill(Paint.valueOf("white"));
            stage.labelTitulo.setFont(Font.font("roboto", FontWeight.BOLD , 12));
            stage.painelTitulo.getChildren().add(stage.labelTitulo);
            AnchorPane.setLeftAnchor(stage.labelTitulo, 5.0);
            AnchorPane.setBottomAnchor(stage.labelTitulo, 1.0);

            this.stage.messageBox.getChildren().add(this.stage.messageLabel);
            this.stage.borderPanel.setCenter(this.stage.messageBox);
            BorderPane.setAlignment(this.stage.messageBox, Pos.CENTER);
            BorderPane.setMargin(this.stage.messageBox, new Insets(10.0D, 10.0D, 10.0D, 20.0D));

            this.stage.buttonsPanel = new HBox();
            this.stage.buttonsPanel.setSpacing(10.0D);
            this.stage.buttonsPanel.setAlignment(Pos.BOTTOM_CENTER);
            BorderPane.setMargin(this.stage.buttonsPanel, new Insets(0.0D, 0.0D, 15.0D, 0.0D));
            this.stage.borderPanel.setBottom(this.stage.buttonsPanel);
            this.stage.borderPanel.widthProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
                Dialog.Builder.this.stage.buttonsPanel.layout();
            });
            this.stage.scene = new Scene(this.stage.borderPanel);
            this.stage.scene.getStylesheets().add("/styles/Styles.css");
            stage.scene.setFill(null);

            this.stage.setScene(this.stage.scene);
            return this;
        }

        public Builder setOwner(Window owner) {
            if (owner != null) {
                this.stage.initOwner(owner);
                this.stage.borderPanel.setMaxWidth(owner.getWidth());
                this.stage.borderPanel.setMaxHeight(owner.getHeight());
            }
            return this;
        }

        public Builder setTitle(String title) {
            this.stage.setTitle(title);
            return this;
        }

        public Builder setMessage(String message) {
            this.stage.messageLabel.setText(message);
            return this;
        }

        private void alignScrollPane() {
            this.stage.setWidth(this.stage.icon
                    .getImage().getWidth()
                    + Math.max(this.stage.messageLabel
                            .getWidth(), this.stage.stacktraceVisible
                                    ? Math.max(this.stage.stacktraceButtonsPanel
                                            .getWidth(), this.stage.stackTraceLabel
                                                    .getWidth()) : this.stage.stacktraceButtonsPanel
                                            .getWidth()) + 50.0D);
            this.stage.setHeight(
                    Math.max(this.stage.icon
                            .getImage().getHeight(), this.stage.messageLabel
                                    .getHeight() + this.stage.stacktraceButtonsPanel
                                    .getHeight() + (this.stage.stacktraceVisible
                                    ? Math.min(this.stage.stackTraceLabel
                                            .getHeight(), 240.0D) : 0.0D)) + this.stage.buttonsPanel
                            .getHeight() + 30.0D);
            if (this.stage.stacktraceVisible) {
                this.stage.scrollPane.setPrefHeight(this.stage
                        .getHeight() - this.stage.messageLabel
                                .getHeight() - this.stage.stacktraceButtonsPanel
                                .getHeight() - 20.0D);
            }
            this.stage.centerOnScreen();
        }

        private Builder setStackTrace(Throwable t) {
            this.stage.viewStacktraceButton = new ToggleButton("View stacktrace");

            this.stage.copyStacktraceButton = new Button("Copy to clipboard");
            HBox.setMargin(this.stage.copyStacktraceButton, new Insets(0.0D, 0.0D, 0.0D, 10.0D));

            this.stage.stacktraceButtonsPanel = new HBox();
            this.stage.stacktraceButtonsPanel.getChildren().addAll(new Node[]{this.stage.viewStacktraceButton, this.stage.copyStacktraceButton});

            VBox.setMargin(this.stage.stacktraceButtonsPanel, new Insets(10.0D, 10.0D, 10.0D, 0.0D));
            this.stage.messageBox.getChildren().add(this.stage.stacktraceButtonsPanel);

            this.stage.stackTraceLabel = new Label();
            this.stage.stackTraceLabel.widthProperty().addListener((ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
                Dialog.Builder.this.alignScrollPane();
            });
            this.stage.stackTraceLabel.heightProperty().addListener((ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
                Dialog.Builder.this.alignScrollPane();
            });
            Dialog.StacktraceExtractor extractor = new Dialog.StacktraceExtractor();
            this.stage.stacktrace = extractor.extract(t);

            this.stage.scrollPane = new ScrollPane();
            this.stage.scrollPane.setContent(this.stage.stackTraceLabel);

            this.stage.viewStacktraceButton.setOnAction((ActionEvent t1) -> {
                Dialog.Builder.this.stage.stacktraceVisible = (!Dialog.Builder.this.stage.stacktraceVisible);
                if (Dialog.Builder.this.stage.stacktraceVisible) {
                    Dialog.Builder.this.stage.messageBox.getChildren().add(Dialog.Builder.this.stage.scrollPane);
                    Dialog.Builder.this.stage.stackTraceLabel.setText(Dialog.Builder.this.stage.stacktrace);

                    Dialog.Builder.this.alignScrollPane();
                } else {
                    Dialog.Builder.this.stage.messageBox.getChildren().remove(Dialog.Builder.this.stage.scrollPane);

                    Dialog.Builder.this.stage.setWidth(Dialog.Builder.this.stage.originalWidth);
                    Dialog.Builder.this.stage.setHeight(Dialog.Builder.this.stage.originalHeight);
                    Dialog.Builder.this.stage.stackTraceLabel.setText(null);
                    Dialog.Builder.this.stage.centerOnScreen();
                }
                Dialog.Builder.this.stage.messageBox.layout();
            });
            this.stage.copyStacktraceButton.setOnAction((ActionEvent t1) -> {
                Clipboard clipboard = Clipboard.getSystemClipboard();
                Map<DataFormat, Object> map = new HashMap();
                map.put(DataFormat.PLAIN_TEXT, Dialog.Builder.this.stage.stacktrace);
                clipboard.setContent(map);
            });
            this.stage.showingProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                if (newValue) {
                    Dialog.Builder.this.stage.originalWidth = Dialog.Builder.this.stage.getWidth();
                    Dialog.Builder.this.stage.originalHeight = Dialog.Builder.this.stage.getHeight();
                }
            });
            return this;
        }

        protected void setIconFromResource(String resourceName) {
            Image image = new Image(getClass().getResourceAsStream(resourceName));
            this.stage.icon.setImage(image);
        }

        protected Builder setWarningIcon() {
            stage.borderPanel.getStyleClass().add("frame_border_blue");

//stage.borderPanel.getStyleClass().add("frame_border_yellow");
            stage.icon.getStyleClass().add("sombra_yellow");
            setIconFromResource("/images/dialog/warningIcon.png");
            return this;
        }

        protected Builder setErrorIcon() {
            stage.borderPanel.getStyleClass().add("frame_border_blue");
//stage.borderPanel.getStyleClass().add("frame_border_red");

            stage.icon.getStyleClass().add("sombra_red");
            setIconFromResource("/images/dialog/errorIcon.png");
            return this;
        }

        protected Builder setThrowableIcon() {
            setIconFromResource("/images/dialog/bugIcon.png");
            return this;
        }

        protected Builder setInfoIcon() {
            stage.borderPanel.getStyleClass().add("frame_border_blue");
            stage.icon.getStyleClass().add("sombra_blue");
            setIconFromResource("/images/dialog/infoIcon.png");
            return this;
        }

        protected Builder setConfirmationIcon() {
            stage.borderPanel.getStyleClass().add("frame_border_blue");
            stage.icon.getStyleClass().add("sombra_blue");
            setIconFromResource("/images/dialog/confirmationIcon.png");
            return this;
        }

        protected Builder addOkButton() {
            this.stage.okButton = new Button("OK");
            this.stage.okButton.getStyleClass().add("botao_menu");
            this.stage.okButton.setPrefWidth(60.0D);
            this.stage.okButton.setOnAction((ActionEvent t) -> {
                Dialog.Builder.this.stage.close();
            });
            this.stage.buttonsPanel.getChildren().add(this.stage.okButton);
            return this;
        }

        protected Builder addConfirmationButton(String buttonCaption, final EventHandler actionHandler) {
            Button confirmationButton = new Button(buttonCaption);
            confirmationButton.setMinWidth(60.0D);
            confirmationButton.getStyleClass().add("botao_menu");
            confirmationButton.setOnAction((ActionEvent t) -> {
                Dialog.Builder.this.stage.close();
                if (actionHandler != null) {
                    actionHandler.handle(t);
                }
            });
            this.stage.buttonsPanel.getChildren().add(confirmationButton);
            return this;
        }

        public Builder addYesButton(EventHandler actionHandler) {
            return addConfirmationButton("Sim", actionHandler);
        }

        public Builder addNoButton(EventHandler actionHandler) {
            return addConfirmationButton("Não", actionHandler);
        }

        public Builder addCancelButton(EventHandler actionHandler) {
            return addConfirmationButton("Cancelar", actionHandler);
        }

        public Dialog build() {
            if (this.stage.buttonsPanel.getChildren().isEmpty()) {
                throw new RuntimeException("Add one dialog button at least");
            }
            ((Node) this.stage.buttonsPanel.getChildren().get(0)).requestFocus();
            return this.stage;
        }
    }

    public static void showInfo(String title, String message, Window owner) {
        new Builder().create().setOwner(owner).setTitle(title).setInfoIcon().setMessage(message).addOkButton().build().showAndWait();
    }

    public static void showInfo(String title, String message) {
        showInfo(title, message, null);
    }

    public static void showWarning(String title, String message, Window owner) {
        new Builder().create().setOwner(owner).setTitle(title).setWarningIcon().setMessage(message).addOkButton().build().showAndWait();
    }

    public static void showWarning(String title, String message) {
        showWarning(title, message, null);
    }

    public static void showError(String title, String message, Window owner) {
        new Builder().create().setOwner(owner).setTitle(title).setErrorIcon().setMessage(message).addOkButton().build().showAndWait();
    }

    public static void showError(String title, String message) {
        showError(title, message, null);
    }

    public static void showThrowable(String title, String message, Throwable t, Window owner) {
        new Builder().create().setOwner(owner).setTitle(title).setThrowableIcon().setMessage(message).setStackTrace(t).addOkButton().build().show();
    }

    public static void showThrowable(String title, String message, Throwable t) {
        showThrowable(title, message, t, null);
    }

    public static Builder buildConfirmation(String title, String message, Window owner) {
        return new Builder().create().setOwner(owner).setTitle(title).setConfirmationIcon().setMessage(message);
    }

    public static Builder buildConfirmation(String title, String message) {
        return buildConfirmation(title, message, null);
    }
}
