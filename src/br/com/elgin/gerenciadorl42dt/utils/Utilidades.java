/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Utilidades {

    private static final DecimalFormat FORMATOMOEDA = new java.text.DecimalFormat("R$ ###,##0.00");
    private static final DecimalFormat FORMATOPESO = new java.text.DecimalFormat("###,##0.000");

    private static final DecimalFormat FORMATO_NUTRI_INTEIRO = new java.text.DecimalFormat("##0");
    private static final DecimalFormat FORMATO_NUTRI_DECIMAL = new java.text.DecimalFormat("##0.0");

    private static final String IP_PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    //verifica se um IP é valido ou não
    public static boolean isIPValido(final String endereco) {
        /*
        //alterado pq antes permitia nome DNS
        try {
            InetAddress addr = InetAddress.getByName(endereco);
            return true;
        } catch (UnknownHostException e) {
        }
        return false;
         */
        return endereco.matches(IP_PATTERN);
    }

    //função para calcular o hash SHA2 de uma string
    public static String toSHA256(final String valor) {
        MessageDigest algorithm;
        StringBuilder hexString = new StringBuilder();
        try {

            algorithm = MessageDigest.getInstance("SHA-256");

            byte messageDigest[] = algorithm.digest(valor.getBytes("UTF-8"));

            for (byte b : messageDigest) {
                hexString.append(String.format("%02X", 0xFF & b));

            }

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Utilidades.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return hexString.toString();
    }

    //função para calcular hash MD5 de um arquivo
    public static byte[] getMD5(String filename) {
        MessageDigest complete = null;
        try (InputStream fis = new FileInputStream(filename)) {
            byte[] buffer = new byte[1024];
            complete = MessageDigest.getInstance("MD5");
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return complete != null ? complete.digest() : null;
    }

    public static String getMD5Checksum(String filename) {
        byte[] b = getMD5(filename);
        String result = "";

        if (b != null) {
            for (int i = 0; i < b.length; i++) {
                result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
            }
        }

        return result;
    }

    // função que formata valores com a mascara de dinheiro
    public static String formatMoeda(BigDecimal valor) {
        if (valor == null) {
            valor = BigDecimal.ZERO;
        }
        return FORMATOMOEDA.format(valor);
    }

    //funcao que formata valores com a mascara de peso
    public static String formatPeso(BigDecimal valor) {
        if (valor == null) {
            valor = BigDecimal.ZERO;
        }
        return FORMATOPESO.format(valor);
    }

    //função que formata valores com as mascaras de nutricionais
    public static String formatNutri(BigDecimal valor, String unit) {
        if (valor == null) {
            valor = BigDecimal.ZERO;
        }
        if (valor.doubleValue() < 10d) {
            return FORMATO_NUTRI_DECIMAL.format(valor) + " " + unit;
        } else {
            return FORMATO_NUTRI_INTEIRO.format(valor) + " " + unit;
        }
    }

    //Retorna valores de nutricionais de uma string contendo valores e unidade
    public static BigDecimal parseAsNutri(String valor, String unit) {
        try {
            valor = valor.replaceAll(unit, "").trim();
            valor = valor.replaceAll(",", ".");
            if (Double.valueOf(valor) > 10) {
                return BigDecimal.valueOf(Math.round(Double.valueOf(valor).floatValue()));
            } else {
                return BigDecimal.valueOf(Double.valueOf(valor));
            }
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    //retorna valor de moeda contido numa string
    public static BigDecimal parseAsMoeda(String valor) {
        try {
            valor = valor.replaceAll(",", ".");
            return BigDecimal.valueOf(Double.valueOf(valor.replace("R$", "")));
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    //retorna valor de peso contido numa string
    public static BigDecimal parseAsPeso(String valor) {
        try {
            valor = valor.replaceAll(",", ".");
            return BigDecimal.valueOf(Double.valueOf(valor));
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    //retorna valor decimal de string com valor e unidade
    public static BigDecimal parseAsDecimalWithUnit(String valor, String unit) {
        try {
            valor = valor.replaceAll(unit, "");
            valor = valor.replaceAll(",", ".");
            return BigDecimal.valueOf(Double.valueOf(valor));
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    //retorna valor inteiro de string com valor e unidade
    public static Integer parseAsIntegerWithUnit(String valor, String unit) {
        try {
            valor = valor.replaceAll(unit, "").trim();
            return Integer.valueOf(valor);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    //retorna a extensão do arquivo
    public static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    //função para converter uma imagem em vetor de bytes para ser salva no banco de dados
    public static byte[] convertImageToBytes(Image image) {
        if (image != null) {
            BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
            try (ByteArrayOutputStream s = new ByteArrayOutputStream()) {
                ImageIO.write(bImage, "png", s);
                byte[] res = s.toByteArray();
                return res;
            } catch (IOException ex) {
                Logger.getLogger(Utilidades.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    //função para converter um blob bytes do banco de dados em uma imagem
    public static Image convertBytesToImage(byte[] bytes) {
        InputStream in = new ByteArrayInputStream(bytes);
        try {
            BufferedImage bImageFromConvert = ImageIO.read(in);
            return SwingFXUtils.toFXImage(bImageFromConvert, null);
        } catch (IOException ex) {
            Logger.getLogger(Utilidades.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    //funcao usada para verificar se existe uma impressora configurada
    public static boolean verifyPrinter() {
        return Printer.getDefaultPrinter() != null;
    }

    // função sendo usada para imprimir o teclado
    public static void print(final Node node) {
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.DEFAULT);

        //double scaleX = pageLayout.getPrintableWidth() / node.getBoundsInParent().getWidth();
        //double scaleY = pageLayout.getPrintableHeight() / node.getBoundsInParent().getHeight();
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(node.getScene().getWindow())) {

            boolean success = job.printPage(pageLayout, node);
            if (success) {
                job.endJob();
            }

        }

    }

    //função de conversao de tipos de data - Date para LocalTime
    public static LocalTime dateToLocalTime(Date time) {
        try {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(time.getTime()), ZoneId.systemDefault()).toLocalTime();
        } catch (Exception ex) {
            return LocalTime.of(0, 0);
        }
    }

    //funcao que gera um localtime por um short BCD
    public static LocalTime shortToLocalTime(short value) {
        return LocalTime.of((int) value / 100, (int) value % 100);
    }

    //funcao que converte um Date de horario para short BCD
    public static short dateToShort(Date date) {
        return date != null ? (short) (date.getHours() * 100 + date.getMinutes()) : 0x0000;
    }

    //função que converte um LocalTime para um Date
    public static Date localTimeToDate(LocalTime time) {
        return Date.from(
                time.atDate(LocalDate.now())
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
    }

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS"));
    }

    //Adiciona uma string a cada n caracteres em outra string e retorna o valor
    public static String insertPeriodically(String text, String insert, int period) {
        if (text.length() > period) {
            StringBuilder builder = new StringBuilder(
                    text.length() + insert.length() * (text.length() / period) + 1);

            int index = 0;
            String prefix = "";
            while (index < text.length()) {
                // Don't put the insert in the very first iteration.
                // This is easier than appending it *after* each substring
                builder.append(prefix);
                prefix = insert;
                builder.append(text.substring(index,
                        Math.min(index + period, text.length())));
                index += period;
            }
            return builder.toString();
        } else {
            return text;
        }
    }

    //TODO testar no linux, no Windows funcionou normalmente
    //TODO remover o ultimo parametro "arquivo" da função e derivar o mesmo do Path diretorio
    public static File getfileCaseInsensitive(Path diretorio, String arquivo) {
        try {
            //Files.walk(diretorio, 0).forEach((p) -> System.out.println(p.toFile().getName()));            
            Optional<Path> arq = Files.walk(diretorio, 0, FileVisitOption.FOLLOW_LINKS).filter((p) -> p.toFile().isFile() && p.toFile().getName().equalsIgnoreCase(arquivo)).findFirst();
            if (arq.isPresent()) {
                return arq.get().toFile();
            }
        } catch (IOException ex) {
            //Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<String> readAllLines(File arquivo) {

        String[] charsetsToBeTested = {"UTF-8", "ISO-8859-1", "windows-1252"};

        for (String charset : charsetsToBeTested) {

            try {
                return Files.readAllLines(arquivo.toPath(), Charset.forName(charset));
            } catch (IOException ex) {
                //Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
                //erro, tentando outro charset
            }
        }
        return null;

    }
    
        public static String substring(String str, int start, int end) {
        if (str == null) {
            return null;
        }

        // handle negatives
        if (end < 0) {
            end = str.length() + end; // remember end is negative
        }
        if (start < 0) {
            start = str.length() + start; // remember start is negative
        }

        // check length next
        if (end > str.length()) {
            end = str.length();
        }

        // if start is greater than end, return ""
        if (start > end) {
            return "";
        }

        if (start < 0) {
            start = 0;
        }
        if (end < 0) {
            end = 0;
        }

        return str.substring(start, end);
    }


}
