/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ImageHelper {

    private static final Image IMAGEM_NAOENCONTRADO = new Image(ImageHelper.class.getResourceAsStream("/images/naodisp.png"), 263, 263, true, true);

    public static Image getImageOf(FormatoEtiqueta formato) {

        if (formato != null) {
            try {
                File arquivo = new File("./imagens/F" + formato.getIndice() + ".png");
                if (arquivo.exists()) {
                    return new Image(arquivo.toURI().toString(), true);
                }
            } catch (Exception e) {
                Logger.getLogger(ImageHelper.class.getName()).log(Level.SEVERE, null, e);
                return IMAGEM_NAOENCONTRADO;
            }
        }
        return IMAGEM_NAOENCONTRADO;
    }

}
