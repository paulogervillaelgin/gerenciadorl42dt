/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.DAO.DepartamentoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ItemDepartamento extends AnchorPane {

    private final Label lbDescricao = new Label();
    private final Label lbImpressoras = new Label();
    private final Label lbGrupos = new Label();

    private Departamento departamento;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);

    private boolean selecionado = false;

    public boolean isSelecionado() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado = selecionado;
        if (selecionado) {
            getStyleClass().add("item_drag");
        } else {
            getStyleClass().removeAll("item_drag");
        }
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
        updateInfos();
    }

    public void updateInfos() {
        if (getDepartamento() != null) {
            Departamento depto = daoDepartamento.findDepartamento(getDepartamento().getIndice());
            if (depto != null) {
                lbDescricao.setText(depto.getDescricao());
                if (depto.getImpressoraSet() != null) {
                    lbImpressoras.setText(String.valueOf(depto.getImpressoraSet().size()) + " impressoras");
                } else {
                    lbImpressoras.setText("0 impressoras");
                }
                if (depto.getGrupoprodutoSet() != null) {
                    lbGrupos.setText(String.valueOf(depto.getGrupoprodutoSet().size()) + " grupos");
                } else {
                    lbGrupos.setText("0 grupos");
                }
            }
        }
    }

    public ItemDepartamento(Departamento departament) {
        getStyleClass().add("frame_radius_all");
        getStyleClass().add("item");

        setCursor(Cursor.HAND);
        VBox.setMargin(this, new Insets(5.0, 0, 0, 0));
        setPrefSize(180, 45);
        setVisible(true);

        lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        //lbDescricao.setTextFill(Color.WHITE);
        lbDescricao.setStyle("-fx-text-fill:-cor-base;");
        lbDescricao.setVisible(true);
        AnchorPane.setLeftAnchor(lbDescricao, 7.0);
        AnchorPane.setTopAnchor(lbDescricao, 7.0);

        lbImpressoras.setFont(Font.font("Roboto", FontWeight.NORMAL, 12));
        //lbBalancas.setTextFill(Color.WHITE);
        lbImpressoras.setStyle("-fx-text-fill:-cor-base;");
        lbImpressoras.setVisible(true);
        AnchorPane.setRightAnchor(lbImpressoras, 80.0);
        AnchorPane.setBottomAnchor(lbImpressoras, 4.0);

        lbGrupos.setFont(Font.font("Roboto", FontWeight.NORMAL, 12));
        //lbGrupos.setTextFill(Color.WHITE);
        lbGrupos.setStyle("-fx-text-fill:-cor-base;");
        lbGrupos.setVisible(true);
        AnchorPane.setRightAnchor(lbGrupos, 10.0);
        AnchorPane.setBottomAnchor(lbGrupos, 4.0);

        setDepartamento(departament);

        getChildren().addAll(lbDescricao, lbImpressoras, lbGrupos);

    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.departamento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemDepartamento other = (ItemDepartamento) obj;
        if (!Objects.equals(this.departamento, other.departamento)) {
            return false;
        }
        return true;
    }

}
