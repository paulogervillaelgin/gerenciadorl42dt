/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.DAO.ImportacaoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.importacao.ImportacaoDadosExternos;
import br.com.elgin.gerenciadorl42dt.importacao.ServiceImportacao;
import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmConexaoBancoController;
import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmMenuController;
import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmJanelaController;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FormHelper {

    public static final String FORM_MENU = "/fxml/frmMenu.fxml";
    public static final String FORM_LOGIN = "/fxml/frmLogin.fxml";
    public static final String FORM_JANELA = "/fxml/frmJanela.fxml";
    public static final String FORM_BANCODADOS = "/fxml/frmConexaoBanco.fxml";
    public static final String FORM_MONITORAMENTO = "/fxml/frmMonitoramento.fxml";
    public static final String FORM_TRANSMISSAO = "/fxml/frmTransmissao.fxml";
    public static final String FORM_STATUS_TRANSMISSAO = "/fxml/frmStatusTransmissao.fxml";
    public static final String FORM_IMPORTACAO_MANUAL = "/fxml/frmImportacaoManual.fxml";
    public static final String FORM_IMPORTACAO_AUTOMATICA = "/fxml/frmConfigImportacaoAutomatica.fxml";
    public static final String FORM_STATUS_TASK = "/fxml/frmStatusTask.fxml";
    public static final String FORM_CADASTRO_USUARIOS = "/fxml/frmCadastroUsuario.fxml";
    public static final String FORM_CADASTRO_GRUPOS = "/fxml/frmCadastroGrupoProduto.fxml";
    public static final String FORM_ALTERA_SENHA = "/fxml/frmAlteraSenha.fxml";
    public static final String FORM_CADASTRO_PRODUTO = "/fxml/frmCadastroProduto.fxml";
    public static final String FORM_CONSULTA_PRODUTO = "/fxml/frmConsultaProdutos.fxml";
    public static final String FORM_VISUALIZA_LOG = "/fxml/frmVisualizaLog.fxml";
    public static final String FORM_CADASTRO_FLEXIBARCODE = "/fxml/frmCadastrarFlexibarcode.fxml";
    public static final String FORM_CONSULTA_FLEXIBARCODE = "/fxml/frmConsultaFlexiBarcode.fxml";
    public static final String FORM_CONSULTA_DEPARTAMENTO = "/fxml/frmConsultaDepartamento.fxml";
    public static final String FORM_CADASTRO_DEPARTAMENTO = "/fxml/frmCadastroDepartamento.fxml";
    public static final String FORM_ASSOCIA_GRUPO = "/fxml/frmAssociaDepartamentoGrupo.fxml";
    public static final String FORM_CONFIG_LOJA = "/fxml/frmConfigLoja.fxml";
    public static final String FORM_CADASTRO_IMPRESSORA = "/fxml/frmCadastroImpressora.fxml";
    public static final String FORM_PESAGEM_UNITARIOS = "/fxml/frmPesagemUnitarios.fxml";
    public static final String FORM_CADASTRO_USUARIO_INICIAL = "/fxml/frmCadastroUsuarioInicial.fxml";
    //* PRG - 1.3.5
    public static final String FORM_ASSISTENTE_INICIAL_FIM = "/fxml/frmAssistenteInicial_Fim.fxml";
    //*

    private static boolean travado = false;

    public static boolean isTravado() {
        return travado;
    }

    public static void setTravado(boolean travado) {
        FormHelper.travado = travado;
    }

    private static FrmMenuController formMenuController;

    private static FrmJanelaController frmPrincipal;

    public static FrmJanelaController getFrmPrincipal() {
        return frmPrincipal;
    }

    public static void setFrmPrincipal(FrmJanelaController frmPrincipal) {
        FormHelper.frmPrincipal = frmPrincipal;
    }

    public static FrmMenuController getFormMenuController() {
        return formMenuController;
    }

    public static void setFormMenuController(FrmMenuController formMenuController) {
        FormHelper.formMenuController = formMenuController;
    }

    public static Formulario getFormularioAtual() {
        return formularioAtual;
    }

    public static void setFormularioAtual(Formulario formularioAtual) {
        FormHelper.formularioAtual = formularioAtual;
    }
    private static Formulario formularioAtual;

    public static void carregaFormulario(String fxml) {
        if (isTravado()) {
            Dialog.showInfo("Aguarde!", "Aguarde a rotina atual finalizar antes de carregar outros formularios.");
            return;
        }
        try {
            if (formularioAtual != null) {
                formularioAtual.onClose();
            }
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(fxml));
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Node root = (Node) fxmlLoader.load(fxmlLoader.getLocation().openStream());
            if ((fxmlLoader.getController() instanceof Formulario)) {
                formularioAtual = (Formulario) fxmlLoader.getController();
            } else {
                formularioAtual = null;
            }
            formMenuController.exibeFormulario(root);
            //GerenciadorLog.logMensagemUsuario("Entrou na tela : " + titulo);
        } catch (IOException ex) {
            Logger.getLogger(FormHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static FrmJanelaController createJanelaController() {
        return createJanelaController(new Stage());
    }

    public static FrmJanelaController createJanelaController(Stage stage) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FormHelper.class.getResource(FORM_JANELA));
        Parent root;
        try {
            root = loader.load();

            ((FrmJanelaController) loader.getController()).setStage(stage);

            Scene scene = new Scene(root);

            scene.getStylesheets().add("/styles/Styles.css");
            scene.setFill(null);

            stage.initStyle(StageStyle.TRANSPARENT);

            stage.setScene(scene);

            stage.setMaximized(false);

        } catch (IOException ex) {
            Logger.getLogger(FrmMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (FrmJanelaController) loader.getController();

    }

    public static void closeSystem() {
        if (getFrmPrincipal() != null) {
            getFrmPrincipal().getStage().close();
        }
        Platform.exit();
        System.exit(0);
    }

    public static void logoff() {
        SessaoAtual.setUsuario(null);
        if (getFormularioAtual() != null) {
            getFormularioAtual().onClose();
        }

        //TODO tratar pausa no monitoramento aqui tambem
        //ServiceImportacao.getInstance().setHabilitado(false);
        gotoLogin();
    }

    public static void gotoCriaAdm() {
        try {

            Parent root = FXMLLoader.load(FormHelper.class.getResource(FORM_CADASTRO_USUARIO_INICIAL));

            frmPrincipal.setRoot(root);

        } catch (IOException ex) {
            Logger.getLogger(FormHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void gotoLogin() {
        try {

            Parent root = FXMLLoader.load(FormHelper.class.getResource(FORM_LOGIN));

            frmPrincipal.setRoot(root);

        } catch (IOException ex) {
            Logger.getLogger(FormHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void gotoMenu() {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(FORM_MENU));
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Node root = (Node) fxmlLoader.load(fxmlLoader.getLocation().openStream());

            setFormMenuController(fxmlLoader.getController());

            /*
            Animation animacao = Animador.animatePanelControlsX_IN((Pane) root);
            animacao.setOnFinished((event) -> {
                pnStack.getChildren().remove(0);
            });

            Pane outro = (Pane) pnStack.getChildren().get(0);

            Animation animacao_out = Animador.animateTranslateX(outro, Duration.millis(100), Duration.ZERO, Animador.DirecaoAnimacao.OUT);
            animacao_out.setOnFinished((event) -> {
                pnStack.getChildren().add(root);
                animacao.play();
            });
            animacao_out.play();
             */
            frmPrincipal.setRoot(root);

            //----------------------------------------------------------------------
            // TRATA DOS SERVICOS DO SISTEMA
            //----------------------------------------------------------------------
            ImportacaoJpaController daoImportacao = new ImportacaoJpaController(GerenciadorBaseDados.getEntityManagerFactory());
            ServiceImportacao.getInstance().setImportacao(daoImportacao.findImportacao(1));
            getFormMenuController().bindTaskProgress();


            //----------------------------------------------------------------------
            // IMPORTA ARQUIVOS DE DADOS DAS ETIQUETAS
            //----------------------------------------------------------------------    
            ImportacaoDadosExternos.getInstance().verificaImportacoes();
            //----------------------------------------------------------------------
        } catch (IOException ex) {
            Logger.getLogger(FrmJanelaController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    //* PRG - 1.3.5
    public static void gotoAssistenteFim() {
        try {

            Parent root = FXMLLoader.load(FormHelper.class.getResource(FORM_ASSISTENTE_INICIAL_FIM));

            frmPrincipal.setRoot(root);

        } catch (IOException ex) {
            Logger.getLogger(FormHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //*

    public static FrmConexaoBancoController gotoBancoDados() {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(FORM_BANCODADOS));
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Node root = (Node) fxmlLoader.load(fxmlLoader.getLocation().openStream());

            frmPrincipal.setRoot(root);

            return (FrmConexaoBancoController) fxmlLoader.getController();

            //pnStack.getChildren().clear();
            //pnStack.getChildren().add(root);
            //root.setTranslateX(-1000);
            //Animador.animateTranslateX((Node) root, Duration.millis(100), Duration.millis(500), Animador.DirecaoAnimacao.IN).play();
            //AnimationHelper.animateTranslateIn(pane,Duration.millis(400), Duration.millis(200));
        } catch (IOException ex) {
            Logger.getLogger(FormHelper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
