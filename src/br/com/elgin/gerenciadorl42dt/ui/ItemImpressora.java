/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.model.Impressora;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ItemImpressora extends AnchorPane {

    private Impressora impressora;

    final Label lbDescricao = new Label();
    //final Label lbEndereco = new Label();
    final BooleanProperty selecionado = new SimpleBooleanProperty();
    final Circle ciStatus = new Circle();

    public boolean isSelecionado() {
        return selecionado.get();
    }

    public void setSelecionado(boolean value) {
        selecionado.set(value);
        if (selecionado.get()) {
            getStyleClass().add("item_drag");
        } else {
            getStyleClass().removeAll("item_drag");
        }
    }

    public BooleanProperty selecionadoProperty() {
        return selecionado;

    }

    public Impressora getImpressora() {
        return impressora;
    }

    public void setImpressora(Impressora impressora) {
        this.impressora = impressora;
        updateInfos();
    }

    public void updateInfos() {
        if (getImpressora() != null) {
            lbDescricao.setText(getImpressora().getDescricao());
            //lbEndereco.setText(getImpressora().getEndereco());
            if (getImpressora().isAtualizada()) {
                ciStatus.setFill(Color.GREEN);
            } else {
                ciStatus.setFill(Color.ORANGE);
            }
        }
    }

    public ItemImpressora(Impressora imp) {
        getStyleClass().add("frame_radius_all");
        getStyleClass().add("item");

        setCursor(Cursor.HAND);

        lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        //lbDescricao.setTextFill(Color.WHITE);
        lbDescricao.setStyle("-fx-text-fill:-cor-base;");
        lbDescricao.setVisible(true);
        AnchorPane.setLeftAnchor(lbDescricao, 12.0);
        AnchorPane.setTopAnchor(lbDescricao, 7.0);
        AnchorPane.setBottomAnchor(lbDescricao, 7.0);

        /*
        lbEndereco.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        //lbEndereco.setTextFill(Color.WHITE);
        lbEndereco.setStyle("-fx-text-fill:-cor-base;");
        lbEndereco.setVisible(true);
        AnchorPane.setRightAnchor(lbEndereco, 32.0);
        AnchorPane.setTopAnchor(lbEndereco, 7.0);
        AnchorPane.setBottomAnchor(lbEndereco, 7.0);
         */
        ciStatus.setVisible(true);
        ciStatus.setStrokeWidth(0);
        ciStatus.setRadius(10);
        AnchorPane.setRightAnchor(ciStatus, 7.0);
        AnchorPane.setTopAnchor(ciStatus, 6.0);
        AnchorPane.setBottomAnchor(ciStatus, 7.0);

        setPrefHeight(32);
        setVisible(true);

        setImpressora(imp);

        getChildren().addAll(lbDescricao,/* lbEndereco,*/ ciStatus);

        /*
        selecionadoProperty().addListener((observable, oldValue, newValue) -> {
            
            if (newValue) {
                this.getStyleClass().add("item_drag");
            } else {
                this.getStyleClass().removeAll("item_drag");
            }
             
        });
         */
        setSelecionado(true);
        setSelecionado(false);

    }

}
