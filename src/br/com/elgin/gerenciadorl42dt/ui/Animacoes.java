/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import com.sun.glass.ui.Screen;
import java.util.Comparator;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.collections.transformation.SortedList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class Animacoes {

    ///retorna efeito com sombra vermelha
    public static Effect getSombraErro() {
        return getSombraOfColor(Color.RED);
    }

    //retorna evento com sombra
    public static Effect getSombraOfColor(Color cor) {
        DropShadow dropShadow = new DropShadow();
        dropShadow.setBlurType(BlurType.GAUSSIAN);
        dropShadow.setColor(cor);
        dropShadow.setRadius(10.0D);
        return dropShadow;
    }

    private static final Comparator<Node> comparatorLayoutY = (p1, p2) -> {
        if (p1.getLayoutX() > p2.getLayoutX()) {
            return -1;
        }
        if (p1.getLayoutX() < p2.getLayoutX()) {
            return 1;
        }
        return 0;
    };

    public static enum DirecaoAnimacao {
        IN, OUT
    }

    public static Animation animatePanelControlsX_IN(Pane painel) {
        return animatePanelControlsX(painel, DirecaoAnimacao.IN);
    }

    public static Animation animatePanelControlsX_OUT(Pane painel) {
        return animatePanelControlsX(painel, DirecaoAnimacao.OUT);
    }

    public static Animation animateFadeIN(Pane panel) {
        FadeTransition anim = new FadeTransition(Duration.millis(70), panel);
        panel.setOpacity(0d);
        anim.setByValue(0d);
        anim.setToValue(1d);
        return anim;

    }

    public static Animation animateFadeOUT(Pane panel) {
        FadeTransition anim = new FadeTransition(Duration.millis(70), panel);
        anim.setByValue(1d);
        anim.setToValue(0d);
        return anim;
    }

    public static void animateNodeHeight(Node nodo, double height) {
        if (nodo instanceof Pane) {
            new Timeline(
                    new KeyFrame(
                            Duration.millis(150),
                            new KeyValue(((Pane) nodo).prefHeightProperty(), height, Interpolator.EASE_BOTH)
                    )
            ).play();
        }
    }

    public static void animateNodeWidth(Node nodo, double width) {
        if (nodo instanceof Pane) {
            new Timeline(
                    new KeyFrame(
                            Duration.millis(150),
                            new KeyValue(((Pane) nodo).prefWidthProperty(), width, Interpolator.EASE_BOTH)
                    )
            ).play();
        }
    }

    public static void animateNodeWidth(Node nodo, double width, Duration delay) {
        if (nodo instanceof Pane) {

            Timeline t = new Timeline(
                    new KeyFrame(
                            Duration.millis(150),
                            new KeyValue(((Pane) nodo).prefWidthProperty(), width, Interpolator.EASE_BOTH)
                    )
            );
            t.setDelay(delay);
            t.play();
        }
    }

    public static Animation animatePanelControlsX(Pane painel, DirecaoAnimacao direcao) {

        SequentialTransition animacao = new SequentialTransition();

        SortedList<Node> lista = new SortedList(painel.getChildren(), comparatorLayoutY);
        lista.forEach((node) -> {
            if (node instanceof Pane) {
                animacao.getChildren().add(animateTranslateX(node, Duration.millis(100), Duration.ZERO, direcao));
                //animacao.getChildren().add(animatePanelControls((Pane) node, direcao));
                //} else {
                //animacao.getChildren().add(animateTranslate(node, Duration.millis(150), Duration.ZERO, direcao));
            }
        });
        return animacao;
    }

    public static TranslateTransition animateTranslateX(Node node, Duration duration, Duration delay, DirecaoAnimacao direcao) {
        TranslateTransition translateIn = new TranslateTransition();
        if (direcao == DirecaoAnimacao.IN) {
            //translateIn.setFromX(node.localToParent(node.getBoundsInLocal()).getMaxX() * -1);
            ((Pane) node).setTranslateX(-1000);
            translateIn.setFromX(-1000);
            translateIn.setToX(0);
        } else {
            translateIn.setFromX(0);
            translateIn.setToX(Screen.getMainScreen().getWidth() - node.localToScreen(node.getBoundsInLocal()).getMinX());
        }
        translateIn.setInterpolator(Interpolator.LINEAR);
        translateIn.setNode(node);
        translateIn.setDuration(duration);
        translateIn.setDelay(delay);
        return translateIn;
    }

    public static void slowScrollToValue(ScrollPane scrollPane, Double valor) {
        Animation animation = new Timeline(
                new KeyFrame(Duration.millis(150),
                        new KeyValue(scrollPane.vvalueProperty(), valor)));
        animation.play();
    }
        //função que retorna um painel com as labels coloridos representando um flexi
    public static HBox getCodigoFlexbarcode(Flexibarcode flex, Double tamanho_fonte) {
        HBox box = new HBox();
        box.setPrefSize(HBox.USE_COMPUTED_SIZE, HBox.USE_COMPUTED_SIZE);
        box.setMaxSize(HBox.USE_PREF_SIZE, HBox.USE_PREF_SIZE);
        box.setMinSize(HBox.USE_PREF_SIZE, HBox.USE_PREF_SIZE);
        Font fonte_tooltip = Font.font("Roboto", FontWeight.BOLD, 12);
        Font fonte = Font.font("Roboto", FontWeight.BOLD, tamanho_fonte);
        String estilo = "-fx-background-radius:0;";
        if (flex.getFlag() >= 0 && flex.getFlag() < 2) {
            Label lbFlag = new Label();
            lbFlag.setText(new String(new char[flex.getFlag() + 1]).replace('\0', 'F'));
            lbFlag.setFont(fonte);
            lbFlag.setStyle(estilo + "-fx-background-color:-paleta1;");
            Tooltip tp = new Tooltip("Flag");
            tp.setFont(fonte_tooltip);
            lbFlag.setTooltip(tp);
            box.getChildren().add(lbFlag);
        }
        if (flex.getTamanhocodigo() > 0) {
            Label lbCod = new Label();
            lbCod.setText(new String(new char[flex.getTamanhocodigo()]).replace('\0', 'C'));
            lbCod.setFont(fonte);
            lbCod.setStyle(estilo + "-fx-background-color:-paleta2;");
            Tooltip tp = new Tooltip("Código do Produto");
            tp.setFont(fonte_tooltip);
            lbCod.setTooltip(tp);
            box.getChildren().add(lbCod);
        }
        if (flex.getCheckdigit() == 1 || flex.getCheckdigit() == 3) {
            Label lbMid = new Label();
            lbMid.setText("D");
            lbMid.setFont(fonte);
            lbMid.setStyle(estilo + "-fx-background-color:-paleta5;");
            Tooltip tp = new Tooltip("Dígito verificador");
            tp.setFont(fonte_tooltip);
            lbMid.setTooltip(tp);
            box.getChildren().add(lbMid);
        }
        if (flex.getTamanhoinfo1() > 0) {
            Label lbInf1 = new Label();
            lbInf1.setText(new String(new char[flex.getTamanhoinfo1()]).replace('\0', 'A'));
            lbInf1.setFont(fonte);
            lbInf1.setStyle(estilo + "-fx-background-color:-paleta3;");
            Tooltip tp = new Tooltip("Informação 1");
            tp.setFont(fonte_tooltip);
            lbInf1.setTooltip(tp);
            box.getChildren().add(lbInf1);
        }
        if (flex.getTamanhoinfo2() > 0) {
            Label lbInf2 = new Label();
            lbInf2.setText(new String(new char[flex.getTamanhoinfo2()]).replace('\0', 'B'));
            lbInf2.setFont(fonte);
            lbInf2.setStyle(estilo + "-fx-background-color:-paleta4;");
            Tooltip tp = new Tooltip("Informação 2");
            tp.setFont(fonte_tooltip);
            lbInf2.setTooltip(tp);
            box.getChildren().add(lbInf2);
        }

        if (flex.getCheckdigit() == 2 || flex.getCheckdigit() == 3) {
            Label lbFim = new Label();
            lbFim.setText("D");
            lbFim.setFont(fonte);
            lbFim.setStyle(estilo + "-fx-background-color:-paleta5;");
            Tooltip tp = new Tooltip("Dígito verificador");
            tp.setFont(fonte_tooltip);
            lbFim.setTooltip(tp);
            box.getChildren().add(lbFim);
        }
        if (box.getChildren().size() > 0) {
            if (box.getChildren().size() == 1) {
                ((Label) box.getChildren().get(0)).setStyle(((Label) box.getChildren().get(0)).getStyle() + "-fx-background-radius:15;");
            } else {
                ((Label) box.getChildren().get(0)).setStyle(((Label) box.getChildren().get(0)).getStyle() + "-fx-background-radius:15 0 0 15;");
                ((Label) box.getChildren().get(box.getChildren().size() - 1)).setStyle(((Label) box.getChildren().get(box.getChildren().size() - 1)).getStyle() + "-fx-background-radius:0 15 15 0;");
            }
        }

        return box;
    }
}
