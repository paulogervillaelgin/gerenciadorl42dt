/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;


import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Paulo
 */
public class ItemGrupoProduto extends AnchorPane {

    private Grupoproduto grupo;
    private final Label lbDescricao = new Label();
    private final Label lbTotal = new Label();

    static EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    static GrupoprodutoJpaController daoGrupo = new GrupoprodutoJpaController(emf);

    private boolean selecionado = false;

    public boolean isSelecionado() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado = selecionado;
        if (selecionado) {
            getStyleClass().add("item_drag");
        } else {
            getStyleClass().removeAll("item_drag");
        }
    }

    public Grupoproduto getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupoproduto grupo) {
        this.grupo = grupo;
        updateInfos();
    }

    public void updateInfos() {
        lbDescricao.setText(grupo.getDescricao());

        /*
        Grupoproduto grupo_prod = daoGrupo.findGrupoproduto(grupo.getIndice());
        if (grupo_prod != null) {
            lbTotal.setText(String.valueOf(grupo_prod.getProdutoSet().size()));
        }
        */
        lbTotal.setText(String.valueOf(daoGrupo.getGrupoprodutoProdutoCount(grupo.getIndice())));

    }

    public ItemGrupoProduto(Grupoproduto grupoproduto) {

        getStyleClass().add("frame_radius_all");
        getStyleClass().add("item");

        setPrefHeight(50);
        setPrefWidth(160);
        setMinWidth(0);
        setVisible(true);

        lbDescricao.setWrapText(true);
        lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 12));
        //lbDescricao.setTextFill(Color.WHITE);
        lbDescricao.setStyle("-fx-text-fill:-cor-base;");
        lbDescricao.setTextAlignment(TextAlignment.CENTER);
        lbDescricao.setAlignment(Pos.CENTER);
        lbDescricao.setVisible(true);
        AnchorPane.setLeftAnchor(lbDescricao, 8.0);
        AnchorPane.setRightAnchor(lbDescricao, 8.0);
        AnchorPane.setTopAnchor(lbDescricao, 5.0);
        AnchorPane.setBottomAnchor(lbDescricao, 5.0);

        lbTotal.setFont(Font.font("Roboto", 10));
        //lbTotal.setTextFill(Color.WHITE);
        lbTotal.setStyle("-fx-text-fill:-cor-base;");
        lbTotal.setTextAlignment(TextAlignment.CENTER);
        lbTotal.setAlignment(Pos.CENTER);
        lbTotal.setPrefHeight(14);
        lbTotal.setPrefWidth(44);
        lbTotal.setLayoutY(5);
        lbTotal.setVisible(true);
        AnchorPane.setRightAnchor(lbTotal, 5.0);

        setGrupo(grupoproduto);

        getChildren().addAll(lbDescricao, lbTotal);
    }

}
