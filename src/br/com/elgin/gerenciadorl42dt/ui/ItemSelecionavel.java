/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Paulo Cesar <paulo.junior at elgin.com.br>
 * @param <T>
 */
public class ItemSelecionavel<T> extends AnchorPane {

    private final Label lbDescricao = new Label();
    private final RadioButton rbSelecionado = new RadioButton();

    private final BooleanProperty selecionado = new SimpleBooleanProperty();
    private T objeto;

    public BooleanProperty selecionadoProperty() {
        return selecionado;
    }

    public T getObjeto() {
        return objeto;
    }

    public void setObjeto(T objeto) {
        this.objeto = objeto;
    }

    public boolean isSelecionado() {
        return selecionado.get();
    }

    public void setSelecionado(boolean value) {
        selecionado.set(value);
    }

    public ItemSelecionavel(T obj, String descricao) {

        selecionado.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                getStyleClass().clear();
                getStyleClass().add("item_selecionado");
                lbDescricao.setStyle("-fx-text-fill : white;");
            } else {
                getStyleClass().clear();
                getStyleClass().add("item_nao_selecionado");
                lbDescricao.setStyle("-fx-text-fill : -cor-base;");
            }
        });
        
        setCursor(Cursor.HAND);
        setObjeto(obj);
        
        rbSelecionado.selectedProperty().bindBidirectional(selecionado);
        
        //para forçar a atualização no Listener
        setSelecionado(true);
        setSelecionado(false);
        
        setVisible(true);

        rbSelecionado.setText("");
        rbSelecionado.setVisible(true);
        AnchorPane.setLeftAnchor(rbSelecionado, 7.0);
        AnchorPane.setTopAnchor(rbSelecionado, 5.0);
        AnchorPane.setBottomAnchor(rbSelecionado, 5.0);

        lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        lbDescricao.setTextFill(Color.WHITE);
        lbDescricao.setText(descricao);
        AnchorPane.setLeftAnchor(lbDescricao, 30.0);
        AnchorPane.setRightAnchor(lbDescricao, 10.0);
        AnchorPane.setTopAnchor(lbDescricao, 5.0);
        AnchorPane.setBottomAnchor(lbDescricao, 5.0);
        lbDescricao.setVisible(true);

        getChildren().addAll(lbDescricao,rbSelecionado);

    }
}
