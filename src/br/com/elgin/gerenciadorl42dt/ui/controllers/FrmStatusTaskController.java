/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmStatusTaskController implements Initializable, Formulario {

    @FXML
    private Button btConfirmar;

    @FXML
    private ProgressBar barProgresso;

    @FXML
    private Label lbStatus;

    @FXML
    private Label lbTitulo;

    private Task<Boolean> task;
    private EventHandler<ActionEvent> onClose;

    public Task<Boolean> getTask() {
        return task;
    }

    public void setTask(Task<Boolean> task) {
        this.task = task;
        processaTask(true);
    }

    public void setTaskExecutando(Task<Boolean> task) {
        this.task = task;
        processaTask(false);
    }

    
    public void setTitulo(String titulo) {
        lbTitulo.setText(titulo);
    }

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    private void processaTask(boolean executar) {
        if (task != null) {
            lbStatus.textProperty().bind(task.messageProperty());
            barProgresso.progressProperty().bind(task.progressProperty());
            btConfirmar.setDisable(true);
            FormHelper.setTravado(true);
            task.setOnSucceeded((event) -> {
                if (task.getValue()) {
                    barProgresso.setStyle("-fx-accent:green;");
                } else {
                    barProgresso.setStyle("-fx-accent:red;");
                }
                btConfirmar.setDisable(false);
                FormHelper.setTravado(false);
            });
            if (executar) {
                new Thread(task).start();
            }
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        onClose();
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

}
