/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.BoundingBox;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 *
 */
public class FrmJanelaController implements Initializable {

    @FXML
    private BorderPane painel;

    @FXML
    private Label lbTitulo;

    @FXML
    private StackPane pnStack;

    @FXML
    private AnchorPane barraFerramentas;

    @FXML
    private Button btFechar;

    @FXML
    private Button btMinimizar;

    @FXML
    private Button btTelaCheia;

    @FXML
    private Button btMaximizar;

    @FXML
    private Pane pnResizeLeft;

    @FXML
    private Pane pnResizeLeft_1;

    @FXML
    private Pane pnResizeLeft_Down;

    @FXML
    private Pane pnResizeDown;

    @FXML
    private Pane pnResizeDown_Left;

    @FXML
    private Pane pnResizeDown_Right;

    @FXML
    private Pane pnResizeTop;

    @FXML
    private Pane pnResizeTop_Left;

    @FXML
    private Pane pnResizeTop_Right;

    @FXML
    private Pane pnResizeRight;

    @FXML
    private Pane pnResizeRight_1;

    @FXML
    private Pane pnResizeRight_Down;

    private Stage stage;

    private boolean resizable;

    public boolean isResizable() {
        return resizable;
    }

    public void setResizable(boolean resizable) {
        this.resizable = resizable;

        pnResizeDown.setMouseTransparent(!resizable);
        pnResizeLeft.setMouseTransparent(!resizable);
        pnResizeRight.setMouseTransparent(!resizable);
        pnResizeDown_Left.setMouseTransparent(!resizable);
        pnResizeDown_Right.setMouseTransparent(!resizable);
        pnResizeLeft_Down.setMouseTransparent(!resizable);
        pnResizeRight_Down.setMouseTransparent(!resizable);
        pnResizeTop_Left.setMouseTransparent(!resizable);
        pnResizeTop_Right.setMouseTransparent(!resizable);
        pnResizeLeft_1.setMouseTransparent(!resizable);
        pnResizeRight_1.setMouseTransparent(!resizable);
        pnResizeTop.setMouseTransparent(!resizable);

        btTelaCheia.setVisible(resizable);
        btMaximizar.setVisible(resizable);
        btMinimizar.setVisible(resizable);

    }

    protected double initialX;
    protected double initialY;

    private static double initX = -1;
    private static double initY = -1;
    private static double newX;
    private static double newY;

    private StringProperty titulo = new SimpleStringProperty("Gerenciador L43DT");

    BoundingBox savedBounds;
    boolean maximized = false;

    public boolean isMaximized() {
        return maximized;
    }

    public void setMaximized(boolean maximized) {
        this.maximized = maximized;
        if (maximized) {
            btMaximizar.getStyleClass().clear();
            btMaximizar.getStyleClass().add("decoration-button-restore");
            barraFerramentas.getStyleClass().clear();
            barraFerramentas.getStyleClass().add("barra-titulo-inteira");
        } else {
            btMaximizar.getStyleClass().clear();
            btMaximizar.getStyleClass().add("decoration-button-maximize");
            barraFerramentas.getStyleClass().clear();
            barraFerramentas.getStyleClass().add("barra-titulo-normal");
        }
    }

    TranslateTransition fullscreenButtonTransition = null;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btFechar.setOnAction((event) -> {
            final Stage stage_final = getStage();
            Platform.runLater(() -> {
                stage_final.fireEvent(new WindowEvent(stage_final, WindowEvent.WINDOW_CLOSE_REQUEST));
            });
        });

        btMinimizar.setOnAction((event) -> {
            if (!Platform.isFxApplicationThread()) {
                Platform.runLater(() -> {
                    _minimize();
                });
            } else {
                _minimize();
            }
        });

        btMaximizar.setOnAction((event) -> {
            if (isMaximized()) {
                restore();
            } else {
                maximize();
            }
            /*
            if (getStage() != null) {
                getStage().setMaximized(!getStage().isMaximized());
            }
             */
        });

        btTelaCheia.setOnAction((event) -> {
            Stage _stage = getStage();
            _stage.setFullScreen(!_stage.isFullScreen());
        });

        barraFerramentas.setOnMousePressed((MouseEvent me) -> {
            if (!stage.isFullScreen() && me.getButton() != MouseButton.MIDDLE) {
                if (stage.isMaximized()) {
                    stage.setMaximized(false);
                }
                initialX = me.getSceneX();
                initialY = me.getSceneY();
                initX = me.getScreenX();
                initY = me.getScreenY();
            }
        });

        barraFerramentas.setOnMouseDragged((MouseEvent me) -> {
            if (!stage.isFullScreen() && me.getButton() != MouseButton.MIDDLE) {
                barraFerramentas.getScene().getWindow().setX(me.getScreenX() - initialX);
                barraFerramentas.getScene().getWindow().setY(me.getScreenY() - initialY);
            }
        });

        barraFerramentas.setOnMouseClicked((MouseEvent mouseEvent) -> {
            if (!stage.isFullScreen() && mouseEvent.getClickCount() > 1) {
                if (isMaximized()) {
                    restore();
                } else {
                    maximize();
                }
                mouseEvent.consume();
            }
        } // Maximize on double click
        );

        setStageResizableWith(pnResizeDown);
        setStageResizableWith(pnResizeLeft);
        setStageResizableWith(pnResizeRight);
        setStageResizableWith(pnResizeDown_Left);
        setStageResizableWith(pnResizeDown_Right);
        setStageResizableWith(pnResizeLeft_Down);
        setStageResizableWith(pnResizeRight_Down);
        setStageResizableWith(pnResizeTop_Left);
        setStageResizableWith(pnResizeTop_Right);
        setStageResizableWith(pnResizeLeft_1);
        setStageResizableWith(pnResizeRight_1);
        setStageResizableWith(pnResizeTop);

        lbTitulo.textProperty().bind(titulo);

        /*
        ChangeListener listener = new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
                gotoLogin();
                painel.layoutBoundsProperty().removeListener(this);
            }
        };
        painel.layoutBoundsProperty().addListener(listener);
         */
    }

    public String getTitulo() {
        return titulo.getValue();
    }

    public void setTitulo(String _titulo) {
        this.titulo.setValue(_titulo);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage _stage) {
        this.stage = _stage;

        stage.titleProperty().bind(titulo);

        // Verificar porque muito provavelmente esse evento nunca ocorre(estamos simulando o efeito de maximizar)
        /*
        stage.maximizedProperty().addListener((observable) -> {
            if (stage.isMaximized()) {
                btMaximizar.getStyleClass().add("decoration-button-restore");
                barraFerramentas.getStyleClass().clear();
                barraFerramentas.getStyleClass().add("barra-titulo-inteira");
            } else {
                btMaximizar.getStyleClass().remove("decoration-button-restore");
                barraFerramentas.getStyleClass().clear();
                barraFerramentas.getStyleClass().add("barra-titulo-normal");
            }
        });
         */
        stage.fullScreenProperty().addListener((Observable observable) -> {
            if (stage.isFullScreen()) {
                if (fullscreenButtonTransition != null) {
                    fullscreenButtonTransition.stop();
                }
                // Animate the fullscreen button
                fullscreenButtonTransition = new TranslateTransition();
                fullscreenButtonTransition.setDuration(Duration.millis(500));
                fullscreenButtonTransition.setToX(68);
                fullscreenButtonTransition.setNode(btTelaCheia);
                fullscreenButtonTransition.setOnFinished((ActionEvent t) -> {
                    fullscreenButtonTransition = null;
                });
                fullscreenButtonTransition.play();
                btTelaCheia.getStyleClass().add("decoration-button-unfullscreen");
                barraFerramentas.getStyleClass().clear();

                barraFerramentas.getStyleClass().add("barra-titulo-inteira");
            } else {
                if (fullscreenButtonTransition != null) {
                    fullscreenButtonTransition.stop();
                }
                // Animate the change
                fullscreenButtonTransition = new TranslateTransition();
                fullscreenButtonTransition.setDuration(Duration.millis(500));
                fullscreenButtonTransition.setToX(0);
                fullscreenButtonTransition.setNode(btTelaCheia);
                fullscreenButtonTransition.setOnFinished((ActionEvent t) -> {
                    fullscreenButtonTransition = null;
                });

                fullscreenButtonTransition.play();
                btTelaCheia.getStyleClass().remove("decoration-button-unfullscreen");
                barraFerramentas.getStyleClass().clear();
                barraFerramentas.getStyleClass().add("barra-titulo-normal");
            }
            btMaximizar.setVisible(!stage.isFullScreen());
            btMinimizar.setVisible(!stage.isFullScreen());

        });

    }

    public void restore() {
        Stage _stage = getStage();
        restoreSavedBounds(_stage);
        this.savedBounds = null;
        setMaximized(false);
    }

    public void maximize() {
        Stage _stage = getStage();

        ObservableList<Screen> screensForRectangle = Screen.getScreensForRectangle(_stage.getX(), _stage.getY(), _stage.getWidth(), _stage.getHeight());
        Screen screen = (Screen) screensForRectangle.get(0);
        Rectangle2D visualBounds = screen.getVisualBounds();

        this.savedBounds = new BoundingBox(_stage.getX(), _stage.getY(), _stage.getWidth(), _stage.getHeight());

        stage.setX(visualBounds.getMinX());
        stage.setY(visualBounds.getMinY());
        stage.setWidth(visualBounds.getWidth());
        stage.setHeight(visualBounds.getHeight());
        setMaximized(true);

    }

    public void restoreSavedBounds(Stage _stage) {
        _stage.setX(this.savedBounds.getMinX());
        _stage.setY(this.savedBounds.getMinY());
        _stage.setWidth(this.savedBounds.getWidth());
        _stage.setHeight(this.savedBounds.getHeight());
        this.savedBounds = null;
    }

    private void _minimize() {
        Stage _stage = getStage();
        _stage.setIconified(true);
    }

    /**
     * Stage resize management
     *
     * @param node
     */
    public void setStageResizableWith(final Node node) {

        node.setOnMousePressed((MouseEvent mouseEvent) -> {
            if (mouseEvent.isPrimaryButtonDown()) {
                initX = mouseEvent.getScreenX();
                initY = mouseEvent.getScreenY();
                mouseEvent.consume();
            }
        });

        node.setOnMouseDragged((MouseEvent mouseEvent) -> {
            final Stage stage1 = getStage();
            if (!mouseEvent.isPrimaryButtonDown() || (initX == -1 && initY == -1)) {
                return;
            }
            if (stage1.isFullScreen()) {
                return;
            }
            /*
            * Long press generates drag event!
             */
            if (mouseEvent.isStillSincePress()) {
                mouseEvent.consume();
                return;
            }
            if (isMaximized()) {
                // Remove maximized state
                setMaximized(false);
                //return;
            }
            newX = mouseEvent.getScreenX();
            newY = mouseEvent.getScreenY();
            double deltax = newX - initX;
            double deltay = newY - initY;

            Cursor cursor = ((Pane) mouseEvent.getSource()).getCursor();

            if (Cursor.E_RESIZE.equals(cursor)) {
                setStageWidth(stage1, stage1.getWidth() + deltax);
                mouseEvent.consume();
            } else if (Cursor.NE_RESIZE.equals(cursor)) {
                if (setStageHeight(stage1, stage1.getHeight() - deltay)) {
                    setStageY(stage1, stage1.getY() + deltay);
                }
                setStageWidth(stage1, stage1.getWidth() + deltax);
                mouseEvent.consume();
            } else if (Cursor.SE_RESIZE.equals(cursor)) {
                setStageWidth(stage1, stage1.getWidth() + deltax);
                setStageHeight(stage1, stage1.getHeight() + deltay);
                mouseEvent.consume();
            } else if (Cursor.S_RESIZE.equals(cursor)) {
                setStageHeight(stage1, stage1.getHeight() + deltay);
                mouseEvent.consume();
            } else if (Cursor.W_RESIZE.equals(cursor)) {
                if (setStageWidth(stage1, stage1.getWidth() - deltax)) {
                    stage1.setX(stage1.getX() + deltax);
                }
                mouseEvent.consume();
            } else if (Cursor.SW_RESIZE.equals(cursor)) {
                if (setStageWidth(stage1, stage1.getWidth() - deltax)) {
                    stage1.setX(stage1.getX() + deltax);
                }
                setStageHeight(stage1, stage1.getHeight() + deltay);
                mouseEvent.consume();
            } else if (Cursor.NW_RESIZE.equals(cursor)) {
                if (setStageWidth(stage1, stage1.getWidth() - deltax)) {
                    stage1.setX(stage1.getX() + deltax);
                }
                if (setStageHeight(stage1, stage1.getHeight() - deltay)) {
                    setStageY(stage1, stage1.getY() + deltay);
                }
                mouseEvent.consume();
            } else if (Cursor.N_RESIZE.equals(cursor)) {
                if (setStageHeight(stage1, stage1.getHeight() - deltay)) {
                    setStageY(stage1, stage1.getY() + deltay);
                }
                mouseEvent.consume();
            }
        });
    }

    /**
     * Under Windows, the undecorator Stage could be been dragged below the Task
     * bar and then no way to grab it again... On Mac, do not drag above the
     * menu bar
     *
     * @param y
     */
    void setStageY(Stage stage, double y) {
        try {
            ObservableList<Screen> screensForRectangle = Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
            if (screensForRectangle.size() > 0) {
                Screen screen = screensForRectangle.get(0);
                Rectangle2D visualBounds = screen.getVisualBounds();
                if (y < visualBounds.getHeight() - 30 && y >= visualBounds.getMinY()) {
                    stage.setY(y);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "setStageY issue", e);
        }
    }

    boolean setStageWidth(Stage stage, double width) {
        if (width >= stage.getMinWidth()) {
            stage.setWidth(width);
            initX = newX;
            return true;
        }
        return false;
    }

    boolean setStageHeight(Stage stage, double height) {
        if (height >= stage.getMinHeight()) {
            stage.setHeight(height);
            initY = newY;
            return true;
        }
        return false;
    }

    public void setSize(double width, double height) {
        setStageHeight(getStage(), height);
        setStageWidth(getStage(), width);
    }

    public void setRoot(Node root) {
        pnStack.getChildren().clear();
        pnStack.getChildren().add(root);
    }

    public void empilhaNodo(Node nodo) {
        for (Node node : pnStack.getChildren()) {
            node.setDisable(true);
            node.setOpacity(0.5);
        }
        pnStack.getChildren().add(nodo);
    }

    public void desempilhaNodo() {
        pnStack.getChildren().remove(pnStack.getChildren().size() - 1);
        if (pnStack.getChildren().size() >= 1) {
            Node node = pnStack.getChildren().get(pnStack.getChildren().size() - 1);
            node.setOpacity(1);
            node.setDisable(false);
        }
    }

}
