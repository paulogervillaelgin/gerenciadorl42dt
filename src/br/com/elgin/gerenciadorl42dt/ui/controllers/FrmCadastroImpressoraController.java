/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.DepartamentoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.FormatoEtiquetaJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.comunicacao.ComandoLeitura;
import br.com.elgin.gerenciadorl42dt.comunicacao.Comunicacao;
import br.com.elgin.gerenciadorl42dt.comunicacao.TaskComunicacao;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import br.com.elgin.gerenciadorl42dt.ui.ImageHelper;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmCadastroImpressoraController implements Initializable, Formulario {

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private TextField txtDescricao;

    @FXML
    private TextField txtSerial;

    @FXML
    private ComboBox<Departamento> cbDepartamento;

    @FXML
    private AnchorPane pnEtiqueta;

    @FXML
    private Pane pnEtiquetaInterno;

    @FXML
    private ComboBox<FormatoEtiqueta> cbEtiquetaPesados;

    @FXML
    private ComboBox<FormatoEtiqueta> cbEtiquetaUnitarios;

    @FXML
    private CheckBox ckDefinirEtiqueta;

    @FXML
    private AnchorPane pnMensagem;

    @FXML
    private Pane pnMensagemInterno;

    @FXML
    private TextArea txtMsgEspecial;

    @FXML
    private CheckBox ckDefinirMensagem;

    @FXML
    private Button btAtualizar;

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private Label lbJaExiste;

    @FXML
    private AnchorPane pnVisualiza1;

    @FXML
    private AnchorPane pnVisualiza2;

    private Impressora impressora;
    private Departamento departamento;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);
    DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);
    FormatoEtiquetaJpaController daoEtiqueta = new FormatoEtiquetaJpaController(emf);

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
        if (getImpressora() == null) {
            if (cbDepartamento.getItems().contains(departamento)) {
                cbDepartamento.getSelectionModel().select(departamento);
                cbDepartamento.setDisable(true);
            }
        } else {
            cbDepartamento.setDisable(false);
        }
    }

    public Impressora getImpressora() {
        return impressora;
    }

    public void setImpressora(Impressora impressora) {
        this.impressora = impressora;
        if (impressora != null) {
            exibeDados(impressora);
            btAtualizar.setVisible(false);
        } else {
            getImpressoraConectada();
        }
    }

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // combo Departamento
        cbDepartamento.setItems(FXCollections.observableArrayList(daoDepartamento.findDepartamentoEntities()));

        cbDepartamento.setCellFactory((ListView<Departamento> param) -> {
            final ListCell<Departamento> cell = new ListCell<Departamento>() {
                @Override
                public void updateItem(Departamento item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText(null);
                    }
                }
            };
            return cell;
        });
        cbDepartamento.setConverter(new StringConverter<Departamento>() {
            @Override
            public String toString(Departamento object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public Departamento fromString(String string) {
                return null;
            }
        });
        ///////////

        List<FormatoEtiqueta> listaEtiquetasPesado = daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOPESADO);
        List<FormatoEtiqueta> listaEtiquetasUnidade = daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOUNIDADE);

        // combo FormatoEtiqueta Pesado   
        cbEtiquetaPesados.setItems(FXCollections.observableArrayList(listaEtiquetasPesado));
        cbEtiquetaPesados.setCellFactory((ListView<FormatoEtiqueta> param) -> {
            final ListCell<FormatoEtiqueta> cell = new ListCell<FormatoEtiqueta>() {
                @Override
                public void updateItem(FormatoEtiqueta item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText("<Nenhum>");
                    }

                }
            };
            return cell;
        });
        cbEtiquetaPesados.setConverter(new StringConverter<FormatoEtiqueta>() {
            @Override
            public String toString(FormatoEtiqueta object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public FormatoEtiqueta fromString(String string) {
                return null;
            }
        });
        ///////////
        // combo FormatoEtiqueta Pesado   
        cbEtiquetaUnitarios.setItems(FXCollections.observableArrayList(listaEtiquetasUnidade));
        cbEtiquetaUnitarios.setCellFactory((ListView<FormatoEtiqueta> param) -> {
            final ListCell<FormatoEtiqueta> cell = new ListCell<FormatoEtiqueta>() {
                @Override
                public void updateItem(FormatoEtiqueta item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText("<Nenhum>");
                    }

                }
            };
            return cell;
        });
        cbEtiquetaUnitarios.setConverter(new StringConverter<FormatoEtiqueta>() {
            @Override
            public String toString(FormatoEtiqueta object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public FormatoEtiqueta fromString(String string) {
                return null;
            }
        });
        ///////////

        ckDefinirEtiqueta.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                pnEtiquetaInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnEtiqueta, 40d);
            } else {
                pnEtiquetaInterno.setVisible(true);
                Animacoes.animateNodeHeight(pnEtiqueta, 150d);
            }
        });

        ckDefinirMensagem.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                pnMensagemInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnMensagem, 40d);
            } else {
                pnMensagemInterno.setVisible(true);
                Animacoes.animateNodeHeight(pnMensagem, 163d);
            }
        });

        pnCarregando.setVisible(false);
        pnCarregando.toBack();

        Platform.runLater(() -> {
            txtDescricao.requestFocus();
        });

    }

    public void exibeDados(Impressora imp) {
        if (imp != null) {
            txtDescricao.setText(imp.getDescricao());
            cbDepartamento.getSelectionModel().select(imp.getDepartamento());
            txtSerial.setText(imp.getSerial());
            ckDefinirEtiqueta.setSelected(imp.isDefinirEtiqueta());
            cbEtiquetaPesados.getSelectionModel().select(imp.getEtiquetaPesado());
            cbEtiquetaUnitarios.getSelectionModel().select(imp.getEtiquetaUnitario());
            ckDefinirMensagem.setSelected(imp.isDefinirMsgEspecial());
            txtMsgEspecial.setText(imp.getMsgEspecial());
        }
    }

    private boolean verificaCampos() {
        boolean res = true;

        txtDescricao.setEffect(null);
        cbDepartamento.setEffect(null);
        cbEtiquetaPesados.setEffect(null);
        cbEtiquetaUnitarios.setEffect(null);
        txtMsgEspecial.setEffect(null);
        txtSerial.setEffect(null);

        if (txtDescricao.getText().isEmpty()) {
            txtDescricao.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (cbDepartamento.getSelectionModel().getSelectedItem() == null) {
            cbDepartamento.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        //* PRG - 1.3.6 - Devido a problema no firmware vamos ceitar impressora sem número de série quando tiver somente uma cadastrada            
        //if (txtSerial.getText().isEmpty() || lbJaExiste.isVisible()) {
        if ((txtSerial.getText().isEmpty() || lbJaExiste.isVisible()) && ((daoImpressora.findImpressoraEntities().size() > 0)))  {            
        //*    
            txtSerial.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (ckDefinirEtiqueta.isSelected()) {
            //TODO Verificar se são iguais
            if (cbEtiquetaPesados.getSelectionModel().getSelectedItem() == null) {
                cbEtiquetaPesados.setEffect(Animacoes.getSombraErro());
                res = false;
            }
            if (cbEtiquetaUnitarios.getSelectionModel().getSelectedItem() == null) {
                cbEtiquetaUnitarios.setEffect(Animacoes.getSombraErro());
                res = false;
            }

        }

        if (ckDefinirMensagem.isSelected()) {
            if (txtMsgEspecial.getText() == null || txtMsgEspecial.getText().isEmpty()) {
                txtMsgEspecial.setEffect(Animacoes.getSombraErro());
                res = false;
            }
        }

        return res;
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    @FXML
    void handleVisualizaEtiqueta1(MouseEvent event) {
        FormHelper.getFormMenuController().visualizaImagem(ImageHelper.getImageOf(cbEtiquetaPesados.getSelectionModel().getSelectedItem()));
    }

    @FXML
    void handleVisualizaEtiqueta2(MouseEvent event) {
        FormHelper.getFormMenuController().visualizaImagem(ImageHelper.getImageOf(cbEtiquetaUnitarios.getSelectionModel().getSelectedItem()));
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        setImpressora(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaCampos()) {
            Impressora imp = new Impressora();
            if (getImpressora() != null) {
                imp.setIndice(getImpressora().getIndice());
            } else {
                //marca como desatualizada
                imp.setFlagAtualizada(ImpressoraStatusEnum.getTotalValue());
            }

            imp.setDescricao(txtDescricao.getText());

            imp.setDepartamento(cbDepartamento.getSelectionModel().getSelectedItem());

            imp.setSerial(txtSerial.getText());
            imp.setDefinirEtiqueta(ckDefinirEtiqueta.isSelected());
            imp.setEtiquetaPesado(cbEtiquetaPesados.getSelectionModel().getSelectedItem());
            imp.setEtiquetaUnitario(cbEtiquetaUnitarios.getSelectionModel().getSelectedItem());
            imp.setDefinirMsgEspecial(ckDefinirMensagem.isSelected());
            imp.setMsgEspecial(txtMsgEspecial.getText());

            //TODO verificar se foi alterado algum valor!
            if (getImpressora() != null) {

                imp.setFlagAtualizada(getImpressora().getFlagAtualizacao());

                //verifica se foi alterado algo em relação às etiquetas 
                if ((imp.isDefinirEtiqueta() != getImpressora().isDefinirEtiqueta())
                        || (!Objects.equals(imp.getEtiquetaPesado(), getImpressora().getEtiquetaPesado()))) {
                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.LAYOUT.getValorStatus());
                }

                //verifica se foi alterado a mensagem especial
                if ((imp.isDefinirMsgEspecial() != getImpressora().isDefinirMsgEspecial())
                        || !(imp.getMsgEspecial().equals(getImpressora().getMsgEspecial()))) {
                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.LAYOUT.getValorStatus());
                }

                //verifica se alterou departamento
                if (!cbDepartamento.getSelectionModel().getSelectedItem().equals(getImpressora().getDepartamento())) {
                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.PRODUTO.getValorStatus());
                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.LAYOUT.getValorStatus());
                }

            }

            //bal.setAtualizada(false);
            setImpressora(imp);
            FormHelper.getFormMenuController().desempilhaFormulario();
            onClose();
        }
    }

    @FXML
    void handleBtAtualizar(ActionEvent event) {
        getImpressoraConectada();
    }

    private void getImpressoraConectada() {
        pnCarregando.toFront();
        pnCarregando.setVisible(true);
        TaskComunicacao task = Comunicacao.getTaskSerial();
        lbCarregandoStatus.textProperty().bind(task.messageProperty());
        piCarregando.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded((event) -> {

            pnCarregando.toBack();
            pnCarregando.setVisible(false);
            piCarregando.progressProperty().unbind();
            lbCarregandoStatus.textProperty().unbind();
            if (task.getValue()) {
                
                String serial =  new String(((ComandoLeitura) task.getListaComandos().get(0)).getBufferLeitura(),Charset.forName("UTF-8")).replace("\r\n", "").replaceAll("\0+$", "");

                txtSerial.setText(serial);
                if (daoImpressora.findImpressoraBySerial(serial) != null) {
                    if (getImpressora() == null) {
                        lbJaExiste.setVisible(true);
                    }
                } else {
                    lbJaExiste.setVisible(false);
                }
            } else {
                lbJaExiste.setVisible(false);
                txtSerial.setText("");
            }
        });
        Comunicacao.EXECUTOR.submit(task);
    }

}
