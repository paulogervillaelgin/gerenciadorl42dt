/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.UsuarioJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmCadastroUsuarioController implements Initializable {

    @FXML
    private TextField txtNome;

    @FXML
    private TextField txtUsuario;

    @FXML
    private Button btAlteraSenha;

    @FXML
    private PasswordField txtSenha;

    @FXML
    private PasswordField txtConfirmaSenha;

    @FXML
    private Button btCadastrar;

    @FXML
    private Button btEditar;

    @FXML
    private Button btExcluir;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private RadioButton rdAdministrador;

    @FXML
    private ToggleGroup tgFuncao;

    @FXML
    private RadioButton rdOperador;

    @FXML
    private TableView<Usuario> tbUsuarios;

    @FXML
    private TableColumn<Usuario, String> colUsuario;

    @FXML
    private TableColumn<Usuario, String> colNome;

    @FXML
    private TableColumn<Usuario, Integer> colFuncao;

    @FXML
    private Label lbJaExiste;

    private AcoesCadastro acaoCadastro;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    UsuarioJpaController daoUsuario = new UsuarioJpaController(emf);
    ObservableList<Usuario> obsUser = null;

    public static enum EstadoBotoes {
        DESABILITADOS, CADASTRO, CONFIRMACAO;

        private EstadoBotoes() {
        }
    }

    public static enum AcoesCadastro {
        CADASTRAR, EDITAR, EXCLUIR;

        private AcoesCadastro() {
        }
    }

    public void setAcaoCadastro(AcoesCadastro acao) {
        this.acaoCadastro = acao;
    }

    public AcoesCadastro getAcaoCadastro() {
        return this.acaoCadastro;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colUsuario.setCellValueFactory(new PropertyValueFactory("Usuario"));
        colNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        colFuncao.setCellValueFactory(new PropertyValueFactory("Adm"));

        colFuncao.setCellFactory(param -> {
            return new TableCell<Usuario, Integer>() {
                @Override
                public void updateItem(Integer tipo, boolean empty) {
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                        setOnMouseClicked(null);
                    } else if (tipo != null) {
                        if (tipo == Usuario.USUARIO_TIPO_ADM) {
                            setText("Administrador");
                        } else {
                            setText("Operador");
                        }
                    }
                }
            };

        });

        limparCampos();

        updateTabela();

        setControlesSomenteLeitura(true);
        setEstadoBotoes(EstadoBotoes.CADASTRO);
    }

    @FXML
    public void handleTableClick(MouseEvent event) {
        exibeDados(getEntidadeSelecionada());
    }

    public void exibeDados(Usuario user) {
        if (user != null) {
            limparCampos();
            txtNome.setText(user.getNome());
            txtUsuario.setText(user.getUsuario());
            if (user.getAdm() == Usuario.USUARIO_TIPO_ADM) {
                tgFuncao.selectToggle(rdAdministrador);
            } else {
                tgFuncao.selectToggle(rdOperador);
            }
        }
    }

    private void updateTabela() {
        colUsuario.setVisible(false);
        colUsuario.setVisible(true);

        List results = this.daoUsuario.findUsuarioEntities();

        this.obsUser = FXCollections.observableArrayList(results);

        this.tbUsuarios.setItems(this.obsUser);
    }

    public void limparCampos() {
        txtNome.setEffect(null);
        txtUsuario.setEffect(null);
        txtSenha.setEffect(null);
        txtConfirmaSenha.setEffect(null);
        rdAdministrador.setEffect(null);
        rdOperador.setEffect(null);

        btAlteraSenha.setVisible(false);

        lbJaExiste.setVisible(false);

        txtNome.setText("");
        txtUsuario.setText("");
        txtSenha.setText("");
        txtConfirmaSenha.setText("");
        tgFuncao.selectToggle(rdOperador);
        //verificar
    }

    public void setControlesSomenteLeitura(boolean valor) {
        txtUsuario.setDisable(valor);
        txtSenha.setDisable(valor);
        txtConfirmaSenha.setDisable(valor);
        txtNome.setDisable(valor);
        rdAdministrador.setDisable(valor);
        rdOperador.setDisable(valor);
    }

    public Usuario getEntidadeSelecionada() {
        return (Usuario) tbUsuarios.getSelectionModel().getSelectedItem();
    }

    @FXML
    void handleBtAlteraSenha(ActionEvent event) {
        txtSenha.setDisable(false);
        txtConfirmaSenha.setDisable(false);
        btAlteraSenha.setVisible(false);
    }

    public void onClose() {
    }

    public void tratarBotaoCadastrar() {
        limparCampos();
        setControlesSomenteLeitura(false);
        setEstadoBotoes(EstadoBotoes.CONFIRMACAO);
        setAcaoCadastro(AcoesCadastro.CADASTRAR);

        btAlteraSenha.setVisible(false);
        txtUsuario.requestFocus();
    }

    public void tratarBotaoEditar() {
        Usuario user = getEntidadeSelecionada();
        if (user != null) {
            setControlesSomenteLeitura(false);
            setEstadoBotoes(EstadoBotoes.CONFIRMACAO);
            setAcaoCadastro(AcoesCadastro.EDITAR);
            exibeDados(user);
            txtSenha.setDisable(true);
            txtConfirmaSenha.setDisable(true);
            btAlteraSenha.setVisible(true);
            txtUsuario.requestFocus();
        } else {
            Dialog.showError("Erro", "Nenhum usuário selecionado para alterar");
        }
    }

    public void tratarBotaoExcluir() {
        Usuario user = getEntidadeSelecionada();
        if (user != null) {
            if (Objects.equals(user.getIndice(), SessaoAtual.getUsuario().getIndice())) {
                Dialog.showError("Erro", "Não é possível excluir o usuario logado!");
            } else {
                Dialog.buildConfirmation("Exclusão de registro", "Deseja realmente excluir este registro ?").addYesButton((Event t) -> {
                    try {
                        daoUsuario.destroy(user.getIndice());
                        //GerenciadorLog.logMensagemUsuario("Exclusão usuario : [" + FormCadastroProdutosController.this.getEntidadeSelecionada().getIdproduto() + "] " + FormCadastroProdutosController.this.getEntidadeSelecionada().getDescricao());
                        updateTabela();
                        limparCampos();
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    }
                }).addNoButton(null).build().show();
            }
        } else {
            Dialog.showError("Erro", "Nenhum produto selecionado para excluir!");
        }
    }

    private boolean verificaCampos() {
        boolean aux = true;
        txtNome.setEffect(null);
        txtUsuario.setEffect(null);
        txtSenha.setEffect(null);
        txtConfirmaSenha.setEffect(null);
        rdAdministrador.setEffect(null);
        rdOperador.setEffect(null);
        lbJaExiste.setVisible(false);

        if (txtUsuario.getText().isEmpty()) {
            txtUsuario.setEffect(Animacoes.getSombraErro());
            aux = false;
        } else {
            if (getAcaoCadastro() == AcoesCadastro.CADASTRAR || !txtUsuario.getText().equals(getEntidadeSelecionada().getUsuario())) {
                if (!daoUsuario.findUsuarioEntitiesByUsuario(txtUsuario.getText()).isEmpty()) {
                    txtUsuario.setEffect(Animacoes.getSombraErro());
                    lbJaExiste.setVisible(true);
                    aux = false;
                }
            }
        }

        if (!txtSenha.isDisabled()) {
            if (txtSenha.getText().isEmpty()) {
                txtSenha.setEffect(Animacoes.getSombraErro());
                aux = false;
            }

            if (txtConfirmaSenha.getText().isEmpty()) {
                txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
                aux = false;
            }

            if ((!txtSenha.getText().isEmpty()) && (!txtConfirmaSenha.getText().isEmpty()) && (txtSenha.getText().equals(txtConfirmaSenha.getText()) == false)) {
                this.txtSenha.setEffect(Animacoes.getSombraErro());
                this.txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
                aux = false;
            }
        }
        if (txtNome.getText().isEmpty()) {
            txtNome.setEffect(Animacoes.getSombraErro());
            aux = false;
        }
        if (tgFuncao.getSelectedToggle() == null) {
            rdAdministrador.setEffect(Animacoes.getSombraErro());
            rdOperador.setEffect(Animacoes.getSombraErro());
            aux = false;
        }
       
        return aux;
    }

    public void tratarBotaoConfirmar() {
        if (verificaCampos()) {
            if (getAcaoCadastro() == AcoesCadastro.CADASTRAR) {
                Usuario usuario = new Usuario();
                usuario.setNome(txtNome.getText());
                usuario.setUsuario(txtUsuario.getText());
                usuario.setAdm(rdAdministrador.isSelected() ? Usuario.USUARIO_TIPO_ADM : Usuario.USUARIO_TIPO_OPERADOR);
                usuario.setSenha(Utilidades.toSHA256(txtSenha.getText()));
                try {
                    daoUsuario.create(usuario);
                    //GerenciadorLog.logMensagemUsuario("Cadastro produto : [" + usuario.getIdproduto() + "] " + usuario.getDescricao());
                } catch (Exception ex) {
                    Logger.getLogger(FrmCadastroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (getAcaoCadastro() == AcoesCadastro.EDITAR) {
                if (getEntidadeSelecionada() != null) {
                    Usuario usuario = new Usuario();
                    usuario.setIndice(getEntidadeSelecionada().getIndice());
                    usuario.setNome(txtNome.getText());
                    usuario.setUsuario(txtUsuario.getText());
                    usuario.setAdm(rdAdministrador.isSelected() ? Usuario.USUARIO_TIPO_ADM : Usuario.USUARIO_TIPO_OPERADOR);
                    if (!txtSenha.isDisabled()) {
                        usuario.setSenha(Utilidades.toSHA256(txtSenha.getText()));
                    } else {
                        usuario.setSenha(getEntidadeSelecionada().getSenha());
                    }
                    try {
                        daoUsuario.edit(usuario);
                    } catch (Exception ex) {
                        Logger.getLogger(FrmCadastroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //GerenciadorLog.logMensagemUsuario("Edi��o produto : [" + produto.getIdproduto() + "] " + produto.getDescricao());
                    //GerenciadorLog.logMensagemUsuario("Edi��o produto : [" + produto.getIdproduto() + "] " + produto.getDescricao());
                } else {
                    Dialog.showError("Erro", "Referencia perdida");
                }
            }
            btAlteraSenha.setVisible(false);
            updateTabela();
            setEstadoBotoes(EstadoBotoes.CADASTRO);
            setAcaoCadastro(null);
            setControlesSomenteLeitura(true);
        }
    }

    public void tratarBotaoCancelar() {
        tbUsuarios.getSelectionModel().selectFirst();
        exibeDados(getEntidadeSelecionada());
        setEstadoBotoes(EstadoBotoes.CADASTRO);
        setAcaoCadastro(null);
        setControlesSomenteLeitura(true);
    }

    public void setEstadoBotoes(EstadoBotoes estado) {
        btCadastrar.setDisable(true);
        btEditar.setDisable(true);
        btExcluir.setDisable(true);
        btConfirmar.setDisable(true);
        btCancelar.setDisable(true);
        switch (estado) {
            case CADASTRO:
                btCadastrar.setDisable(false);
                btEditar.setDisable(false);
                btExcluir.setDisable(false);
                break;
            case CONFIRMACAO:
                btConfirmar.setDisable(false);
                btCancelar.setDisable(false);
                break;
        }
    }

    @FXML
    void handleBtCadastrar(ActionEvent event) {
        tratarBotaoCadastrar();
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        tratarBotaoCancelar();

    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        tratarBotaoConfirmar();
    }

    @FXML
    void handleBtEditar(ActionEvent event) {
        tratarBotaoEditar();
    }

    @FXML
    void handleBtExcluir(ActionEvent event) {
        tratarBotaoExcluir();
    }

}
