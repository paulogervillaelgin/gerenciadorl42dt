/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.UsuarioJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmCadastroUsuarioInicialController implements Initializable {

    @FXML
    private TextField txtNome;

    @FXML
    private TextField txtUsuario;

    @FXML
    private PasswordField txtSenha;

    @FXML
    private PasswordField txtConfirmaSenha;

    @FXML
    private Button btAvancar;

    @FXML
    private Button btVoltar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    private final UsuarioJpaController usuarioADO = new UsuarioJpaController(emf);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public boolean verificaDados() {
        boolean aux = true;
        txtNome.setEffect(null);
        txtUsuario.setEffect(null);
        txtSenha.setEffect(null);
        txtConfirmaSenha.setEffect(null);

        if (txtNome.getText() == null || txtNome.getText().isEmpty()) {
            txtNome.setEffect(Animacoes.getSombraErro());
            aux = false;
        }

        if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
            txtUsuario.setEffect(Animacoes.getSombraErro());
            aux = false;
        }

        if (txtSenha.getText() == null || txtSenha.getText().isEmpty()) {
            txtSenha.setEffect(Animacoes.getSombraErro());
            aux = false;
        }

        if (txtConfirmaSenha.getText() == null || txtConfirmaSenha.getText().isEmpty()) {
            txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
            aux = false;
        }

        //verifica se as senhas digitadas são as mesmas
        if ((txtSenha.getText() != null && !txtSenha.getText().isEmpty()) && (txtConfirmaSenha.getText() != null && !txtConfirmaSenha.getText().isEmpty())
                && (txtSenha.getText().equals(txtConfirmaSenha.getText()) == false)) {
            this.txtSenha.setEffect(Animacoes.getSombraErro());
            this.txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
            aux = false;
        }

        return aux;

    }

    @FXML
    void handleBtAvancar(ActionEvent event) {
        if (verificaDados()) {
            Usuario user = new Usuario();
            user.setAdm(Usuario.USUARIO_TIPO_ADM);
            user.setNome(txtNome.getText());
            user.setUsuario(txtUsuario.getText());
            user.setSenha(Utilidades.toSHA256(txtSenha.getText()));
            usuarioADO.create(user);
            Configuracao.salvar();
            FormHelper.gotoLogin();
        }
    }

    @FXML
    void handleBtVoltar(ActionEvent event) {
        FormHelper.closeSystem();
    }

}
