/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.DepartamentoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.ItemDepartamento;
import br.com.elgin.gerenciadorl42dt.ui.ItemImpressora;
import br.com.elgin.gerenciadorl42dt.ui.ItemSimplesFlow;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmConsultaDepartamentoController implements Initializable {

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private VBox pnDepartamentos;

    @FXML
    private VBox pnImpressoras;

    @FXML
    private FlowPane pnGrupos;

    @FXML
    private Label lbSemGrupo;

    @FXML
    private Label lbSemImpressora;

    @FXML
    private AnchorPane pnSemImpressora;

    @FXML
    private AnchorPane pnGeralDepartamentos;

    @FXML
    private AnchorPane pnGeralGrupos;

    @FXML
    private AnchorPane pnGeralBalancas;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);
    ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);

    private final ObservableList<Departamento> obsDepartamento = FXCollections.observableArrayList();

    private ItemDepartamento itemDepartamentoSelecionado;
    private ItemImpressora itemImpressoraSelecionado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        updateListaDepartamento();
        verificarPermissoes();

    }

    private void verificarPermissoes() {
        if (!Permissoes.verificarAdm()) {
            pnGeralGrupos.setDisable(true);
            pnGeralDepartamentos.setDisable(true);
            pnGeralBalancas.setDisable(true);
        }
    }

    private final EventHandler<MouseEvent> handleItemBalancaDragDetected = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemImpressora) {
            ItemImpressora item = (ItemImpressora) event.getSource();
            itemImpressoraSelecionado = item;
            Dragboard db = item.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.putString("impressora");
            db.setContent(content);
            event.consume();
        }
    };

    private final EventHandler<DragEvent> handleDragDropDepartamento = (DragEvent event) -> {
        if (event.getSource() instanceof ItemDepartamento) {
            ItemDepartamento itemdept = (ItemDepartamento) event.getSource();
            Dragboard db = event.getDragboard();
            if (event.getDragboard().getString().equals("impressora")) {
                if (itemImpressoraSelecionado != null && !itemdept.getDepartamento().equals(itemImpressoraSelecionado.getImpressora().getDepartamento())) {
                    Impressora imp = itemImpressoraSelecionado.getImpressora();
                    imp.setDepartamento(itemdept.getDepartamento());
                    //itemdept.getDepartamento().getBalancaSet().add(bal);

                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.PRODUTO.getValorStatus());
                    imp.setFlagAtualizada(imp.getFlagAtualizacao() | ImpressoraStatusEnum.LAYOUT.getValorStatus());

                    try {
                        daoImpressora.edit(imp);
                        //daoDepartamento.edit(itemdept.getDepartamento());
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    }

                    //Monitoramento.recarregarServicos();
                    updateListaDepartamento();
                }
            }
        }
        event.setDropCompleted(true);
        event.consume();
    };

    private final EventHandler<DragEvent> handleDragOverDepartamento = (DragEvent event) -> {
        // data is dragged over the target
        Dragboard db = event.getDragboard();
        if (event.getDragboard().hasString() && event.getDragboard().getString().equals("impressora")) {
            event.acceptTransferModes(TransferMode.MOVE);
        }
        event.consume();
    };

    private final EventHandler<DragEvent> handleDragDepartamentoEntered = (DragEvent event) -> {
        if (event.getSource() instanceof ItemDepartamento) {
            //((ItemGrupoProduto) event.getSource()).getStyleClass().clear();
            ((ItemDepartamento) event.getSource()).setEffect(Animacoes.getSombraOfColor(Color.BLUE));
            //((ItemGrupoProduto) event.getSource()).setSelecionado(true);
        }
    };

    private final EventHandler<DragEvent> handleDragDepartamentoExited = (DragEvent event) -> {
        if (event.getSource() instanceof ItemDepartamento) {
            //((ItemGrupoProduto) event.getSource()).getStyleClass().clear();
            ((ItemDepartamento) event.getSource()).setEffect(null);
            //((ItemGrupoProduto) event.getSource()).setSelecionado(false);

        }
    };

    private final EventHandler<MouseEvent> handleItemImpressoraClicked = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemImpressora) {
            ItemImpressora item = (ItemImpressora) event.getSource();
            if (event.getClickCount() == 2) {
                handleEditarImpressora(event);
            } else {
                if (!item.equals(itemImpressoraSelecionado)) {
                    if (itemImpressoraSelecionado != null) {
                        itemImpressoraSelecionado.setSelecionado(false);
                    }
                    itemImpressoraSelecionado = item;
                    item.setSelecionado(true);
                }
            }
        }
    };

    private final EventHandler<MouseEvent> handleItemDepartamentoClicked = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemDepartamento) {
            ItemDepartamento item = (ItemDepartamento) event.getSource();
            if (event.getClickCount() == 2) {
                handleEditarDepartamento(event);
            } else {
                if (!item.equals(itemDepartamentoSelecionado)) {
                    if (itemDepartamentoSelecionado != null) {
                        itemDepartamentoSelecionado.setSelecionado(false);
                    }
                    itemDepartamentoSelecionado = item;
                    item.setSelecionado(true);
                }

                updateListaImpressoras();
                updateListaGrupos();

                //itemBalancaSelecionado = null;
            }
        }
    };

    private class TaskBuscaDadosDepartamento extends Task<List<ItemDepartamento>> {

        EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
        DepartamentoJpaController daoDepartamento = new DepartamentoJpaController(emf);

        @Override
        protected List<ItemDepartamento> call() throws Exception {
            List<ItemDepartamento> lista = new LinkedList<>();
            List<Departamento> deptos = daoDepartamento.findDepartamentoEntities();
            for (Departamento depto : deptos) {
                ItemDepartamento item = new ItemDepartamento(depto);

                item.setOnDragDropped(handleDragDropDepartamento);
                item.setOnDragOver(handleDragOverDepartamento);
                item.setOnDragEntered(handleDragDepartamentoEntered);
                item.setOnDragExited(handleDragDepartamentoExited);

                item.setOnMouseClicked(handleItemDepartamentoClicked);
                lista.add(item);
                if (itemDepartamentoSelecionado != null && depto.getIndice().equals(itemDepartamentoSelecionado.getDepartamento().getIndice())) {
                    item.setSelecionado(true);
                    itemDepartamentoSelecionado = item;
                }
            }
            return lista;
        }

    }

    private void selecionarItemByDepartamento(Departamento valor) {
        for (Node nodo : pnDepartamentos.getChildren()) {
            if (nodo instanceof ItemDepartamento) {
                ItemDepartamento item = (ItemDepartamento) nodo;
                if (item.getDepartamento().equals(valor)) {
                    item.setSelecionado(true);
                    itemDepartamentoSelecionado = item;
                    return;
                }
            }
        }
    }

    private void updateListaDepartamento() {
        TaskBuscaDadosDepartamento task = new TaskBuscaDadosDepartamento();
        pnCarregando.setVisible(true);
        pnCarregando.toFront();
        lbCarregandoStatus.setText("Carregando departamentos...");
        piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        task.setOnSucceeded((event) -> {
            try {
                pnDepartamentos.getChildren().clear();
                if (task.getValue() != null & !task.getValue().isEmpty()) {
                    pnDepartamentos.getChildren().addAll(task.getValue());
                }
                if (itemDepartamentoSelecionado != null) {
                    selecionarItemByDepartamento(itemDepartamentoSelecionado.getDepartamento());
                } else {
                    if (!pnDepartamentos.getChildren().isEmpty() && (pnDepartamentos.getChildren().get(0) instanceof ItemDepartamento)) {
                        itemDepartamentoSelecionado = (ItemDepartamento) pnDepartamentos.getChildren().get(0);
                        itemDepartamentoSelecionado.setSelecionado(true);
                    }
                }
            } finally {
                updateListaImpressoras();
                updateListaGrupos();
                pnCarregando.setVisible(false);
            }
        });
        //databaseExecutor.submit(task);
        new Thread(task).start();
    }

    private void updateListaImpressoras() {
        pnImpressoras.getChildren().clear();
        if (itemDepartamentoSelecionado != null) {
            Departamento depto = daoDepartamento.findDepartamento(itemDepartamentoSelecionado.getDepartamento().getIndice());
            if (depto.getImpressoraSet() != null & !depto.getImpressoraSet().isEmpty()) {
                for (Impressora imp : depto.getImpressoraSet()) {
                    ItemImpressora item = new ItemImpressora(imp);
                    item.setOnDragDetected(handleItemBalancaDragDetected);
                    item.setOnMouseClicked(handleItemImpressoraClicked);
                    if (itemImpressoraSelecionado != null && itemImpressoraSelecionado.getImpressora().equals(imp)) {
                        itemImpressoraSelecionado = item;
                        item.setSelecionado(true);
                    }
                    pnImpressoras.getChildren().add(item);
                }
                if (itemImpressoraSelecionado == null || itemImpressoraSelecionado.getImpressora().getDepartamento() != depto) {
                    if (pnImpressoras.getChildren().get(0) instanceof ItemImpressora) {
                        itemImpressoraSelecionado = (ItemImpressora) pnImpressoras.getChildren().get(0);
                        itemImpressoraSelecionado.setSelecionado(true);
                    }
                }
            } else {
                itemImpressoraSelecionado = null;
                pnImpressoras.getChildren().add(pnSemImpressora);
            }
        }
    }

    private void updateListaGrupos() {
        pnGrupos.getChildren().clear();
        if (itemDepartamentoSelecionado != null) {
            if (itemDepartamentoSelecionado.getDepartamento().getGrupoprodutoSet() != null && !itemDepartamentoSelecionado.getDepartamento().getGrupoprodutoSet().isEmpty()) {
                for (Grupoproduto grupo : itemDepartamentoSelecionado.getDepartamento().getGrupoprodutoSet()) {
                    ItemSimplesFlow item = new ItemSimplesFlow(grupo.getDescricao());
                    pnGrupos.getChildren().add(item);
                }
            } else {
                pnGrupos.getChildren().add(lbSemGrupo);
            }
        }
    }

    @FXML
    void handleAdicionarDepartamento(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader
                    .setLocation(FormHelper.class
                            .getResource(FormHelper.FORM_CADASTRO_DEPARTAMENTO));
            Parent root = (Parent) fxmlLoader.load();
            FrmCadastroDepartamentoController controller = (FrmCadastroDepartamentoController) fxmlLoader.getController();
            controller.setDepartamento(null);
            controller.setOnClose((evento) -> {
                if (controller.getDepartamento() != null) {
                    Departamento depart = new Departamento();
                    depart.setDescricao(controller.getDepartamento().getDescricao());
                    daoDepartamento.create(depart);
                    itemDepartamentoSelecionado = new ItemDepartamento(depart);
                    updateListaDepartamento();
                }
            });

            FormHelper.getFormMenuController().empilhaFormulario(root);

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void handleEditarDepartamento(MouseEvent event) {
        if (itemDepartamentoSelecionado != null) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader
                        .setLocation(FormHelper.class
                                .getResource(FormHelper.FORM_CADASTRO_DEPARTAMENTO));
                Parent root = (Parent) fxmlLoader.load();
                FrmCadastroDepartamentoController controller = (FrmCadastroDepartamentoController) fxmlLoader.getController();

                controller.setDepartamento(itemDepartamentoSelecionado.getDepartamento());

                controller.setOnClose((ActionEvent evento) -> {
                    if (controller.getDepartamento() != null) {
                        try {
                            Departamento depto = daoDepartamento.findDepartamento(itemDepartamentoSelecionado.getDepartamento().getIndice());
                            depto.setDescricao(controller.getDepartamento().getDescricao());
                            daoDepartamento.edit(depto);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Dialog.showError("Erro", "Erro ao alterar grupo.");
                        }
                        updateListaDepartamento();
                    }
                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Dialog.showError("Erro", "Antes de editar, selecione um departamento clicando sobre o mesmo.");
        }
    }

    @FXML
    void handleDeletarDepartamento(MouseEvent event) {
        //
        // TODO: verificar se pode excluir e o que fazer com as balanças que ainda estao cadastradas
        //
        if (itemDepartamentoSelecionado != null) {
            if (itemDepartamentoSelecionado.getDepartamento().isPadrao()) {
                Dialog.showError("Erro", "Não é possivel excluir o departamento padrão.");
                return;
            }
            Dialog.buildConfirmation("Exclusão de registro", "Deseja realmente excluir este departamento ?\nDepartamento: " + itemDepartamentoSelecionado.getDepartamento().getDescricao()).addYesButton((Event t) -> {
                try {
                    daoDepartamento.destroy(itemDepartamentoSelecionado.getDepartamento().getIndice());
                    itemDepartamentoSelecionado = null;

                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(FrmConsultaProdutosController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                updateListaDepartamento();

                //recarregar lista de balanças
                //Monitoramento.recarregarServicos();
            }).addNoButton(null).build().show();
        } else {
            Dialog.showError("Erro", "Nenhum departamento selecionado para exclusão.");
        }
    }

    @FXML
    void handleAdicionarImpressora(MouseEvent event) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader
                    .setLocation(FormHelper.class
                            .getResource(FormHelper.FORM_CADASTRO_IMPRESSORA));
            Parent root = (Parent) fxmlLoader.load();
            FrmCadastroImpressoraController controller = (FrmCadastroImpressoraController) fxmlLoader.getController();

            // sinaliza que está adicionando
            controller.setImpressora(null);

            //se o departamento tiver selecionado, trazer na combo
            if (itemDepartamentoSelecionado != null) {
                controller.setDepartamento(itemDepartamentoSelecionado.getDepartamento());
            }

            pnCarregando.setVisible(true);
            pnCarregando.toFront();
            controller.setOnClose((evento) -> {
                if (controller.getImpressora() != null) {
                    daoImpressora.create(controller.getImpressora());

                    //recarregar lista de balanças
                    //Monitoramento.recarregarServicos();
                }
                pnCarregando.setVisible(false);

                updateListaDepartamento();

            });

            FormHelper.getFormMenuController().empilhaFormulario(root);

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    void handleDeletarImpressora(MouseEvent event) {
        if (itemImpressoraSelecionado == null) {
            Dialog.showWarning("Atenção", "Selecione a impressora que deseja excluir");
        } else {
            Dialog.buildConfirmation("Exclusão de registro", "Deseja realmente excluir esta impressora ?").addYesButton((Event t) -> {
                try {

                    daoImpressora.destroy(itemImpressoraSelecionado.getImpressora().getIndice());

                    itemImpressoraSelecionado = null;

                    updateListaDepartamento();

                    //recarregar lista de balanças
                    //Monitoramento.recarregarServicos();
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }).addNoButton(null).build().show();
        }
    }

    @FXML
    void handleEditarImpressora(MouseEvent event) {

        if (itemImpressoraSelecionado == null) {
            Dialog.showWarning("Atenção", "Selecione uma balança antes de alterar.");
        } else {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader
                        .setLocation(FormHelper.class
                                .getResource(FormHelper.FORM_CADASTRO_IMPRESSORA));
                Parent root = (Parent) fxmlLoader.load();
                FrmCadastroImpressoraController controller = (FrmCadastroImpressoraController) fxmlLoader.getController();

                // sinaliza que está adicionando
                controller.setImpressora(itemImpressoraSelecionado.getImpressora());
                pnCarregando.setVisible(true);
                pnCarregando.toFront();
                controller.setOnClose((evento) -> {
                    if (controller.getImpressora() != null) {
                        try {
                            daoImpressora.edit(controller.getImpressora());

                            //recarregar lista de balanças
                            //Monitoramento.recarregarServicos();
                        } catch (Exception ex) {
                            Logger.getLogger(FrmConsultaProdutosController.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    pnCarregando.setVisible(false);
                    updateListaDepartamento();

                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    @FXML
    void handleEditarGrupos(MouseEvent event) {
        if (itemDepartamentoSelecionado != null) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader
                        .setLocation(FormHelper.class
                                .getResource(FormHelper.FORM_ASSOCIA_GRUPO));
                Parent root = (Parent) fxmlLoader.load();
                FrmAssociaDepartamentoGrupoController controller = (FrmAssociaDepartamentoGrupoController) fxmlLoader.getController();

                controller.setSelecionados(itemDepartamentoSelecionado.getDepartamento().getGrupoprodutoSet());

                controller.setOnClose((evento) -> {

                    if (controller.getSelecionados() != null) {
                        try {
                            Departamento depto = daoDepartamento.findDepartamento(itemDepartamentoSelecionado.getDepartamento().getIndice());
                            depto.setGrupoprodutoSet(controller.getSelecionados());
                            daoDepartamento.edit(depto);

                            //seta as balanças como desatualizadas
                            ImpressoraStatusManager.updateStatusListaImpressora(depto.getImpressoraSet(), ImpressoraStatusEnum.PRODUTO, false);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Dialog.showError("Erro", "Erro ao alterar grupo.");
                        }
                        updateListaDepartamento();
                    }

                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Dialog.showError("Erro", "Antes de associar, selecione um departamento clicando sobre o mesmo.");
        }
    }

}
