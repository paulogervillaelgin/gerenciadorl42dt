/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.ItemGrupoProduto;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo
 */
public class FrmConsultaProdutosController implements Initializable {

    @FXML
    private VBox painel;

    @FXML
    private Button btProduto;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private TableView<Produto> tbProduto;

    @FXML
    private TableColumn<Produto, Integer> colCodigo;

    @FXML
    private TableColumn<Produto, String> colDescricao;

    @FXML
    private TableColumn<Produto, BigDecimal> colPreco;

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private FlowPane pnGrupos;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private ScrollPane scrollGrupo;

    @FXML
    private AnchorPane btAdicionarGrupo;

    @FXML
    private AnchorPane btEditarGrupo;

    @FXML
    private AnchorPane btExcluirGrupo;

    @FXML
    private AnchorPane btAdicionarProduto;

    @FXML
    private AnchorPane btEditarProduto;

    @FXML
    private AnchorPane btExcluirProduto;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ProdutoJpaController daoProduto = new ProdutoJpaController(emf);
    GrupoprodutoJpaController daoGrupo = new GrupoprodutoJpaController(emf);

    private final ObservableList<Produto> obsProdutos = FXCollections.observableArrayList();
    private final ObservableList<Produto> obsFiltroProdutos = FXCollections.observableArrayList();

    private final ObservableList<Grupoproduto> obsGrupos = FXCollections.observableArrayList();

    private List<Produto> listaProdutos = null;

    private ItemGrupoProduto itemGrupoSelecionado;
    private Produto produtoAlterando;

    private final EventHandler<DragEvent> handleDragDropGrupo = (DragEvent event) -> {
        TaskAgrupaProdutos task = new TaskAgrupaProdutos(((ItemGrupoProduto) event.getSource()).getGrupo(), listaProdutos);
        lbCarregandoStatus.textProperty().bind(task.messageProperty());
        piCarregando.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded((evento) -> {
            pnCarregando.setVisible(false);
            pnCarregando.getStyleClass().removeAll("frame_transparente_2");
            lbCarregandoStatus.textProperty().unbind();
            piCarregando.progressProperty().unbind();
            updateListaGrupo();
            updateFiltroTabela();
        });
        pnCarregando.setVisible(true);
        pnCarregando.getStyleClass().add("frame_transparente_2");
        pnCarregando.toFront();
        //databaseExecutor.submit(task);
        new Thread(task).start();
    };

    /*
    private void updateTotaisGrupos() {
        for (Node node : pnGrupos.getChildren()) {
            if(node instanceof ItemGrupoProduto){
                ((ItemGrupoProduto)node).updateInfos();
            }
        }
    }
     */
    private final EventHandler<DragEvent> handleDragOverGrupo = (DragEvent event) -> {
        // data is dragged over the target
        Dragboard db = event.getDragboard();
        if (event.getDragboard().hasString() && listaProdutos != null) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    };

    private final EventHandler<DragEvent> handleDragGrupoEntered = (DragEvent event) -> {
        if (event.getSource() instanceof ItemGrupoProduto) {
            //((ItemGrupoProduto) event.getSource()).getStyleClass().clear();
            ((ItemGrupoProduto) event.getSource()).setEffect(Animacoes.getSombraOfColor(Color.BLUE));
            //((ItemGrupoProduto) event.getSource()).setSelecionado(true);
        }
    };

    private final EventHandler<DragEvent> handleDragGrupoExited = (DragEvent event) -> {
        if (event.getSource() instanceof ItemGrupoProduto) {
            //((ItemGrupoProduto) event.getSource()).getStyleClass().clear();
            ((ItemGrupoProduto) event.getSource()).setEffect(null);
            //((ItemGrupoProduto) event.getSource()).setSelecionado(false);

        }
    };

    private final EventHandler<MouseEvent> handleItemGrupoClicked = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemGrupoProduto) {
            ItemGrupoProduto item = (ItemGrupoProduto) event.getSource();
            if (item.equals(itemGrupoSelecionado)) {
                itemGrupoSelecionado = null;
                item.setSelecionado(false);
            } else {
                if (itemGrupoSelecionado != null) {
                    itemGrupoSelecionado.setSelecionado(false);
                }
                itemGrupoSelecionado = item;
                item.setSelecionado(true);
            }
            updateFiltroTabela();
        }
    };

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        colCodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        colPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));

        colPreco.setCellFactory((TableColumn<Produto, BigDecimal> p) -> {
            TableCell<Produto, BigDecimal> cell = new TableCell<Produto, BigDecimal>() {
                @Override
                public void updateItem(BigDecimal item, boolean empty) {
                    setAlignment(Pos.CENTER_RIGHT);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else if (item != null) {

                        setText(Utilidades.formatMoeda(item));
                    }
                }
            };
            //System.out.println(cell.getIndex());
            return cell;
        });

        tbProduto.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        tbProduto.setOnDragDetected((MouseEvent event) -> {
            listaProdutos = tbProduto.getSelectionModel().getSelectedItems();

            Dragboard db = tbProduto.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString("produtos");
            db.setContent(content);
            event.consume();

        } //drag
        );

        tbProduto.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2) {
                handleEditarProduto(event);
            }
        });

        tbProduto.setOnKeyReleased((event) -> {
            switch (event.getCode()) {
                case DELETE:
                    handleDeletarProduto(null);
                    break;
                case ENTER:
                    handleEditarProduto(null);
                    break;
            }
        });

        //limparPaineis();
        //showProdutos();
        updateTabela();

        updateListaGrupo();

        txtPesquisa.setOnKeyReleased(
                (event) -> {
                    //if (txtPesquisa.getText().isEmpty() && event.getCode().equals(KeyCode.ENTER)) {
                    //showProdutos();
                    //   updateFiltroTabela();
                    //} else  else {
                    switch (event.getCode()) {
                        case UP:
                            if (event.isShiftDown()) {
                                tbProduto.getSelectionModel().selectPrevious();
                            } else if (tbProduto.getSelectionModel().getSelectedIndex() > 0) {
                                tbProduto.getSelectionModel().clearAndSelect(tbProduto.getSelectionModel().getSelectedIndex() - 1);
                            }
                            event.consume();
                            break;
                        case DOWN:
                            if (event.isShiftDown()) {
                                tbProduto.getSelectionModel().selectNext();
                            } else {
                                tbProduto.getSelectionModel().clearAndSelect(tbProduto.getSelectionModel().getSelectedIndex() + 1);
                            }
                            event.consume();
                            break;
                        case ENTER:
                            tbProduto.requestFocus();
                            event.consume();
                            break;
                        default:
                            updateFiltroTabela();
                            break;
                    }

                }
        );

        verificarPermissoes();

    }

    private void verificarPermissoes() {
        if (!Permissoes.verificarAdm()) {
            tbProduto.setOnDragDetected(null);
            tbProduto.setOnKeyReleased(null);
            
            btAdicionarGrupo.setDisable(true);
            btEditarGrupo.setDisable(true);
            btExcluirGrupo.setDisable(true);

            btAdicionarProduto.setDisable(true);
            btExcluirProduto.setDisable(true);
        }
    }

    private void updateFiltroTabela() {
        obsFiltroProdutos.clear();

        for (Produto prod : obsProdutos) {
            if (matchesFilter(prod)) {
                obsFiltroProdutos.add(prod);
            }
        }
        colCodigo.setVisible(false);
        colCodigo.setVisible(true);

        tbProduto.setItems(obsFiltroProdutos);

    }

    private boolean matchesFilter(Produto produto) {
        boolean aux = false;

        if (itemGrupoSelecionado != null) {
            if (produto.getGrupoproduto() == null) {
                return false;
            }
            if (!produto.getGrupoproduto().getIndice().equals(itemGrupoSelecionado.getGrupo().getIndice())) {
                return false;
            }
        }

        String filterString = txtPesquisa.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }

        String lowerCaseFilterString = filterString.toLowerCase();

        if (produto.getDescricao() != null && produto.getDescricao().toLowerCase().contains(lowerCaseFilterString)) {
            aux = true;
        } else if (String.valueOf(produto.getCodigo()).contains(lowerCaseFilterString)) {
            aux = true;
        } else {
            aux = false;
        }

        return aux; // Does not match

    }

    private class TaskAgrupaProdutos extends Task {

        private Grupoproduto grupo;
        List<Produto> lista;

        public Grupoproduto getGrupo() {
            return grupo;
        }

        public void setGrupo(Grupoproduto grupo) {
            this.grupo = grupo;
        }

        public List<Produto> getLista() {
            return lista;
        }

        public void setLista(List<Produto> lista) {
            this.lista = lista;
        }

        public TaskAgrupaProdutos(Grupoproduto grupoprod, List<Produto> listaprod) {
            grupo = grupoprod;
            lista = listaprod;
        }

        @Override
        protected Object call() throws Exception {
            long total = 0;
            for (Produto produto : lista) {
                updateProgress(++total, lista.size());
                produto.setGrupoproduto(grupo);
                try {
                    daoProduto.edit(produto);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return null;
        }

    }

    private class TaskBuscaDados extends Task<List<Produto>> {

        @Override
        protected List<Produto> call() throws Exception {
            return daoProduto.findProdutoEntities();
        }

    }

    private class TaskBuscaDadosGrupos extends Task<List<ItemGrupoProduto>> {

        @Override
        protected List<ItemGrupoProduto> call() throws Exception {
            List<ItemGrupoProduto> lista = new LinkedList<>();
            List<Grupoproduto> grupos = daoGrupo.findGrupoprodutoEntities();
            grupos.forEach((grupo) -> {
                ItemGrupoProduto item = new ItemGrupoProduto(grupo);
                item.setOnDragDropped(handleDragDropGrupo);
                item.setOnDragOver(handleDragOverGrupo);
                item.setOnDragEntered(handleDragGrupoEntered);
                item.setOnDragExited(handleDragGrupoExited);
                item.setOnMouseClicked(handleItemGrupoClicked);
                lista.add(item);
                if (itemGrupoSelecionado != null && grupo.getIndice().equals(itemGrupoSelecionado.getGrupo().getIndice())) {
                    item.setSelecionado(true);
                    itemGrupoSelecionado = item;
                }
            });
            return lista;
        }

    }

    private ScrollBar getVerticalScrollbar(ScrollPane pane) {
        ScrollBar result = null;
        for (Node n : pane.lookupAll(".scroll-bar")) {
            if (n instanceof ScrollBar) {
                ScrollBar bar = (ScrollBar) n;
                if (bar.getOrientation().equals(Orientation.VERTICAL)) {
                    result = bar;
                }
            }
        }
        return result;
    }

    private void updateListaGrupo() {
        TaskBuscaDadosGrupos task = new TaskBuscaDadosGrupos();
        pnCarregando.setVisible(true);
        pnCarregando.getStyleClass().add("frame_transparente_2");
        pnCarregando.toFront();
        lbCarregandoStatus.setText("Carregando grupos...");
        piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        task.setOnSucceeded((event) -> {
            try {
                pnGrupos.getChildren().clear();
                pnGrupos.getChildren().addAll(task.getValue());

                //TODO tratar largura dos componentes quando scrollbar tiver visivel
                /*
                if (getVerticalScrollbar(scrollGrupo).get) {
                    pnGrupos.setPrefWidth(100);
                } else {
                    pnGrupos.setPrefWidth(160);
                }
                 */
            } finally {
                pnCarregando.setVisible(false);
                pnCarregando.getStyleClass().removeAll("frame_transparente_2");
                pnCarregando.toBack();
            }
        });
        //databaseExecutor.submit(task);
        new Thread(task).start();
    }

    private void updateTabela() {
        TaskBuscaDados task = new TaskBuscaDados();
        pnCarregando.setVisible(true);
        pnCarregando.getStyleClass().add("frame_transparente_2");
        piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        lbCarregandoStatus.setText("Carregando produtos...");
        task.setOnSucceeded((WorkerStateEvent event) -> {
            try {
                colDescricao.setVisible(false);
                colDescricao.setVisible(true);

                List results = task.getValue();
                //if (obsProdutos == null) {
                //    obsProdutos = FXCollections.observableArrayList(results);
                //} else {
                obsProdutos.clear();
                obsProdutos.addAll(results);
                //}

                tbProduto.setItems(obsProdutos);

                if (itemGrupoSelecionado != null || (txtPesquisa.getText() != null && !txtPesquisa.getText().isEmpty())) {
                    updateFiltroTabela();
                }

                if (produtoAlterando != null) {
                    tbProduto.getSelectionModel().select(produtoAlterando);
                    produtoAlterando = null;
                }

            } finally {
                pnCarregando.setVisible(false);
                pnCarregando.getStyleClass().removeAll("frame_transparente_2");
            }
        }
        );
        new Thread(task).start();
        //databaseExecutor.submit(task);
    }

    @FXML
    void handleAdicionarGrupo(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_CADASTRO_GRUPOS));
            Parent root = (Parent) fxmlLoader.load();
            FrmCadastroGrupoProdutoController controller = (FrmCadastroGrupoProdutoController) fxmlLoader.getController();

            controller.setOnClose((evento) -> {
                if (controller.getDescricao() != null) {
                    Grupoproduto grupo = new Grupoproduto();
                    grupo.setDescricao(controller.getDescricao());
                    daoGrupo.create(grupo);
                    updateListaGrupo();
                }
            });

            FormHelper.getFormMenuController().empilhaFormulario(root);

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    void handleDeletarGrupo(MouseEvent event) {
        if (itemGrupoSelecionado != null) {
            if (itemGrupoSelecionado.getGrupo().isPadrao()) {
                Dialog.showError("Erro", "Não é possível excluir o grupo padrão.");
                return;
            }
            Dialog.buildConfirmation("Exclusão de registro", "Deseja realmente excluir este grupo ?\nGrupo: " + itemGrupoSelecionado.getGrupo().getDescricao()).addYesButton((Event t) -> {
                try {
                    daoGrupo.destroy(itemGrupoSelecionado.getGrupo().getIndice());
                    itemGrupoSelecionado = null;

                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(FrmConsultaProdutosController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                updateListaGrupo();
                updateFiltroTabela();
            }).addNoButton(null).build().show();
        } else {
            Dialog.showError("Erro", "Nenhum grupo selecionado para exclusão.");
        }
    }

    @FXML
    void handleEditarGrupo(MouseEvent event) {
        if (itemGrupoSelecionado != null) {
            if (itemGrupoSelecionado.getGrupo().isPadrao()) {
                Dialog.showError("Erro", "Não é possível editar o grupo padrão.");
                return;
            }
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_CADASTRO_GRUPOS));
                Parent root = (Parent) fxmlLoader.load();
                FrmCadastroGrupoProdutoController controller = (FrmCadastroGrupoProdutoController) fxmlLoader.getController();

                controller.setDescricao(itemGrupoSelecionado.getGrupo().getDescricao());

                controller.setOnClose((evento) -> {
                    if (controller.getDescricao() != null) {
                        try {
                            Grupoproduto grupo = daoGrupo.findGrupoproduto(itemGrupoSelecionado.getGrupo().getIndice());
                            grupo.setDescricao(controller.getDescricao());
                            daoGrupo.edit(grupo);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Dialog.showError("Erro", "Erro ao alterar grupo.");
                        }
                        updateListaGrupo();
                    }
                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Dialog.showError("Erro", "Antes de editar, selecione um grupo de produto clicando sobre o mesmo.");
        }
    }

    @FXML
    void handleAdicionarProduto(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_CADASTRO_PRODUTO));
            Parent root = (Parent) fxmlLoader.load();
            FrmCadastroProdutoController controller = (FrmCadastroProdutoController) fxmlLoader.getController();

            // sinaliza que está adicionando
            controller.setProduto(null);

            //Traz o grupo selecionado para o cadastro de produto
            if (itemGrupoSelecionado != null) {
                controller.setGrupoProduto(itemGrupoSelecionado.getGrupo());
            }

            pnCarregando.setVisible(true);
            pnCarregando.toFront();
            controller.setOnClose((evento) -> {
                if (controller.getProduto() != null) {
                    daoProduto.create(controller.getProduto());

                    ImpressoraStatusManager.updateImpressorasOf(controller.getProduto(), false);
                    produtoAlterando = controller.getProduto();
                }
                pnCarregando.setVisible(false);
                updateTabela();
                updateListaGrupo();
            });

            FormHelper.getFormMenuController().empilhaFormulario(root);

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    void handleDeletarProduto(MouseEvent event) {
        if (tbProduto.getSelectionModel().getSelectedItems().isEmpty()) {
            Dialog.showWarning("Atenção", "Selecione os produtos que deseja excluir");
        } else {
            Dialog.buildConfirmation("Exclusão de registro", "Deseja realmente excluir estes produtos ?").addYesButton((Event t) -> {
                try {
                    for (Produto prod : tbProduto.getSelectionModel().getSelectedItems()) {
                        //trata a restrição do DIVERSOS
                        if (prod.getCodigo() == 0) {
                            Dialog.showWarning("Atenção", "Não é possivel excluir o produto DIVERSOS.");
                        } else {
                            daoProduto.destroy(prod.getIndice());
                        }
                    }
                    updateTabela();
                    updateListaGrupo();
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }).addNoButton(null).build().show();
        }
    }

    @FXML
    void handleEditarProduto(MouseEvent event) {
        if (tbProduto.getSelectionModel().getSelectedItems().size() > 1) {
            Dialog.showWarning("Atenção", "Só é possivel alterar um produto por vez");
        } else {
            if (tbProduto.getSelectionModel().getSelectedItem() != null) {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_CADASTRO_PRODUTO));
                    Parent root = (Parent) fxmlLoader.load();
                    FrmCadastroProdutoController controller = (FrmCadastroProdutoController) fxmlLoader.getController();
                    // sinaliza que está adicionando
                    controller.setProduto(tbProduto.getSelectionModel().getSelectedItem());
                    pnCarregando.setVisible(true);
                    pnCarregando.toFront();
                    controller.setOnClose((evento) -> {
                        if (controller.getProduto() != null) {

                            try {
                                daoProduto.edit(controller.getProduto());

                                ImpressoraStatusManager.updateImpressorasOf(controller.getProduto(), false);
                            } catch (Exception ex) {
                                Logger.getLogger(FrmConsultaProdutosController.class
                                        .getName()).log(Level.SEVERE, null, ex);
                            }

                        }

                        pnCarregando.setVisible(false);
                        updateTabela();
                        updateListaGrupo();

                    });
                    produtoAlterando = controller.getProduto();
                    FormHelper.getFormMenuController().empilhaFormulario(root);

                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Dialog.showWarning("Atenção", "Selecione um produto antes de alterar.");
            }
        }
    }

    public void getProdutosFromFile(File arquivo) {

        Scanner scanner;
        try {
            scanner = new Scanner(arquivo, "windows-1252");
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.trim().length() >= 36) {
                    line = line.trim().substring(0, 36);
                    String codigo = line.substring(0, 6);
                    String tipo = line.substring(6, 7);
                    String descricao = line.substring(7, 29);
                    String preco = line.substring(29, 36);
                    //String validade = line.substring(36, 39);
                    Produto prod = new Produto();
                    prod.setCodigo(Integer.valueOf(codigo));
                    prod.setDescricao(descricao);
                    prod.setPreco(BigDecimal.valueOf(Double.parseDouble(preco) / 100));
                    prod.setPesado(tipo.equals("P"));
                    daoProduto.create(prod);
                } else {
                    //adicionar no log sobre possiveis linhas fora do padrão!
                    System.out.println("Linha : \"" + line + "\" fora de padrão");
                }
            }
            scanner.close();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

}
