package br.com.elgin.gerenciadorl42dt.ui.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.com.elgin.gerenciadorl42dt.DAO.UsuarioJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmAlteraSenhaController implements Initializable {

    @FXML
    private PasswordField txtSenhaAtual;

    @FXML
    private PasswordField txtSenha;

    @FXML
    private PasswordField txtConfirmaSenha;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    UsuarioJpaController daoUsuario = new UsuarioJpaController(emf);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    private boolean verificaCampos() {
        boolean res = true;
        txtSenhaAtual.setEffect(null);
        txtSenha.setEffect(null);
        txtConfirmaSenha.setEffect(null);

        if (txtSenhaAtual.getText().isEmpty()) {
            txtSenhaAtual.setEffect(Animacoes.getSombraErro());
            res = false;
        } else {

            if (daoUsuario.verificarSenha(SessaoAtual.getUsuario().getUsuario(), Utilidades.toSHA256(txtSenhaAtual.getText())) == null) {
                txtSenhaAtual.setEffect(Animacoes.getSombraErro());
                res = false;
            }
        }

        if (txtSenha.getText().isEmpty()) {
            txtSenha.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (txtConfirmaSenha.getText().isEmpty()) {
            txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if ((!txtSenha.getText().isEmpty()) && (!txtConfirmaSenha.getText().isEmpty()) && (txtSenha.getText().equals(txtConfirmaSenha.getText()) == false)) {
            txtSenha.setEffect(Animacoes.getSombraErro());
            txtConfirmaSenha.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        return res;
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaCampos() == true) {
            Usuario usuario = daoUsuario.findUsuario(SessaoAtual.getUsuario().getIndice());
            usuario.setSenha(Utilidades.toSHA256(txtSenha.getText()));
            try {
                daoUsuario.edit(usuario);
            } catch (Exception ex) {
                Logger.getLogger(FrmAlteraSenhaController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Dialog.showInfo("Alteração de senha", "Senha alterada com sucesso!");
            FormHelper.getFormMenuController().desempilhaFormulario();
        }
    }

}
