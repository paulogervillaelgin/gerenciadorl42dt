/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.FlexibarcodeJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.exceptions.NonexistentEntityException;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo
 */
public class FrmConsultaFlexiBarcodeController implements Initializable, Formulario {

    @FXML
    private VBox pnBarCode;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    FlexibarcodeJpaController daoBarcode = new FlexibarcodeJpaController(emf);

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    private final EventHandler<MouseEvent> handleItemBarcodeClicked = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemBarcode) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader
                        .setLocation(FormHelper.class
                                .getResource(FormHelper.FORM_CADASTRO_FLEXIBARCODE));
                Parent root = (Parent) fxmlLoader.load();
                FrmCadastrarFlexibarcodeController controller = (FrmCadastrarFlexibarcodeController) fxmlLoader.getController();

                controller.setFlexibarcode(((ItemBarcode) event.getSource()).getFlexibarcode());

                controller.setOnClose((ActionEvent evento) -> {
                    if (controller.getFlexibarcode() != null) {

                        try {
                            daoBarcode.edit(controller.getFlexibarcode());

                            //TODO aalterar status das impressoras tambem ?
                            //BalancaStatusManager.updateBalancasOf(controller.getFlexibarcode(), false);

                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(FrmConsultaFlexiBarcodeController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(FrmConsultaFlexiBarcodeController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        updateListaFlexiBarcode();

                    }
                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        event.consume();
    };

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        updateListaFlexiBarcode();
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    private void updateListaFlexiBarcode() {
        pnBarCode.getChildren().clear();
        for (Flexibarcode flex : daoBarcode.findFlexibarcodeEntities()) {
            ItemBarcode item = new ItemBarcode(flex);
            item.setOnMouseClicked(handleItemBarcodeClicked);
            pnBarCode.getChildren().add(item);
        }
    }

    private class ItemBarcode extends AnchorPane {

        private final Label lbDescricao = new Label();
        //private final Label lbFormato = new Label();

        private Flexibarcode flexibarcode;

        public Flexibarcode getFlexibarcode() {
            return flexibarcode;
        }

        public void setFlexibarcode(Flexibarcode flexibarcode) {
            this.flexibarcode = flexibarcode;
            if (flexibarcode != null) {
                lbDescricao.setText(flexibarcode.getDescricao());
                //lbFormato.setText(flexibarcode.getStringFormato());
                HBox box = Animacoes.getCodigoFlexbarcode(flexibarcode, 16d);
                AnchorPane.setRightAnchor(box, 10.0);
                AnchorPane.setTopAnchor(box, 5.0);
                AnchorPane.setBottomAnchor(box, 5.0);
                getChildren().add(box);
            }
        }

        public ItemBarcode(Flexibarcode flexi) {
            setVisible(true);
            getStyleClass().add("item");
            getStyleClass().add("frame_radius_all");

            setCursor(Cursor.HAND);

            lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
            //lbDescricao.setTextFill(Color.WHITE);
            lbDescricao.setStyle("-fx-text-fill:-cor-base;");
            AnchorPane.setLeftAnchor(lbDescricao, 10.0);
            AnchorPane.setTopAnchor(lbDescricao, 6.0);
            AnchorPane.setBottomAnchor(lbDescricao, 6.0);
            lbDescricao.setVisible(true);

            /*
            lbFormato.setFont(Font.font("Roboto", FontWeight.BOLD, 16));
            lbFormato.setTextFill(Color.WHITE);
            AnchorPane.setRightAnchor(lbFormato, 10.0);
            AnchorPane.setTopAnchor(lbFormato, 5.0);
            AnchorPane.setBottomAnchor(lbFormato, 5.0);
            lbFormato.setVisible(true);
             */
            getChildren().addAll(lbDescricao);

            setFlexibarcode(flexi);
        }
    }

}
