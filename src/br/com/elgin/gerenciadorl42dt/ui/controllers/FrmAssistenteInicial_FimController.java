/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.FormularioWizard;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmAssistenteInicial_FimController extends FormularioWizard implements Initializable {

    @FXML
    private Button btAvancar;

    @FXML
    private Button btVoltar;

    @FXML
    private Label lbDados;

    private ProgressIndicator pgProgresso;

    @FXML
    void handleBtAvancar(ActionEvent event) {
        Avancar();
    }

    @FXML
    void handleBtVoltar(ActionEvent event) {
        Voltar();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbDados.setText(lbDados.getText() + "\nUsuário : admin");
        lbDados.setText(lbDados.getText() + "\nSenha : admin");
    }

    @Override
    public void setValores(Properties mapa) {
        super.setValores(mapa);
    }

    @Override
    public boolean verificaDados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Voltar() {
        //getWizard().loadFromStack();
    }

    @Override
    public void Avancar() {
        FormHelper.gotoLogin();
    }

}
