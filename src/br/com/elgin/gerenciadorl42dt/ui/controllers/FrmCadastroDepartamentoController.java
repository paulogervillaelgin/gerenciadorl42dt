/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Departamento;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmCadastroDepartamentoController implements Initializable, Formulario {

    @FXML
    private TextField txtDescricao;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private AnchorPane pnDepto;

    private Departamento departamento;

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
        if (departamento != null) {
            exibeDados(departamento);
        }
    }

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
            txtDescricao.requestFocus();
        });
    }

    private void exibeDados(Departamento depto) {
        if (depto.isPadrao()) {
            txtDescricao.setDisable(true);
        }
        txtDescricao.setText(depto.getDescricao());
    }

    private boolean verificaDados() {
        boolean res = true;
        txtDescricao.setEffect(null);

        if (txtDescricao.getText() == null || txtDescricao.getText().isEmpty()) {
            res = false;
            txtDescricao.setEffect(Animacoes.getSombraErro());
        }

        return res;
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        setDepartamento(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaDados()) {
            Departamento depto;
            if (getDepartamento() == null) {
                depto = new Departamento();
            } else {
                depto = getDepartamento();
            }
            depto.setDescricao(txtDescricao.getText());
            setDepartamento(depto);
            FormHelper.getFormMenuController().desempilhaFormulario();
            onClose();

        }
    }

}
