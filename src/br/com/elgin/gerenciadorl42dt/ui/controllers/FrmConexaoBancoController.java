/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.AbstractConexaoBD;
import br.com.elgin.gerenciadorl42dt.DAO.UsuarioJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import br.com.elgin.gerenciadorl42dt.TaskConexaoBanco;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmConexaoBancoController implements Initializable {

    @FXML
    private ProgressIndicator piAguarde;

    @FXML
    private Label lbStatus;

    @FXML
    private ImageView imgAlerta;

    @FXML
    private Button btNovamente;

    private AbstractConexaoBD conexaoBD;

    public AbstractConexaoBD getConexaoBD() {
        return conexaoBD;
    }

    public void setConexaoBD(AbstractConexaoBD conexaoBD) {
        this.conexaoBD = conexaoBD;
        criarTask();
    }

    public void showOK(boolean aux) {
        if (aux) {
            piAguarde.setProgress(1);
        } else {
            piAguarde.setVisible(false);
        }
        imgAlerta.setVisible(!aux);
        btNovamente.setVisible(!aux);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        piAguarde.setVisible(true);
        imgAlerta.setVisible(false);
        btNovamente.setOnAction((event) -> {
            criarTask();
            btNovamente.setVisible(false);
        });
    }

    public void criarTask() {

        TaskConexaoBanco task = new TaskConexaoBanco(conexaoBD);

        lbStatus.textProperty().bind(task.messageProperty());

        task.setOnRunning((event) -> {
            piAguarde.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            piAguarde.setVisible(true);
            btNovamente.setVisible(false);
            imgAlerta.setVisible(false);
        });

        task.setOnSucceeded((event) -> {
            if (!Configuracao.isServico()) {
                try {
                    showOK(task.get() != null);
                    if (task.get() != null) {
                        //* PRG - 1.3.5
                        /*
                        if (Configuracao.ARQUIVOCONFIG.exists()) {
                            FormHelper.gotoLogin();
                        } else {
                            FormHelper.gotoCriaAdm();
                        }
                        */
                        if (!Configuracao.ARQUIVOCONFIG.exists()) {
                            EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
                            UsuarioJpaController usuarioADO = new UsuarioJpaController(emf);

                            Usuario user = new Usuario();
                            
                            user.setAdm(Usuario.USUARIO_TIPO_ADM);
                            user.setNome("Administrador");
                            user.setUsuario("admin");
                            user.setSenha("8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918");
                            
                            usuarioADO.create(user);
                            
                            Configuracao.setLoginautomatico(true);
                            Configuracao.setLoginautomaticoTmp(Configuracao.isLoginAutomatico());
                            Configuracao.salvar();
                            
                            FormHelper.gotoAssistenteFim();
                        } else {
                            FormHelper.gotoLogin();
                        }
                        //*
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                } catch (ExecutionException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        new Thread(task).start();
    }

}
