/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.FlexibarcodeJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.FormatoEtiquetaJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.LojaJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.model.Loja;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.ImageHelper;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmConfigLojaController implements Initializable {

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private ComboBox<FormatoEtiqueta> cbEtiqueta_Pesado;

    @FXML
    private ComboBox<FormatoEtiqueta> cbEtiqueta_Unitario;

    @FXML
    private ComboBox<Flexibarcode> cbCodigoBarras_Pesado;

    @FXML
    private TextField txtFlag_Pesado;

    @FXML
    private ComboBox<Flexibarcode> cbCodigoBarras_Unitario;

    @FXML
    private TextField txtFlag_Unitario;

    @FXML
    private TextField txtDescricao;

    @FXML
    private AnchorPane pnVisualiza1;

    @FXML
    private AnchorPane pnVisualiza2;
    
    @FXML
    private CheckBox ckContinuo;
    
    @FXML
    private ComboBox cbIntensidadeImpressao;
    
    //* Paulo Gervilla - 1.3.5
    @FXML
    private AnchorPane pnLoginAutomatico;
    @FXML
    private CheckBox ckLoginAutomatico;
    //*

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    FlexibarcodeJpaController daoBarcode = new FlexibarcodeJpaController(emf);
    FormatoEtiquetaJpaController daoEtiqueta = new FormatoEtiquetaJpaController(emf);
    ProdutoJpaController daoProduto = new ProdutoJpaController(emf);
    LojaJpaController daoLoja = new LojaJpaController(emf);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //cbIntensidadeImpressao.getItems().addAll(
        //    "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"
        //);        
        cbIntensidadeImpressao.getItems().addAll(IntStream.range(0, 15+1).boxed().collect(Collectors.toList()));
                
        
        List<FormatoEtiqueta> listaEtiquetasPesado = daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOPESADO);
        List<FormatoEtiqueta> listaEtiquetasUnidade = daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOUNIDADE);

        cbEtiqueta_Pesado.setItems(FXCollections.observableArrayList(listaEtiquetasPesado));
        cbEtiqueta_Pesado.setCellFactory((ListView<FormatoEtiqueta> param) -> {
            final ListCell<FormatoEtiqueta> cell = new ListCell<FormatoEtiqueta>() {
                @Override
                public void updateItem(FormatoEtiqueta item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText(null);
                    }
                }
            };
            return cell;
        });
        cbEtiqueta_Pesado.setConverter(new StringConverter<FormatoEtiqueta>() {
            @Override
            public String toString(FormatoEtiqueta object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public FormatoEtiqueta fromString(String string) {
                return null;
            }
        });

        /////////////////////////////////////////////////////////////
        cbEtiqueta_Unitario.setItems(FXCollections.observableArrayList(listaEtiquetasUnidade));
        cbEtiqueta_Unitario.setCellFactory((ListView<FormatoEtiqueta> param) -> {
            final ListCell<FormatoEtiqueta> cell = new ListCell<FormatoEtiqueta>() {
                @Override
                public void updateItem(FormatoEtiqueta item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText("<Nenhum>");
                    }
                }
            };
            return cell;
        });
        cbEtiqueta_Unitario.setConverter(new StringConverter<FormatoEtiqueta>() {
            @Override
            public String toString(FormatoEtiqueta object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public FormatoEtiqueta fromString(String string) {
                return null;
            }
        });

        /////////////////////////////////////////////////////////////
        List<Flexibarcode> listaBarcodes_pesado = daoBarcode.findFlexibarcodeByProdutoQtd(false);
        List<Flexibarcode> listaBarcodes_qtd = daoBarcode.findFlexibarcodeByProdutoQtd(true);

        cbCodigoBarras_Pesado.setItems(FXCollections.observableArrayList(listaBarcodes_pesado));
        cbCodigoBarras_Pesado.setCellFactory((ListView<Flexibarcode> param) -> {
            final ListCell<Flexibarcode> cell = new ListCell<Flexibarcode>() {
                @Override
                public void updateItem(Flexibarcode item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setContentDisplay(ContentDisplay.RIGHT);
                        setText(item.getDescricao());
                        setGraphic(Animacoes.getCodigoFlexbarcode(item, 16d));
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
            return cell;
        });

        cbCodigoBarras_Pesado.setConverter(new StringConverter<Flexibarcode>() {
            @Override
            public String toString(Flexibarcode object) {
                if (object != null) {
                    return object.getDescricao() + " ( " + Flexibarcode.TIPOS.get(object.getTipo()) + " - " + object.getStringFormato() + " )";
                } else {
                    return null;
                }
            }

            @Override
            public Flexibarcode fromString(String string) {
                return null;
            }
        });

        cbCodigoBarras_Unitario.setItems(FXCollections.observableArrayList(listaBarcodes_qtd));
        cbCodigoBarras_Unitario.setCellFactory((ListView<Flexibarcode> param) -> {
            final ListCell<Flexibarcode> cell = new ListCell<Flexibarcode>() {
                @Override
                public void updateItem(Flexibarcode item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setContentDisplay(ContentDisplay.RIGHT);
                        setText(item.getDescricao());
                        setGraphic(Animacoes.getCodigoFlexbarcode(item, 16d));
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
            return cell;
        });

        cbCodigoBarras_Unitario.setConverter(new StringConverter<Flexibarcode>() {
            @Override
            public String toString(Flexibarcode object) {
                if (object != null) {
                    return object.getDescricao() + " ( " + Flexibarcode.TIPOS.get(object.getTipo()) + " - " + object.getStringFormato() + " )";
                } else {
                    return null;
                }
            }

            @Override
            public Flexibarcode fromString(String string) {
                return null;
            }
        });

        /////////////////////////////////////////////////////////////
        cbCodigoBarras_Pesado.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            /*
            if (newValue != null) {
                switch (newValue.getFlag()) {
                    case 0:
                        txtFlag1_Pesado.setDisable(true);
                        txtFlag1_Pesado.setText("");
                        txtFlag2_Pesado.setDisable(false);
                        break;
                    case 1:
                        txtFlag1_Pesado.setDisable(false);
                        txtFlag2_Pesado.setDisable(false);
                        txtFlag1_Pesado.setText("");
                        txtFlag2_Pesado.setText("");
                        break;
                    case 2:
                        txtFlag1_Pesado.setDisable(true);
                        txtFlag2_Pesado.setDisable(true);
                        txtFlag1_Pesado.setText("");
                        txtFlag2_Pesado.setText("");
                        break;
                }
            }
             */
        });

        txtFlag_Pesado.setOnKeyReleased((event) -> {
            if (txtFlag_Pesado.getText().length() > 1) {
                txtFlag_Pesado.setText(txtFlag_Pesado.getText().substring(txtFlag_Pesado.getText().length() - 1));
                txtFlag_Pesado.positionCaret(txtFlag_Pesado.getText().length());
            }
            try {
                Integer.valueOf(txtFlag_Pesado.getText());
            } catch (NumberFormatException e) {
                txtFlag_Pesado.setText("");
            }
        });

        /////////////////////////////////////////////////////////////
        cbCodigoBarras_Unitario.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            /*
            if (newValue != null) {
                switch (newValue.getFlag()) {
                    case 0:
                        txtFlag1_Unitario.setDisable(true);
                        txtFlag1_Unitario.setText("");
                        txtFlag2_Unitario.setDisable(false);
                        break;
                    case 1:
                        txtFlag1_Unitario.setDisable(false);
                        txtFlag2_Unitario.setDisable(false);
                        txtFlag1_Unitario.setText("");
                        txtFlag2_Unitario.setText("");
                        break;
                    case 2:
                        txtFlag1_Unitario.setDisable(true);
                        txtFlag2_Unitario.setDisable(true);
                        txtFlag1_Unitario.setText("");
                        txtFlag2_Unitario.setText("");
                        break;
                }
            }
             */
        });

        txtFlag_Unitario.setOnKeyReleased((event) -> {
            if (txtFlag_Unitario.getText().length() > 1) {
                txtFlag_Unitario.setText(txtFlag_Unitario.getText().substring(txtFlag_Unitario.getText().length() - 1));
                txtFlag_Unitario.positionCaret(txtFlag_Unitario.getText().length());
            }
            try {
                Integer.valueOf(txtFlag_Unitario.getText());
            } catch (NumberFormatException e) {
                txtFlag_Unitario.setText("");
            }
        });

        exibeDados(SessaoAtual.getLoja());

        Platform.runLater(() -> {
            txtDescricao.requestFocus();
        });

    }

    private void exibeDados(Loja loj) {

        Loja loja = daoLoja.findLoja(loj.getIndice());
        if (loja != null) {

            cbCodigoBarras_Pesado.getSelectionModel().select(loja.getCodigobarrasPesado());
            txtFlag_Pesado.setText(loja.getFlagPesado() != null ? loja.getFlagPesado().toString() : "");

            cbCodigoBarras_Unitario.getSelectionModel().select(loja.getCodigobarrasUnitario());
            txtFlag_Unitario.setText(loja.getFlagUnitario() != null ? loja.getFlagUnitario().toString() : "");

            cbEtiqueta_Pesado.getSelectionModel().select(loja.getEtiquetaPesado());
            cbEtiqueta_Unitario.getSelectionModel().select(loja.getEtiquetaUnitario());

            if (loja.getDescricaoetiqueta() != null) {
                txtDescricao.setText(loja.getDescricaoetiqueta());
            } else {
                txtDescricao.setText(loja.getDescricao());
            }
        }
        
        Configuracao.carregar();
        ckContinuo.setSelected(Configuracao.isModoContinuo());
        cbIntensidadeImpressao.getSelectionModel().select(Configuracao.getIntensidadeImpressao());
        //* Paulo Gervilla - 1.3.5
        ckLoginAutomatico.setSelected(Configuracao.isLoginAutomatico());
        //*

        if (!Permissoes.verificarAdm()) {

            txtDescricao.setDisable(true);
            
            ckContinuo.setDisable(true);

            cbCodigoBarras_Pesado.setDisable(true);
            cbEtiqueta_Pesado.setDisable(true);
            txtFlag_Pesado.setDisable(true);
            pnVisualiza1.setDisable(true);

            cbCodigoBarras_Unitario.setDisable(true);
            cbEtiqueta_Unitario.setDisable(true);
            txtFlag_Unitario.setDisable(true);
            pnVisualiza2.setDisable(true);
            
            //* Paulo Gervilla - 1.3.5
            ckLoginAutomatico.setDisable(true);
            //*

            btConfirmar.setDisable(true);
        }
    }

    private boolean verificaDados() {
        boolean res = true;

        cbCodigoBarras_Pesado.setEffect(null);
        txtFlag_Pesado.setEffect(null);
        cbCodigoBarras_Unitario.setEffect(null);
        txtFlag_Unitario.setEffect(null);
        cbEtiqueta_Pesado.setEffect(null);
        cbEtiqueta_Unitario.setEffect(null);
        txtDescricao.setEffect(null);

        if (cbCodigoBarras_Pesado.getSelectionModel().getSelectedItem() == null) {
            cbCodigoBarras_Pesado.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (!txtFlag_Pesado.isDisabled()) {
            if (txtFlag_Pesado.getText() == null || txtFlag_Pesado.getText().isEmpty()) {
                txtFlag_Pesado.setEffect(Animacoes.getSombraErro());
                res = false;
            }
        }

        if (cbCodigoBarras_Unitario.getSelectionModel().getSelectedItem() == null) {
            cbCodigoBarras_Unitario.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (!txtFlag_Unitario.isDisabled()) {
            if (txtFlag_Unitario.getText() == null || txtFlag_Unitario.getText().isEmpty()) {
                txtFlag_Unitario.setEffect(Animacoes.getSombraErro());
                res = false;
            }
        }

        if (cbEtiqueta_Pesado.getSelectionModel().getSelectedItem() == null) {
            cbEtiqueta_Pesado.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (cbEtiqueta_Unitario.getSelectionModel().getSelectedItem() == null) {
            cbEtiqueta_Unitario.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (txtDescricao.getText() == null || txtDescricao.getText().isEmpty()) {
            txtDescricao.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        return res;
    }

    @FXML
    void handleVisualizaEtiqueta1(MouseEvent event) {
        FormHelper.getFormMenuController().visualizaImagem(ImageHelper.getImageOf(cbEtiqueta_Pesado.getSelectionModel().getSelectedItem()));
    }

    @FXML
    void handleVisualizaEtiqueta2(MouseEvent event) {
        FormHelper.getFormMenuController().visualizaImagem(ImageHelper.getImageOf(cbEtiqueta_Unitario.getSelectionModel().getSelectedItem()));
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaDados()) {
            try {

                Loja loja = daoLoja.findLoja(SessaoAtual.getLoja().getIndice());

                //Verificando se algum dado padrão foi alterado e caso tenha sido então alterar nos produtos
                //TODO:Adicionar um componente indicador de espera para esse processo
                if (!Objects.equals(loja.getEtiquetaPesado(), cbEtiqueta_Pesado.getSelectionModel().getSelectedItem())
                        || !Objects.equals(loja.getEtiquetaUnitario(), cbEtiqueta_Unitario.getSelectionModel().getSelectedItem())
                        || !Objects.equals(loja.getCodigobarrasPesado(), cbCodigoBarras_Pesado.getSelectionModel().getSelectedItem())
                        || !Objects.equals(loja.getCodigobarrasUnitario(), cbCodigoBarras_Unitario.getSelectionModel().getSelectedItem())
                        || !Objects.equals(loja.getFlagPesado(), !txtFlag_Pesado.isDisable() ? Integer.valueOf(txtFlag_Pesado.getText()) : null)
                        || !Objects.equals(loja.getFlagUnitario(), !txtFlag_Unitario.isDisable() ? Integer.valueOf(txtFlag_Unitario.getText()) : null)) {
                    Dialog.buildConfirmation("Informações padrão alteradas", "Deseja alterar as informações padrão de todos os produtos para os novos valores ?").addYesButton((Event t) -> {
                        if (!Objects.equals(loja.getEtiquetaPesado(), cbEtiqueta_Pesado.getSelectionModel().getSelectedItem())) {
                            daoProduto.updateEtiquetaPesado(cbEtiqueta_Pesado.getSelectionModel().getSelectedItem());
                        }
                        if (!Objects.equals(loja.getEtiquetaUnitario(), cbEtiqueta_Unitario.getSelectionModel().getSelectedItem())) {
                            daoProduto.updateEtiquetaUnitario(cbEtiqueta_Unitario.getSelectionModel().getSelectedItem());
                        }
                        if (!Objects.equals(loja.getCodigobarrasPesado(), cbCodigoBarras_Pesado.getSelectionModel().getSelectedItem())
                                || !Objects.equals(loja.getFlagPesado(), !txtFlag_Pesado.isDisable() ? Integer.valueOf(txtFlag_Pesado.getText()) : null)) {
                            daoProduto.updateCodigoBarra(cbCodigoBarras_Pesado.getSelectionModel().getSelectedItem(), !txtFlag_Pesado.isDisable() ? Integer.valueOf(txtFlag_Pesado.getText()) : null, true);
                        }
                        if (!Objects.equals(loja.getCodigobarrasUnitario(), cbCodigoBarras_Unitario.getSelectionModel().getSelectedItem())
                                || !Objects.equals(loja.getFlagUnitario(), !txtFlag_Unitario.isDisable() ? Integer.valueOf(txtFlag_Unitario.getText()) : null)) {
                            daoProduto.updateCodigoBarra(cbCodigoBarras_Unitario.getSelectionModel().getSelectedItem(), !txtFlag_Unitario.isDisable() ? Integer.valueOf(txtFlag_Unitario.getText()) : null, false);
                        }
                        ImpressoraStatusManager.updateImpressorasOf(ImpressoraStatusEnum.PRODUTO, false);
                        ImpressoraStatusManager.updateImpressorasOf(ImpressoraStatusEnum.LAYOUT, false);
                    }).addNoButton(null).build().showAndWait();
                }

                loja.setCodigobarrasPesado(cbCodigoBarras_Pesado.getSelectionModel().getSelectedItem());
                loja.setFlagPesado(!txtFlag_Pesado.isDisable() ? Integer.valueOf(txtFlag_Pesado.getText()) : null);

                loja.setCodigobarrasUnitario(cbCodigoBarras_Unitario.getSelectionModel().getSelectedItem());
                loja.setFlagUnitario(!txtFlag_Unitario.isDisable() ? Integer.valueOf(txtFlag_Unitario.getText()) : null);

                loja.setEtiquetaPesado(cbEtiqueta_Pesado.getSelectionModel().getSelectedItem());
                loja.setEtiquetaUnitario(cbEtiqueta_Unitario.getSelectionModel().getSelectedItem());
                
                Configuracao.setModoContinuo(ckContinuo.isSelected());
                Configuracao.setIntensidadeImpressao(cbIntensidadeImpressao.getSelectionModel().getSelectedIndex());
                
                //caso tenha alterado o nome da loja nas etiquetas, marcar as balanças como desatualizadas
                if (!loja.getDescricaoetiqueta().equals(txtDescricao.getText())) {
                    ImpressoraStatusManager.updateImpressorasOf(ImpressoraStatusEnum.LAYOUT, false);
                }

                loja.setDescricaoetiqueta(txtDescricao.getText());
                
                //* Paulo Gervilla - 1.3.5
                if (!ckLoginAutomatico.isSelected() && Configuracao.isLoginAutomatico()) {
                    Dialog.showWarning("Atenção", "O login automático será desabilitado após a reinicialização do programa.");
                }
                Configuracao.setLoginautomatico(ckLoginAutomatico.isSelected());
                Configuracao.salvar();

                //*
                
                daoLoja.edit(loja);
                Configuracao.salvar();
                
                SessaoAtual.setLoja(loja);
            } catch (Exception ex) {
                Logger.getLogger(FrmConfigLojaController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FormHelper.getFormMenuController().desempilhaFormulario();
        }
    }

}
