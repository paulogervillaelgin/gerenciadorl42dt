/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.comunicacao.ComandoLeitura;
import br.com.elgin.gerenciadorl42dt.comunicacao.Comunicacao;
import br.com.elgin.gerenciadorl42dt.comunicacao.TaskComunicacao;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmPesagemUnitariosController implements Initializable {

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private Label lbImpressoraDescricao;

    @FXML
    private Label lbImpressoraDepartamento;

    @FXML
    private Button btAtualizar;

    @FXML
    private TableView<Produto> tbProduto;

    @FXML
    private TableColumn<Produto, Integer> colCodigo;

    @FXML
    private TableColumn<Produto, String> colDescricao;

    @FXML
    private TableColumn<Produto, BigDecimal> colPreco;

    @FXML
    private TextField txtQuantidade;

    @FXML
    private TextField txtCopias;

    @FXML
    private Button btTransmitir;

    @FXML
    private Button btCancelar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ProdutoJpaController daoProduto = new ProdutoJpaController(emf);
    ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);

    private final ObservableList<Produto> obsProdutos = FXCollections.observableArrayList();
    private final ObservableList<Produto> obsFiltroProdutos = FXCollections.observableArrayList();

    private Impressora impressora;

    public Impressora getImpressora() {
        return impressora;
    }

    public void setImpressora(Impressora impressora) {
        this.impressora = impressora;
        if (impressora != null) {
            lbImpressoraDepartamento.setText("Departamento: " + impressora.getDepartamento().getDescricao());
            lbImpressoraDescricao.setText(impressora.getDescricao());
            if (!impressora.isAtualizada()) {
                Dialog.showError("Atenção", "Antes de prosseguir com a impressão, atualize a impressora.");
                FormHelper.carregaFormulario(FormHelper.FORM_TRANSMISSAO);
                return;
            }
            btTransmitir.setDisable(false);
            txtPesquisa.setDisable(false);
            txtQuantidade.setDisable(false);
            tbProduto.setDisable(false);
            updateTabela();
            Platform.runLater(() -> {
                txtPesquisa.requestFocus();
            });
        } else {
            lbImpressoraDepartamento.setText("");
            lbImpressoraDescricao.setText("Impressora não cadastrada");
            btTransmitir.setDisable(true);
            txtPesquisa.setDisable(true);
            txtQuantidade.setDisable(true);
            tbProduto.setDisable(true);
            txtPesquisa.clear();
            txtQuantidade.clear();
            obsProdutos.clear();
            obsFiltroProdutos.clear();
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colCodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        colPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));

        colPreco.setCellFactory((TableColumn<Produto, BigDecimal> p) -> {
            TableCell<Produto, BigDecimal> cell = new TableCell<Produto, BigDecimal>() {
                @Override
                public void updateItem(BigDecimal item, boolean empty) {
                    setAlignment(Pos.CENTER_RIGHT);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else if (item != null) {

                        setText(Utilidades.formatMoeda(item));
                    }
                }
            };
            //System.out.println(cell.getIndex());
            return cell;
        });

        txtPesquisa.setOnKeyReleased(
                (event) -> {
                    //if (txtPesquisa.getText().isEmpty() && event.getCode().equals(KeyCode.ENTER)) {
                    //showProdutos();
                    //   updateFiltroTabela();
                    //} else  else {
                    switch (event.getCode()) {
                        case UP:
                            if (event.isShiftDown()) {
                                tbProduto.getSelectionModel().selectPrevious();
                            } else if (tbProduto.getSelectionModel().getSelectedIndex() > 0) {
                                tbProduto.getSelectionModel().clearAndSelect(tbProduto.getSelectionModel().getSelectedIndex() - 1);

                            }
                            event.consume();
                            break;
                        case DOWN:
                            if (event.isShiftDown()) {
                                tbProduto.getSelectionModel().selectNext();
                            } else {
                                tbProduto.getSelectionModel().clearAndSelect(tbProduto.getSelectionModel().getSelectedIndex() + 1);
                            }
                            event.consume();
                            break;
                        case ENTER:
                            tbProduto.requestFocus();
                            event.consume();
                            break;
                        default:
                            updateFiltroTabela();
                            break;
                    }

                }
        );

        pnCarregando.setVisible(false);
        pnCarregando.toBack();
        btTransmitir.setDisable(true);
        txtPesquisa.setDisable(true);
        txtQuantidade.setDisable(true);
        tbProduto.setDisable(true);

        getImpressoraConectada();
    }

    private void updateFiltroTabela() {
        obsFiltroProdutos.clear();

        for (Produto prod : obsProdutos) {
            if (matchesFilter(prod)) {
                obsFiltroProdutos.add(prod);
            }
        }
        colCodigo.setVisible(false);
        colCodigo.setVisible(true);

        tbProduto.setItems(obsFiltroProdutos);

    }

    private boolean matchesFilter(Produto produto) {
        boolean aux = false;

        String filterString = txtPesquisa.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }

        String lowerCaseFilterString = filterString.toLowerCase();

        if (produto.getDescricao() != null && produto.getDescricao().toLowerCase().contains(lowerCaseFilterString)) {
            aux = true;
        } else if (String.valueOf(produto.getCodigo()).contains(lowerCaseFilterString)) {
            aux = true;
        } else {
            aux = false;
        }

        return aux; // Does not match

    }

    private class TaskBuscaDados extends Task<List<Produto>> {

        @Override
        protected List<Produto> call() throws Exception {
            return daoProduto.findProdutoUnitariosEntitiesByDepartamento(impressora.getDepartamento());
        }

    }

    private void updateTabela() {
        TaskBuscaDados task = new TaskBuscaDados();
        pnCarregando.setVisible(true);
        pnCarregando.getStyleClass().add("frame_transparente_2");
        piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        lbCarregandoStatus.setText("Carregando produtos...");
        task.setOnSucceeded((WorkerStateEvent event) -> {
            try {
                colDescricao.setVisible(false);
                colDescricao.setVisible(true);

                List results = task.getValue();
                //if (obsProdutos == null) {
                //    obsProdutos = FXCollections.observableArrayList(results);
                //} else {
                obsProdutos.clear();
                obsProdutos.addAll(results);
                //}

                tbProduto.setItems(obsProdutos);

                if ((txtPesquisa.getText() != null && !txtPesquisa.getText().isEmpty())) {
                    updateFiltroTabela();
                }

            } finally {
                pnCarregando.setVisible(false);
                pnCarregando.getStyleClass().removeAll("frame_transparente_2");
            }
        }
        );
        new Thread(task).start();
        //databaseExecutor.submit(task);
    }

    private void getImpressoraConectada() {
        pnCarregando.toFront();
        pnCarregando.setVisible(true);
        TaskComunicacao task = Comunicacao.getTaskSerial();
        lbCarregandoStatus.textProperty().bind(task.messageProperty());
        piCarregando.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded((event) -> {
            pnCarregando.toBack();
            pnCarregando.setVisible(false);
            piCarregando.progressProperty().unbind();
            lbCarregandoStatus.textProperty().unbind();
            if (task.getValue()) {
                String serial = new String(((ComandoLeitura) task.getListaComandos().get(0)).getBufferLeitura()).replace("\r\n", "").replaceAll("\0+$", "");
                setImpressora(daoImpressora.findImpressoraBySerial(serial));
            } else {
                lbImpressoraDescricao.setText("Impressora não conectada.");
                lbImpressoraDepartamento.setText("");
                btTransmitir.setDisable(true);
                txtPesquisa.setDisable(true);
                txtQuantidade.setDisable(true);
                tbProduto.setDisable(true);
                txtPesquisa.clear();
                txtQuantidade.clear();
                obsProdutos.clear();
                obsFiltroProdutos.clear();
            }
        });
        Comunicacao.EXECUTOR.submit(task);
    }

    @FXML
    void handleBtAtualizar(ActionEvent event) {
        getImpressoraConectada();
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();

    }

    //task que codifica os dados e gera uma lista de comandos a serem executados
    private class TaskGeraComandos extends Task<TaskComunicacao> {

        @Override
        protected TaskComunicacao call() throws Exception {
            return Comunicacao.getTaskPesagemUnitaria(tbProduto.getSelectionModel().getSelectedItem(), Integer.parseInt(txtQuantidade.getText()), Integer.parseInt(txtCopias.getText()));
        }

    }

    @FXML
    void handleBtTransmitir(ActionEvent event) {
        if (getImpressora() != null) {

            if (tbProduto.getSelectionModel().getSelectedItem() != null) {
                if (txtQuantidade.getText() != null && !txtQuantidade.getText().isEmpty()) {

                    try {
                        if (Integer.valueOf(txtQuantidade.getText()) <= 0) {
                            throw new NumberFormatException();
                        }
                    } catch (NumberFormatException ex) {
                        Dialog.showError("Erro", "Forneça um valor inteiro para a quantidade de produtos.");
                        Platform.runLater(() -> {
                            txtQuantidade.requestFocus();
                        });
                        return;
                    }

                    try {
                        if (Integer.valueOf(txtCopias.getText()) <= 0) {
                            throw new NumberFormatException();
                        }
                    } catch (NumberFormatException ex) {
                        Dialog.showError("Erro", "Forneça um valor inteiro para a quantidade de cópias.");
                        Platform.runLater(() -> {
                            txtCopias.requestFocus();
                        });
                        return;
                    }

                    Dialog.buildConfirmation("Impressão", "Deseja realmente efetuar essa impressão de unitários ?").addYesButton((evento_yes) -> {

                        pnCarregando.setVisible(true);
                        pnCarregando.toFront();
                        lbCarregandoStatus.setText("Preparando dados...");
                        piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
                        TaskGeraComandos task = new TaskGeraComandos();
                        piCarregando.progressProperty().bind(task.progressProperty());
                        task.setOnSucceeded((evento) -> {
                            if (task.getValue() != null) {

                                TaskComunicacao taskComunicacao = task.getValue();

                                try {
                                    FXMLLoader fxmlLoader = new FXMLLoader();
                                    fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_STATUS_TASK));
                                    Parent root = (Parent) fxmlLoader.load();
                                    FrmStatusTaskController controller = (FrmStatusTaskController) fxmlLoader.getController();

                                    controller.setTitulo("Pesagem");
                                    controller.setTaskExecutando(taskComunicacao);

                                    controller.setOnClose((evento_controller) -> {
                                        pnCarregando.setVisible(false);
                                        pnCarregando.toBack();
                                        piCarregando.progressProperty().unbind();

                                        getImpressoraConectada();
                                    });

                                    Comunicacao.EXECUTOR.submit(taskComunicacao);

                                    FormHelper.getFormMenuController().empilhaFormulario(root);
                                } catch (IOException ex) {
                                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        });
                        new Thread(task).start();
                    }).addNoButton(null).build().show();
                } else {
                    Dialog.showError("Erro", "Forneça a quantidade da impressão.");
                    Platform.runLater(() -> {
                        txtQuantidade.requestFocus();
                    });
                }
            } else {
                Dialog.showError("Erro", "Selecione um produto na listagem antes de continuar.");
                Platform.runLater(() -> {
                    tbProduto.requestFocus();
                });

            }
        }
    }

}
