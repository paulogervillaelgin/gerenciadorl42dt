/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.FlexibarcodeJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.FormatoEtiquetaJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.model.FormatoEtiqueta;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.model.Loja;
import br.com.elgin.gerenciadorl42dt.model.Produto;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import br.com.elgin.gerenciadorl42dt.ui.ImageHelper;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

/**
 * FXML Controller class
 *
 * @author Paulo
 */
public class FrmCadastroProdutoController implements Initializable, Formulario {

    @FXML
    private AnchorPane pnGeral;

    @FXML
    private TextField txtCodigo;

    @FXML
    private RadioButton rbPesado;

    @FXML
    private ToggleGroup tgTipo;

    @FXML
    private RadioButton rbUnitario;

    @FXML
    private Label lbJaExiste;

    @FXML
    private TextField txtDescricao;

    @FXML
    private TextField txtDescricao2;

    @FXML
    private TextField txtDescricao3;

    @FXML
    private TextField txtPreco;

    @FXML
    private Label lbTara;

    @FXML
    private TextField txtTara;

    @FXML
    private Label lbQuantidade;

    @FXML
    private TextField txtQuantidade;

    @FXML
    private ComboBox<Grupoproduto> cbGrupoProduto;

    @FXML
    private ComboBox<FormatoEtiqueta> cbEtiqueta1;

    @FXML
    private TextField txtValidadeDias;

    @FXML
    private CheckBox ckImpValidade;

    @FXML
    private ImageView imgEtiqueta1;

    @FXML
    private TextField txtPorcao;

    @FXML
    private TextField txtValorEnergetico;

    @FXML
    private TextField txtCarboidratos;

    @FXML
    private TextField txtProteinas;

    @FXML
    private TextField txtGordurasTotais;

    @FXML
    private TextField txtGordurasSaturadas;

    @FXML
    private TextField txtGordurasTrans;

    @FXML
    private TextField txtFibraAlimentar;

    @FXML
    private TextField txtSodio;

    @FXML
    private Label lbValorEnergetico;

    @FXML
    private Label lbCarboidratos;

    @FXML
    private Label lbProteinas;

    @FXML
    private Label lbGordurasTotais;

    @FXML
    private Label lbGordurasSaturadas;

    @FXML
    private Label lbGordurasTrans;

    @FXML
    private Label lbFibraAlimentar;

    @FXML
    private Label lbSodio;

    @FXML
    private TextArea txtIngredientes;

    @FXML
    private TextArea txtMsgEspecial;

    @FXML
    private TextField txtItemCodigo;

    @FXML
    private TextField txtFlag1;

    @FXML
    private ComboBox<Flexibarcode> cbCodigoBarras;

    @FXML
    private TabPane pnTabs;

    @FXML
    private Tab tabItem;

    @FXML
    private Tab tabEtiqueta;

    @FXML
    private Tab tabNutricao;

    @FXML
    private Tab tabOutros;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private AnchorPane pnItem;

    @FXML
    private AnchorPane pnEtiqueta;

    @FXML
    private AnchorPane pnNutricao;

    @FXML
    private AnchorPane pnOutros;

    private Produto produto;
    private Grupoproduto grupoProduto;

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ProdutoJpaController daoProduto = new ProdutoJpaController(emf);
    GrupoprodutoJpaController daoGrupo = new GrupoprodutoJpaController(emf);
    FormatoEtiquetaJpaController daoEtiqueta = new FormatoEtiquetaJpaController(emf);
    FlexibarcodeJpaController daoBarcode = new FlexibarcodeJpaController(emf);

    //private final ObservableList<Grupoproduto> obsGrupos = FXCollections.observableArrayList();
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
        if (produto != null) {
            //Bloqueia os dados do DIVERSOS
            if (produto.getCodigo() == 0) {
                txtCodigo.setDisable(true);
            }
            exibeDados(produto);
        }
    }

    public Grupoproduto getGrupoProduto() {
        return grupoProduto;
    }

    public void setGrupoProduto(Grupoproduto grupoProduto) {
        this.grupoProduto = grupoProduto;
        //somente altera grupo quando estiver cadastrando 
        if (getProduto() == null) {
            if (cbGrupoProduto.getItems().contains(grupoProduto)) {
                cbGrupoProduto.getSelectionModel().select(grupoProduto);
            }
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // combo Grupo de Produtos
        cbGrupoProduto.setItems(FXCollections.observableArrayList(daoGrupo.findGrupoprodutoEntities()));
        cbGrupoProduto.setCellFactory((ListView<Grupoproduto> param) -> {
            final ListCell<Grupoproduto> cell = new ListCell<Grupoproduto>() {
                @Override
                public void updateItem(Grupoproduto item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText(null);
                    }
                }
            };
            return cell;
        });
        cbGrupoProduto.setConverter(new StringConverter<Grupoproduto>() {
            @Override
            public String toString(Grupoproduto object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public Grupoproduto fromString(String string) {
                return null;
            }
        });
        ////////////////////////////////////////////////////////////////////////
        // combo FormatoEtiqueta 1   
        List<FormatoEtiqueta> listaEtiquetas = daoEtiqueta.findFormatoEtiquetaEntities();

        cbEtiqueta1.setItems(FXCollections.observableArrayList(listaEtiquetas));
        cbEtiqueta1.setCellFactory((ListView<FormatoEtiqueta> param) -> {
            final ListCell<FormatoEtiqueta> cell = new ListCell<FormatoEtiqueta>() {
                @Override
                public void updateItem(FormatoEtiqueta item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getDescricao());
                    } else {
                        setText(null);
                    }
                }
            };
            return cell;
        });
        cbEtiqueta1.setConverter(new StringConverter<FormatoEtiqueta>() {
            @Override
            public String toString(FormatoEtiqueta object) {
                if (object != null) {
                    return object.getDescricao();
                } else {
                    return null;
                }
            }

            @Override
            public FormatoEtiqueta fromString(String string) {
                return null;
            }
        });
        cbEtiqueta1.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            imgEtiqueta1.setImage(ImageHelper.getImageOf(newValue));
        });
        ///////////

        List<Flexibarcode> listaBarcodes = daoBarcode.findFlexibarcodeEntities();

        cbCodigoBarras.setItems(FXCollections.observableArrayList(listaBarcodes));
        cbCodigoBarras.setCellFactory((ListView<Flexibarcode> param) -> {
            final ListCell<Flexibarcode> cell = new ListCell<Flexibarcode>() {
                @Override
                public void updateItem(Flexibarcode item,
                        boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setContentDisplay(ContentDisplay.RIGHT);
                        setText(item.getDescricao());
                        setGraphic(Animacoes.getCodigoFlexbarcode(item, 16d));
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
            return cell;
        });

        cbCodigoBarras.setConverter(new StringConverter<Flexibarcode>() {
            @Override
            public String toString(Flexibarcode object) {
                if (object != null) {
                    return object.getDescricao() + " ( " + Flexibarcode.TIPOS.get(object.getTipo()) + " - " + object.getStringFormato() + " )";
                } else {
                    return null;
                }
            }

            @Override
            public Flexibarcode fromString(String string) {
                return null;
            }
        });

        txtFlag1.setOnKeyReleased((event) -> {
            if (txtFlag1.getText().length() > 1) {
                txtFlag1.setText(txtFlag1.getText().substring(txtFlag1.getText().length() - 1));
                txtFlag1.positionCaret(txtFlag1.getText().length());
            }
            try {
                Integer.valueOf(txtFlag1.getText());
            } catch (NumberFormatException e) {
                txtFlag1.setText("");
            }
        });
        ////////////////////////////////////////////////////////////////////////

        lbJaExiste.setVisible(false);

        txtCodigo.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                lbJaExiste.setVisible(false);
                txtCodigo.setEffect(null);
                try {
                    Integer.valueOf(txtCodigo.getText());
                    if (getProduto() == null || (getProduto() != null && getProduto().getCodigo() != Integer.valueOf(txtCodigo.getText()))) {
                        TaskVerificaCodigo task = new TaskVerificaCodigo(Integer.valueOf(txtCodigo.getText()));
                        task.setOnSucceeded((event) -> {
                            if (task.getValue()) {
                                lbJaExiste.setVisible(true);
                                txtCodigo.setEffect(Animacoes.getSombraErro());
                            } else {
                                if (txtItemCodigo.getText().isEmpty()) {
                                    txtItemCodigo.setText(txtCodigo.getText());
                                }
                            }
                        });
                        new Thread(task).start();
                    }
                } catch (NumberFormatException e) {
                    txtCodigo.setEffect(Animacoes.getSombraErro());
                }

            }
        });

        txtPreco.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtPreco.setText(String.valueOf(Utilidades.parseAsMoeda(txtPreco.getText())));
                txtPreco.selectAll();
            } else {
                txtPreco.setText(Utilidades.formatMoeda(Utilidades.parseAsMoeda(txtPreco.getText())));
            }
        });

        txtTara.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtTara.setText(String.valueOf(Utilidades.parseAsPeso(txtTara.getText())));
                txtTara.selectAll();
            } else {
                txtTara.setText(Utilidades.formatPeso(Utilidades.parseAsPeso(txtTara.getText())));
            }
        });

        txtQuantidade.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                try {
                    txtQuantidade.setText(String.valueOf(Integer.valueOf(txtQuantidade.getText())));
                } catch (NumberFormatException ex) {
                    txtQuantidade.setText("0");
                }
            }
        });

        //////////////////
        tgTipo.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            // Comentado pois a tara e a qtd não estão sendo enviados para a impressora/balança
            /*
            lbTara.setVisible(newValue == rbPesado);
            txtTara.setVisible(newValue == rbPesado);
            lbQuantidade.setVisible(newValue == rbUnitario);
            txtQuantidade.setVisible(newValue == rbUnitario);
             */
            Loja loja = SessaoAtual.getLoja();
            //valores padrão para campos
            if (newValue == rbPesado) {
                cbEtiqueta1.setItems(FXCollections.observableArrayList(daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOPESADO)));
                cbEtiqueta1.getSelectionModel().select(loja.getEtiquetaPesado());
                cbCodigoBarras.getSelectionModel().select(loja.getCodigobarrasPesado());
                txtFlag1.setText(loja.getFlagPesado() != null ? String.valueOf(loja.getFlagPesado()) : "");
            } else {
                cbEtiqueta1.setItems(FXCollections.observableArrayList(daoEtiqueta.findFormatoEtiquetaByTipo(FormatoEtiqueta.TIPOUNIDADE)));
                cbEtiqueta1.getSelectionModel().select(loja.getEtiquetaUnitario());
                cbCodigoBarras.getSelectionModel().select(loja.getCodigobarrasUnitario());
                txtFlag1.setText(loja.getFlagUnitario() != null ? String.valueOf(loja.getFlagUnitario()) : "");
            }

        });
        //força uma atualização :
        rbUnitario.setSelected(true);
        rbPesado.setSelected(true);

        /////////////////////
        txtValorEnergetico.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtValorEnergetico.setText(String.valueOf(Utilidades.parseAsIntegerWithUnit(txtValorEnergetico.getText(), "kcal")));
                txtValorEnergetico.selectAll();
            } else {
                txtValorEnergetico.setText(String.valueOf(Utilidades.parseAsIntegerWithUnit(txtValorEnergetico.getText(), "kcal").toString()) + " kcal");
                // trata o limite minimo 4 kcal
                if (Utilidades.parseAsIntegerWithUnit(txtValorEnergetico.getText(), "kcal") <= 4) {
                    txtValorEnergetico.setText("0 kcal");
                }
                calcVD();
            }
        });

        txtCarboidratos.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtCarboidratos.setText(String.valueOf(Utilidades.parseAsNutri(txtCarboidratos.getText(), "g")));
                txtCarboidratos.selectAll();
            } else {
                txtCarboidratos.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtCarboidratos.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtCarboidratos.getText(), "g").doubleValue() <= 0.5d) {
                    txtCarboidratos.setText("0 g");
                }
                calcVD();
            }
        });

        txtProteinas.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtProteinas.setText(String.valueOf(Utilidades.parseAsNutri(txtProteinas.getText(), "g")));
                txtProteinas.selectAll();
            } else {
                txtProteinas.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtProteinas.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtProteinas.getText(), "g").doubleValue() <= 0.5d) {
                    txtProteinas.setText("0 g");
                }
                calcVD();
            }
        });

        txtGordurasTotais.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtGordurasTotais.setText(String.valueOf(Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g")));
                txtGordurasTotais.selectAll();
            } else {
                txtGordurasTotais.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g").doubleValue() <= 0.5d) {
                    txtGordurasTotais.setText("0 g");
                }
                calcVD();
            }
        });

        txtGordurasSaturadas.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtGordurasSaturadas.setText(String.valueOf(Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g")));
                txtGordurasSaturadas.selectAll();
            } else {
                txtGordurasSaturadas.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g").doubleValue() <= 0.2d) {
                    txtGordurasSaturadas.setText("0 g");
                }
                calcVD();
            }
        });

        txtGordurasTrans.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtGordurasTrans.setText(String.valueOf(Utilidades.parseAsNutri(txtGordurasTrans.getText(), "g")));
                txtGordurasTrans.selectAll();
            } else {
                txtGordurasTrans.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtGordurasTrans.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtGordurasTrans.getText(), "g").doubleValue() <= 0.2d) {
                    txtGordurasTrans.setText("0 g");
                }
                calcVD();
            }
        });

        txtFibraAlimentar.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtFibraAlimentar.setText(String.valueOf(Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g")));
                txtFibraAlimentar.selectAll();
            } else {
                txtFibraAlimentar.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g"), "g"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g").doubleValue() <= 0.5d) {
                    txtFibraAlimentar.setText("0 g");
                }
                calcVD();
            }
        });

        txtSodio.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                txtSodio.setText(String.valueOf(Utilidades.parseAsNutri(txtSodio.getText(), "mg")));
                txtSodio.selectAll();
            } else {
                txtSodio.setText(Utilidades.formatNutri(Utilidades.parseAsNutri(txtSodio.getText(), "mg"), "mg"));
                //trata o limite minimo
                if (Utilidades.parseAsNutri(txtSodio.getText(), "mg").doubleValue() <= 5d) {
                    txtSodio.setText("0 mg");
                }
                calcVD();
            }
        });

        txtPorcao.textProperty().addListener((observable, oldValue, newValue) -> {
            if (txtPorcao.getText() != null && txtPorcao.getText().length() > 20) {
                String s = txtPorcao.getText().substring(0, 20);
                txtPorcao.setText(s);
            }
        });

        txtValidadeDias.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                try {
                    txtValidadeDias.setText(Integer.valueOf(txtValidadeDias.getText()).toString());
                } catch (NumberFormatException ex) {
                    txtValidadeDias.setText("0");
                }
            }
        });

        //Forçar uma atualização
        tgTipo.selectToggle(rbUnitario);
        tgTipo.selectToggle(rbPesado);

        txtValidadeDias.setText("0");
        ckImpValidade.setSelected(true);
        
        verificarPermissoes();

        Platform.runLater(() -> {
            txtCodigo.requestFocus();
        });

    }

    private void verificarPermissoes() {
        if (!Permissoes.verificarAdm()) {
            btConfirmar.setDisable(true);
            pnItem.setDisable(true);
            pnEtiqueta.setDisable(true);
            pnNutricao.setDisable(true);
            pnOutros.setDisable(true);
        }
    }

    //Calcula os Valores diarios 
    private void calcVD() {
        lbValorEnergetico.setText(String.valueOf(Math.round((Utilidades.parseAsIntegerWithUnit(txtValorEnergetico.getText(), "kcal") / 2000d) * 100)) + "%");
        lbCarboidratos.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtCarboidratos.getText(), "g").floatValue() / 300d) * 100)) + "%");
        lbProteinas.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtProteinas.getText(), "g").floatValue() / 75d) * 100)) + "%");
        lbGordurasTotais.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g").floatValue() / 55d) * 100)) + "%");
        lbGordurasSaturadas.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g").floatValue() / 22d) * 100)) + "%");
        lbFibraAlimentar.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g").floatValue() / 25d) * 100)) + "%");
        lbSodio.setText(String.valueOf(Math.round((Utilidades.parseAsNutri(txtSodio.getText(), "mg").floatValue() / 2400d) * 100)) + "%");
    }

    private void exibeDados(Produto prod) {
        if (prod != null) {
            txtCodigo.setText(String.valueOf(prod.getCodigo()));
            tgTipo.selectToggle(prod.isPesado() ? rbPesado : rbUnitario);
            txtDescricao.setText(prod.getDescricao());
            txtDescricao2.setText(prod.getDescricao2());
            txtPreco.setText(Utilidades.formatMoeda(prod.getPreco()));
            txtTara.setText(Utilidades.formatPeso(prod.getTara()));
            cbGrupoProduto.getSelectionModel().select(prod.getGrupoproduto());
            cbEtiqueta1.getSelectionModel().select(prod.getEtiqueta());
            txtPorcao.setText(prod.getPorcao());
            txtValorEnergetico.setText(String.valueOf(prod.getCalorias() == null ? 0 : prod.getCalorias()) + " kcal");
            txtCarboidratos.setText(Utilidades.formatNutri(prod.getCarboidratos(), "g"));
            txtProteinas.setText(Utilidades.formatNutri(prod.getProteinas(), "g"));
            txtGordurasTotais.setText(Utilidades.formatNutri(prod.getGordurastotais(), "g"));
            txtGordurasSaturadas.setText(Utilidades.formatNutri(prod.getGordurassaturadas(), "g"));
            txtGordurasTrans.setText(Utilidades.formatNutri(prod.getGordurastrans(), "g"));
            txtFibraAlimentar.setText(Utilidades.formatNutri(prod.getFibraalimentar(), "g"));
            txtSodio.setText(Utilidades.formatNutri(prod.getSodio(), "mg"));
            txtValidadeDias.setText(String.valueOf(prod.getValidadeDias()));
            txtIngredientes.setText(prod.getIngredientes());
            txtMsgEspecial.setText(prod.getMsgEspecial());
            cbCodigoBarras.getSelectionModel().select(prod.getCodigoBarras());
            txtFlag1.setText(prod.getFlag() != null ? prod.getFlag().toString() : "");
            //txtFlag2.setText(prod.getFlag2() != null ? prod.getFlag2().toString() : "");
            txtItemCodigo.setText(prod.getItemCodigo() != null ? String.valueOf(prod.getItemCodigo()) : "");
            ckImpValidade.setSelected(prod.isImprimirValidade());
            calcVD();
        }
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    //Task para verificar se o codigo já foi cadastrado
    private class TaskVerificaCodigo extends Task<Boolean> {

        private Integer codigo;

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        private TaskVerificaCodigo(Integer cod) {
            codigo = cod;
        }

        @Override
        protected Boolean call() throws Exception {
            try {
                return daoProduto.findProdutoByCodigo(getCodigo()) != null;
            } catch (NoResultException e) {
                return false;
            }
        }
    }

    private boolean verificaCampos() {
        boolean res = true;

        txtDescricao.setEffect(null);
        txtCodigo.setEffect(null);
        txtPreco.setEffect(null);
        txtTara.setEffect(null);
        txtQuantidade.setEffect(null);
        cbGrupoProduto.setEffect(null);

        txtFlag1.setEffect(null);
        cbCodigoBarras.setEffect(null);
        txtItemCodigo.setEffect(null);

        cbEtiqueta1.setEffect(null);

        txtPorcao.setEffect(null);
        txtValorEnergetico.setEffect(null);
        txtCarboidratos.setEffect(null);
        txtProteinas.setEffect(null);
        txtGordurasTotais.setEffect(null);
        txtGordurasSaturadas.setEffect(null);
        txtGordurasTrans.setEffect(null);
        txtFibraAlimentar.setEffect(null);
        txtSodio.setEffect(null);

        if (txtCodigo.getText().isEmpty() || lbJaExiste.isVisible()) {
            txtCodigo.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }
        try {
            Integer.valueOf(txtCodigo.getText());
        } catch (Exception e) {
            txtCodigo.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (txtDescricao.getText() == null || txtDescricao.getText().isEmpty()) {
            txtDescricao.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (cbGrupoProduto.getSelectionModel().getSelectedItem() == null) {
            cbGrupoProduto.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (txtPreco.getText() == null || txtPreco.getText().isEmpty()) {
            txtPreco.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (txtTara.isVisible() && (txtTara.getText() == null || txtTara.getText().isEmpty())) {
            txtTara.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (txtQuantidade.isVisible() && (txtQuantidade.getText() == null || txtQuantidade.getText().isEmpty())) {
            txtQuantidade.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        if (cbEtiqueta1.getSelectionModel().getSelectedItem() == null) {
            cbEtiqueta1.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabEtiqueta);
            res = false;
        }

        if (cbCodigoBarras.getSelectionModel().getSelectedItem() == null) {
            cbCodigoBarras.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabEtiqueta);
            res = false;
        }

        if (!txtFlag1.isDisabled()) {
            if (txtFlag1.getText() == null || txtFlag1.getText().isEmpty()) {
                txtFlag1.setEffect(Animacoes.getSombraErro());
                pnTabs.getSelectionModel().select(tabEtiqueta);
                res = false;
            }
            try {
                Integer.valueOf(txtFlag1.getText());
            } catch (Exception e) {
                txtFlag1.setEffect(Animacoes.getSombraErro());
                pnTabs.getSelectionModel().select(tabEtiqueta);
                res = false;
            }

        }

        if (txtItemCodigo.getText() == null || txtItemCodigo.getText().isEmpty()) {
            txtItemCodigo.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabEtiqueta);
            res = false;
        }

        try {
            Integer.valueOf(txtItemCodigo.getText());
            if ((long) Math.log10(Integer.valueOf(txtItemCodigo.getText())) + 1 > cbCodigoBarras.getSelectionModel().getSelectedItem().getTamanhocodigo()) {
                Dialog.showError("Erro", "Comprimento do Código Item excede o configurado no codigo de barras.");
                txtItemCodigo.setEffect(Animacoes.getSombraErro());
                pnTabs.getSelectionModel().select(tabEtiqueta);
                res = false;
            }
        } catch (NumberFormatException e) {
            txtItemCodigo.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabEtiqueta);
            res = false;
        }

        //não verifica mais para permitir nulo
        /*
        if (cbEtiqueta2.getSelectionModel().getSelectedItem() == null) {
            cbEtiqueta2.setEffect(Animacoes.getSombraErro());
            res = false;
        }
         */
        if (txtTara.isVisible()) {
            //verifica se foi fornecido valor valido
            try {
                Utilidades.parseAsPeso(txtTara.getText());
            } catch (Exception e) {
                txtTara.setEffect(Animacoes.getSombraErro());
                pnTabs.getSelectionModel().select(tabItem);
                res = false;
            }
        }

        try {
            Utilidades.parseAsMoeda(txtPreco.getText());
        } catch (Exception e) {
            txtPreco.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        try {
            Integer.valueOf(txtValidadeDias.getText());
        } catch (Exception e) {
            txtValidadeDias.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabItem);
            res = false;
        }

        try {
            Utilidades.parseAsIntegerWithUnit(txtValorEnergetico.getText(), "kcal");
        } catch (Exception e) {
            txtValorEnergetico.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtCarboidratos.getText(), "g");
        } catch (Exception e) {
            txtCarboidratos.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtProteinas.getText(), "g");
        } catch (Exception e) {
            txtProteinas.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g");
        } catch (Exception e) {
            txtGordurasTotais.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g");
        } catch (Exception e) {
            txtGordurasSaturadas.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtGordurasTrans.getText(), "g");
        } catch (Exception e) {
            txtGordurasTrans.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g");
        } catch (Exception e) {
            txtFibraAlimentar.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        try {
            Utilidades.parseAsNutri(txtSodio.getText(), "mg");
        } catch (Exception e) {
            txtSodio.setEffect(Animacoes.getSombraErro());
            pnTabs.getSelectionModel().select(tabNutricao);
            res = false;
        }

        return res;
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        setProduto(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
        //stage.close();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {

        if (verificaCampos()) {
            Produto prod = new Produto();
            if (getProduto() != null) {
                prod.setIndice(getProduto().getIndice());
            }
            prod.setCodigo(Integer.valueOf(txtCodigo.getText()));
            prod.setPesado(tgTipo.getSelectedToggle() == rbPesado);
            prod.setDescricao(txtDescricao.getText().trim());
            prod.setDescricao2(txtDescricao2.getText() != null ? txtDescricao2.getText().trim() : "");
            prod.setGrupoproduto(cbGrupoProduto.getSelectionModel().getSelectedItem());
            prod.setEtiqueta(cbEtiqueta1.getSelectionModel().getSelectedItem());
            prod.setTara(txtTara.isVisible() ? Utilidades.parseAsPeso(txtTara.getText()) : BigDecimal.ZERO);
            prod.setPreco(Utilidades.parseAsMoeda(txtPreco.getText()));
            prod.setPorcao(txtPorcao.getText());
            prod.setCalorias(Integer.valueOf(txtValorEnergetico.getText().replaceAll("kcal", "").trim()));
            prod.setCarboidratos(Utilidades.parseAsNutri(txtCarboidratos.getText(), "g"));
            prod.setProteinas(Utilidades.parseAsNutri(txtProteinas.getText(), "g"));
            prod.setGordurastotais(Utilidades.parseAsNutri(txtGordurasTotais.getText(), "g"));
            prod.setGordurassaturadas(Utilidades.parseAsNutri(txtGordurasSaturadas.getText(), "g"));
            prod.setGordurastrans(Utilidades.parseAsNutri(txtGordurasTrans.getText(), "g"));
            prod.setFibraalimentar(Utilidades.parseAsNutri(txtFibraAlimentar.getText(), "g"));
            prod.setSodio(Utilidades.parseAsNutri(txtSodio.getText(), "mg"));
            prod.setValidadeDias(Integer.valueOf(txtValidadeDias.getText()));
            prod.setIngredientes(txtIngredientes.getText());
            prod.setMsgEspecial(txtMsgEspecial.getText());
            prod.setItemCodigo(Integer.valueOf(txtItemCodigo.getText()));
            prod.setCodigoBarras(cbCodigoBarras.getSelectionModel().getSelectedItem());
            prod.setFlag(!txtFlag1.isDisable() ? Integer.valueOf(txtFlag1.getText()) : null);
            prod.setImprimirValidade(ckImpValidade.isSelected());
            setProduto(prod);

            //para testar ageração do comando de produto
            /*
            Cronometro crono = new Cronometro();
            crono.start();
            GeradorComandoProduto.getInstance().getComandoEnviar(getProduto());
            crono.stop();
            System.out.println(crono.getTime());
             */
            //GeradorComandoProduto.getInstance().getComandoEnviar(getProduto(), new Balanca());
            FormHelper.getFormMenuController().desempilhaFormulario();
            onClose();
        }
        //stage.close();
    }

}
