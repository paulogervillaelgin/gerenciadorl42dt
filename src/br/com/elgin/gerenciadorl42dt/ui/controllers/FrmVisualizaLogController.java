/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.LogJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.model.Log;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmVisualizaLogController implements Initializable, Formulario {

    @FXML
    private TableView<Log> tbLog;

    @FXML
    private TableColumn<Log, Date> colData;

    @FXML
    private TableColumn<Log, Date> colHora;

    @FXML
    private TableColumn<Log, Integer> colTipo;

    @FXML
    private TableColumn<Log, String> colUsuario;

    @FXML
    private TableColumn<Log, String> colMensagem;

    @FXML
    private AnchorPane pnData;

    @FXML
    private JFXDatePicker dpInicio;

    @FXML
    private JFXDatePicker dpFinal;

    @FXML
    private RadioButton ckUsuario;

    @FXML
    private ToggleGroup tgTipoLog;

    @FXML
    private RadioButton ckTodos;

    @FXML
    private RadioButton ckSistema;

    @FXML
    private Button btProcurar;

    @FXML
    private TextField txtFiltro;

    @FXML
    private CheckBox ckIntervaloData;

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private TitledPane pnTipoLog;

    @FXML
    private TitledPane pnLoja;

    private EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    private LogJpaController daoLog = new LogJpaController(emf);

    private boolean servidor;

    private ObservableList<Log> obsLog = FXCollections.observableArrayList();
    private ObservableList<Log> obsFiltroLog = FXCollections.observableArrayList();

    private ExecutorService databaseExecutor = Executors.newFixedThreadPool(1);

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void onClose() {
        databaseExecutor.shutdownNow();
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        pnData.disableProperty().bind(ckIntervaloData.selectedProperty().not());

        colData.setCellValueFactory(new PropertyValueFactory<>("Date"));
        colHora.setCellValueFactory(new PropertyValueFactory<>("Date"));
        colTipo.setCellValueFactory(new PropertyValueFactory<>("Tipo"));
        colUsuario.setCellValueFactory(new PropertyValueFactory<>("Usuario"));
        colMensagem.setCellValueFactory(new PropertyValueFactory<>("Mensagem"));

        colData.setCellFactory(
                (TableColumn<Log, Date> param) -> {
                    TableCell<Log, Date> cell = new TableCell<Log, Date>() {
                @Override
                public void updateItem(Date item, boolean empty) {
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                        setOnMouseClicked(null);
                    } else {
                        if (item != null && getTableRow() != null) {
                            setAlignment(Pos.CENTER);

                            setText(new SimpleDateFormat("dd/MM/yyyy").format(item));
                        } else {
                            setText(null);
                            setGraphic(null);
                            setOnMouseClicked(null);
                        }
                    }
                }
            };
                    //System.out.println(cell.getIndex());
                    return cell;
                }
        );

        colHora.setCellFactory(
                (TableColumn<Log, Date> param) -> {
                    TableCell<Log, Date> cell = new TableCell<Log, Date>() {
                @Override
                public void updateItem(Date item, boolean empty) {
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                        setOnMouseClicked(null);
                    } else {
                        if (item != null && getTableRow() != null) {
                            setAlignment(Pos.CENTER);

                            setText(new SimpleDateFormat("HH:mm:ss").format(item));
                        } else {
                            setText(null);
                            setGraphic(null);
                            setOnMouseClicked(null);
                        }
                    }
                }
            };
                    //System.out.println(cell.getIndex());
                    return cell;
                }
        );

        colTipo.setCellFactory(
                (TableColumn<Log, Integer> param) -> {
                    TableCell<Log, Integer> cell = new TableCell<Log, Integer>() {
                @Override
                public void updateItem(Integer item, boolean empty) {
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                        setOnMouseClicked(null);
                    } else {
                        if (item != null && getTableRow() != null) {
                            setAlignment(Pos.CENTER);

                            switch (item) {
                                case Log.TIPO_SISTEMA:
                                    setText("Sistema");
                                    break;
                                case Log.TIPO_USUARIO:
                                    setText("Usuario");
                                    break;
                            }
                        }
                    }
                }
            };
                    //System.out.println(cell.getIndex());
                    return cell;
                });

        ////////////////////////////////////////////////////////////////////////
        updateTabela();

    }

    @FXML
    void handleBotaoProcurar(ActionEvent event) {
        if (verificaDatas()) {
            updateFiltroTabela();
        }
    }

    private Boolean verificaDatas() {
        Boolean aux = true;
        dpInicio.setEffect(null);
        dpFinal.setEffect(null);

        if (ckIntervaloData.isSelected()) {

            if (dpInicio.getValue() == null) {
                dpInicio.setEffect(Animacoes.getSombraErro());
                aux = false;
            }
            if (dpFinal.getValue() == null) {
                dpFinal.setEffect(Animacoes.getSombraErro());
                aux = false;
            }
            if (aux && dpInicio.getValue().compareTo(dpFinal.getValue()) >= 0) {
                dpFinal.setEffect(Animacoes.getSombraErro());
                Dialog.showError("Erro", "Data final tem de ser posterior a data inicial");
                aux = false;
            }

        }
        return aux;
    }

    private void updateTabela() {
        TaskBuscaDados task = new TaskBuscaDados();
        pnCarregando.setVisible(true);
        pnCarregando.toFront();
        task.setOnSucceeded((WorkerStateEvent event) -> {
            try {
                //workaround to update the table content
                colData.setVisible(false);
                colData.setVisible(true);

                List results = task.getValue();

                obsLog.clear();
                obsLog.addAll(results);

                tbLog.setItems(obsLog);
            } finally {
                pnCarregando.setVisible(false);
                pnCarregando.toBack();
            }
        });
        databaseExecutor.submit(task);
    }

    private void updateFiltroTabela() {
        obsFiltroLog.clear();
        TaskFiltraDados task = new TaskFiltraDados();
        pnCarregando.setVisible(true);
        pnCarregando.toFront();
        task.setOnSucceeded((WorkerStateEvent event) -> {
            try {
                tbLog.setItems(task.getValue());
            } finally {
                pnCarregando.setVisible(false);
                pnCarregando.toBack();
            }
        });
        databaseExecutor.submit(task);
    }

    private boolean matchesFilter(Log log) {
        Boolean aux = true;

        //verifica intervalo de data
        if (ckIntervaloData.isSelected()) {
            if (log.getDate() != null) {
                final Date inicio = Date.from(dpInicio.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
                final Date fim = Date.from(dpFinal.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

                if (log.getDate().compareTo(inicio) < 0 || log.getDate().compareTo(fim) > 0) {
                    aux = false;
                }
            }
        }

        //verifica o tipo do log
        if (aux) {
            if (tgTipoLog.getSelectedToggle() != ckTodos) {
                if (tgTipoLog.getSelectedToggle() == ckUsuario) {
                    aux = log.getTipo() == Log.TIPO_USUARIO;
                } else if (tgTipoLog.getSelectedToggle() == ckSistema) {
                    aux = log.getTipo() == Log.TIPO_SISTEMA;
                }
            }
        }

        //verifica se a mensagem contem o texto fornecido
        if (aux) {
            String filterString = txtFiltro.getText();
            if (filterString != null && !filterString.isEmpty()) {
                String lowerCaseFilterString = filterString.toLowerCase();

                if (log.getMensagem() != null && !log.getMensagem().toLowerCase().contains(lowerCaseFilterString)) {
                    aux = false;
                }
            }
        }
        return aux;
    }

    private class TaskBuscaDados extends Task<List<Log>> {

        @Override
        protected List<Log> call() throws Exception {
            return daoLog.findLogEntities();
        }

    }

    private class TaskFiltraDados extends Task<ObservableList<Log>> {

        @Override
        protected ObservableList<Log> call() throws Exception {
            ObservableList<Log> lista = FXCollections.observableArrayList();
            obsLog.stream().filter((log) -> (matchesFilter(log))).forEachOrdered((log) -> {
                lista.add(log);
            });
            return lista;
        }
    }

}
