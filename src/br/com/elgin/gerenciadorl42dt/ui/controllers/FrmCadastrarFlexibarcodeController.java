/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.model.Flexibarcode;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author Paulo
 */
public class FrmCadastrarFlexibarcodeController implements Initializable, Formulario {

    @FXML
    private TextField txtDescricao;

    @FXML
    private ComboBox<String> cbTipo;

    @FXML
    private ComboBox<String> cbFlag;

    @FXML
    private ComboBox<String> cbCodProd;

    @FXML
    private ComboBox<String> cbInfo1;

    @FXML
    private ComboBox<String> cbInfo2;

    @FXML
    private ComboBox<String> cbTamanhoInfo1;

    @FXML
    private ComboBox<String> cbTamanhoInfo2;

    @FXML
    private ComboBox<String> cbDigito;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private StackPane pnVisu;

    @FXML
    private Label lbMensagem;

    @FXML
    private Label lbTotal;

    private Flexibarcode flexibarcode;

    public Flexibarcode getFlexibarcode() {
        return flexibarcode;
    }

    public void setFlexibarcode(Flexibarcode flexibarcode) {
        this.flexibarcode = flexibarcode;
        if (flexibarcode != null) {
            exibeDados(flexibarcode);

        }
    }

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    private EventHandler<ActionEvent> handleVisualizacao = (event) -> {
        pnVisu.getChildren().clear();
        pnVisu.getChildren().add(Animacoes.getCodigoFlexbarcode(getFlex(), 48d));
        verificaCampos();
        lbTotal.setText(String.valueOf(getTamanhoCodigo()) + " Caracteres.");
    };

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        cbTipo.setItems(FXCollections.observableList(Flexibarcode.TIPOS));
        cbFlag.setItems(FXCollections.observableList(Flexibarcode.FLAGS));
        cbCodProd.setItems(FXCollections.observableList(Flexibarcode.CODIGO));
        cbInfo1.setItems(FXCollections.observableList(Flexibarcode.INFOS));
        cbTamanhoInfo1.setItems(FXCollections.observableList(Flexibarcode.TAMANHO_INFO));
        cbInfo2.setItems(FXCollections.observableList(Flexibarcode.INFOS));
        cbTamanhoInfo2.setItems(FXCollections.observableList(Flexibarcode.TAMANHO_INFO));
        cbDigito.setItems(FXCollections.observableList(Flexibarcode.CHECKDIGIT));

        cbTipo.setOnAction(handleVisualizacao);
        cbFlag.setOnAction(handleVisualizacao);
        cbCodProd.setOnAction(handleVisualizacao);
        cbInfo1.setOnAction(handleVisualizacao);
        cbTamanhoInfo1.setOnAction(handleVisualizacao);
        cbInfo2.setOnAction(handleVisualizacao);
        cbTamanhoInfo2.setOnAction(handleVisualizacao);
        cbDigito.setOnAction(handleVisualizacao);

    }

    private void exibeDados(Flexibarcode flexi) {
        if (flexi != null) {
            txtDescricao.setText(flexi.getDescricao());
            cbTipo.getSelectionModel().select(flexi.getTipo());
            cbFlag.getSelectionModel().select(flexi.getFlag());
            cbCodProd.getSelectionModel().select(flexi.getTamanhocodigo());
            cbInfo1.getSelectionModel().select(flexi.getInfo1());
            cbInfo2.getSelectionModel().select(flexi.getInfo2());
            cbTamanhoInfo1.getSelectionModel().select(flexi.getTamanhoinfo1());
            cbTamanhoInfo2.getSelectionModel().select(flexi.getTamanhoinfo2());
            cbDigito.getSelectionModel().select(flexi.getCheckdigit());
            handleVisualizacao.handle(new ActionEvent());
        }
    }

    private int getTamanhoCodigo() {
        int total = 0;
        if (pnVisu.getChildren().size() == 1) {
            HBox box = (HBox) pnVisu.getChildren().get(0);
            for (Node nodo : box.getChildren()) {
                if (nodo instanceof Label) {
                    total += ((Label) nodo).getText().length();
                }
            }
        }
        return total;
    }

    private boolean verificaCampos() {
        boolean res = true;

        txtDescricao.setEffect(null);
        lbMensagem.setText("");

        if (txtDescricao.getText().isEmpty()) {
            txtDescricao.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        int total = getTamanhoCodigo();

        if (cbTipo.getSelectionModel().getSelectedItem().equals("EAN")) {
            if (!(total == 8 || total == 13)) {
                res = false;
                lbMensagem.setText("Formato invalido ! O codigo de barrras do tipo EAN somente suporta formatos com 8 ou 13 caracteres.");
            }
        } else {
            if (!(total >= 4 && total <= 22 && total % 2 == 0)) {
                res = false;
                lbMensagem.setText("Formato invalido ! O codigo de barrras do tipo ITF somente suporta numeros pares entre 4 e 22 caracteres.");
            }
        }

        return res;
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        setFlexibarcode(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaCampos()) {
            flexibarcode.setDescricao(txtDescricao.getText());
            flexibarcode.setTipo(cbTipo.getSelectionModel().getSelectedIndex());
            flexibarcode.setFlag(cbFlag.getSelectionModel().getSelectedIndex());
            flexibarcode.setTamanhocodigo(cbCodProd.getSelectionModel().getSelectedIndex());
            flexibarcode.setInfo1(cbInfo1.getSelectionModel().getSelectedIndex());
            flexibarcode.setInfo2(cbInfo2.getSelectionModel().getSelectedIndex());
            flexibarcode.setTamanhoinfo1(cbTamanhoInfo1.getSelectionModel().getSelectedIndex());
            flexibarcode.setTamanhoinfo2(cbTamanhoInfo2.getSelectionModel().getSelectedIndex());
            flexibarcode.setCheckdigit(cbDigito.getSelectionModel().getSelectedIndex());

            FormHelper.getFormMenuController().desempilhaFormulario();
            onClose();
        }
    }

    private Flexibarcode getFlex() {

        Flexibarcode flex = new Flexibarcode();

        flex.setDescricao(txtDescricao.getText());
        flex.setTipo(cbTipo.getSelectionModel().getSelectedIndex());
        flex.setFlag(cbFlag.getSelectionModel().getSelectedIndex());
        flex.setTamanhocodigo(cbCodProd.getSelectionModel().getSelectedIndex());
        flex.setInfo1(cbInfo1.getSelectionModel().getSelectedIndex());
        flex.setInfo2(cbInfo2.getSelectionModel().getSelectedIndex());
        flex.setTamanhoinfo1(cbTamanhoInfo1.getSelectionModel().getSelectedIndex());
        flex.setTamanhoinfo2(cbTamanhoInfo2.getSelectionModel().getSelectedIndex());
        flex.setCheckdigit(cbDigito.getSelectionModel().getSelectedIndex());

        return flex;
    }

}
