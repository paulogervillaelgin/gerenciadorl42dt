/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.ImpressoraJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.ProdutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusEnum;
import br.com.elgin.gerenciadorl42dt.ImpressoraStatusManager;
import br.com.elgin.gerenciadorl42dt.comunicacao.ComandoLeitura;
import br.com.elgin.gerenciadorl42dt.comunicacao.Comunicacao;
import br.com.elgin.gerenciadorl42dt.comunicacao.TaskComunicacao;
import br.com.elgin.gerenciadorl42dt.model.Impressora;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import com.jfoenix.controls.JFXToggleButton;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmTransmissaoController implements Initializable {

    @FXML
    private Button btTransmitir;

    @FXML
    private Button btCancelar;

    @FXML
    private Label lbImpressoraDescricao;

    @FXML
    private Label lbImpressoraDepartamento;

    @FXML
    private Button btAtualizar;

    @FXML
    private VBox pnInformacoes;

    @FXML
    private AnchorPane pnProdutos;

    @FXML
    private JFXToggleButton ckProduto;

    @FXML
    private AnchorPane pnFormato;

    @FXML
    private JFXToggleButton ckFormatoEtiqueta;

    @FXML
    private AnchorPane pnDataHora;

    @FXML
    private JFXToggleButton ckDataHora;

    @FXML
    private CheckBox ckApagarArquivos;

    @FXML
    private AnchorPane pnCarregando;

    @FXML
    private ProgressIndicator piCarregando;

    @FXML
    private Label lbCarregandoStatus;

    @FXML
    private Circle ciEtiquetas;

    @FXML
    private Circle ciProdutos;
    
    @FXML
    private ProgressBar pbDetectandoImpressora;
    @FXML
    private AnchorPane pnDetectandoImpressora;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ImpressoraJpaController daoImpressora = new ImpressoraJpaController(emf);
    ProdutoJpaController daoProduto = new ProdutoJpaController(emf);

    private Impressora impressora;

    public Impressora getImpressora() {
        return impressora;
    }

    public void setImpressora(Impressora impressora) {
        this.impressora = impressora;
        if (impressora != null) {
            lbImpressoraDepartamento.setText("Departamento: " + impressora.getDepartamento().getDescricao());
            lbImpressoraDescricao.setText(impressora.getDescricao());
            if ((impressora.getFlagAtualizacao() & ImpressoraStatusEnum.LAYOUT.getValorStatus()) == ImpressoraStatusEnum.LAYOUT.getValorStatus()) {
                ciEtiquetas.setFill(Color.ORANGE);
            } else {
                ciEtiquetas.setFill(Color.GREEN);
            }
            if ((impressora.getFlagAtualizacao() & ImpressoraStatusEnum.PRODUTO.getValorStatus()) == ImpressoraStatusEnum.PRODUTO.getValorStatus()) {
                ciProdutos.setFill(Color.ORANGE);
            } else {
                ciProdutos.setFill(Color.GREEN);
            }
            ckDataHora.setSelected(true);
            ckProduto.setSelected(true);
            ckFormatoEtiqueta.setSelected(true);
            btTransmitir.setDisable(false);
        } else {
            ciProdutos.setFill(Color.GREY);
            ciEtiquetas.setFill(Color.GRAY);
            //lbImpressoraDepartamento.setText("");
            lbImpressoraDescricao.setText("Impressora não cadastrada");
            btTransmitir.setDisable(true);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pnCarregando.setVisible(false);
        pnCarregando.toBack();
        btTransmitir.setDisable(true);
        ciProdutos.setFill(Color.GREY);
        ciEtiquetas.setFill(Color.GRAY);

        btAtualizar.setDisable(true);
        pnDetectandoImpressora.setVisible(false);
        
        getImpressoraConectada();
    }

    private void getImpressoraConectada() {
        //pnCarregando.toFront();
        //pnCarregando.setVisible(true);
        TaskComunicacao task = Comunicacao.getTaskSerial();
        
        //lbCarregandoStatus.textProperty().bind(task.messageProperty());
        //piCarregando.progressProperty().bind(task.progressProperty());
        lbImpressoraDescricao.textProperty().bind(task.messageProperty());
        pbDetectandoImpressora.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        pnDetectandoImpressora.setVisible(true);
        btAtualizar.setDisable(true);
        
        task.setOnSucceeded((event) -> {
            

            pnCarregando.toBack();
            pnCarregando.setVisible(false);
            //piCarregando.progressProperty().unbind();
            //lbCarregandoStatus.textProperty().unbind();
            if (task.getValue()) {
                pnDetectandoImpressora.setVisible(false);
                btAtualizar.setDisable(false);
                lbImpressoraDescricao.textProperty().unbind();                
                
                String serial =  new String(((ComandoLeitura) task.getListaComandos().get(0)).getBufferLeitura(),Charset.forName("UTF-8")).replace("\r\n", "").replaceAll("\0+$", "");                

                lbImpressoraDepartamento.setText("Serial: "+serial);
                setImpressora(daoImpressora.findImpressoraBySerial(serial));
            } else {
                ciProdutos.setFill(Color.GREY);
                ciEtiquetas.setFill(Color.GRAY);
                
                pnDetectandoImpressora.setVisible(false);
                lbImpressoraDescricao.textProperty().unbind();
                btAtualizar.setDisable(false);
                lbImpressoraDescricao.setText("Impressora não conectada.");
                
                lbImpressoraDepartamento.setText("");
                btTransmitir.setDisable(true);
            }
        });
        Comunicacao.EXECUTOR.submit(task);
    }

    @FXML
    void handleBtAtualizar(ActionEvent event) {
        getImpressoraConectada();
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    //task que codifica os dados e gera uma lista de comandos a serem executados
    private class TaskGeraComandos extends Task<TaskComunicacao> {

        @Override
        protected TaskComunicacao call() throws Exception {
            return Comunicacao.getTaskTransferencia(impressora, ckApagarArquivos.isSelected(), ckFormatoEtiqueta.isSelected(), ckDataHora.isSelected());
        }
        
    }

    @FXML
    void handleBtTransmitir(ActionEvent event) {
        if (getImpressora() != null) {
            Dialog.buildConfirmation("Transmissão", "Deseja realmente efetuar essa transmissão ?").addYesButton((evento_yes) -> {
                if (daoProduto.countProdutoEntitiesByDepartamento(getImpressora().getDepartamento()) <= 1000) {
                    pnCarregando.setVisible(true);
                    pnCarregando.toFront();
                    lbCarregandoStatus.setText("Preparando dados...");
                    piCarregando.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
                    TaskGeraComandos task = new TaskGeraComandos();
                    piCarregando.progressProperty().bind(task.progressProperty());
                    task.setOnSucceeded((evento) -> {
                        if (task.getValue() != null) {

                            TaskComunicacao taskComunicacao = task.getValue();

                            try {
                                FXMLLoader fxmlLoader = new FXMLLoader();
                                fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_STATUS_TASK));
                                Parent root = (Parent) fxmlLoader.load();
                                FrmStatusTaskController controller = (FrmStatusTaskController) fxmlLoader.getController();

                                controller.setTitulo("Transmissão");
                                controller.setTaskExecutando(taskComunicacao);

                                controller.setOnClose((evento_controller) -> {
                                    pnCarregando.setVisible(false);
                                    pnCarregando.toBack();
                                    piCarregando.progressProperty().unbind();

                                    //Altera o status da impressora
                                    if (taskComunicacao.getValue()) {
                                        if (ckProduto.isSelected()) {
                                            ImpressoraStatusManager.updateStatusImpressora(impressora, ImpressoraStatusEnum.PRODUTO, true);
                                        }
                                        if (ckFormatoEtiqueta.isSelected()) {
                                            ImpressoraStatusManager.updateStatusImpressora(impressora, ImpressoraStatusEnum.LAYOUT, true);
                                        }
                                    } else {
                                        if (ckProduto.isSelected()) {
                                            ImpressoraStatusManager.updateStatusImpressora(impressora, ImpressoraStatusEnum.PRODUTO, false);
                                        }
                                        if (ckFormatoEtiqueta.isSelected()) {
                                            ImpressoraStatusManager.updateStatusImpressora(impressora, ImpressoraStatusEnum.LAYOUT, false);
                                        }
                                    }

                                    getImpressoraConectada();
                                });

                                Comunicacao.EXECUTOR.submit(taskComunicacao);

                                FormHelper.getFormMenuController().empilhaFormulario(root);
                            } catch (IOException ex) {
                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    new Thread(task).start();
                } else {
                    Dialog.showError("Erro", "Quantidade de produtos nesse departamento supera o limite(1000)");
                }
            }).addNoButton(null).build().show();
        }
    }

    @FXML
    void handleMouseInfos(MouseEvent event
    ) {
        if (event.getSource() instanceof Pane) {
            ((Pane) event.getSource()).getChildren().stream().filter((object) -> (object instanceof JFXToggleButton)).forEachOrdered((object) -> {
                ((JFXToggleButton) object).setSelected(!((JFXToggleButton) object).isSelected());
            });
        }
    }

}
