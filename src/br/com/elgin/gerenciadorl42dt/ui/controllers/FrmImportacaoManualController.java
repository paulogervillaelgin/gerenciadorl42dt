/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.ImportacaoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.importacao.FactoryImportacao;
import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmImportacaoManualController implements Initializable {

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private ComboBox<String> cbTipo;

    @FXML
    private TextField txtCaminho;

    @FXML
    private Button btProcurar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ImportacaoJpaController daoImportacao = new ImportacaoJpaController(emf);

    private Importacao importacao;

    public Importacao getImportacao() {
        return importacao;
    }

    public void setImportacao(Importacao importacao) {
        this.importacao = importacao;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        setImportacao(daoImportacao.findImportacao(1));

        cbTipo.setItems(FXCollections.observableArrayList(FactoryImportacao.LISTA_TIPOS_IMPORTACAO));
        cbTipo.getSelectionModel().select(importacao.getTipo());
        txtCaminho.setText(importacao.getEndereco());

        if (!Permissoes.verificarAdm()) {
            txtCaminho.setDisable(true);
            cbTipo.setDisable(true);
            btProcurar.setDisable(true);
            btConfirmar.setDisable(true);
        }

        Platform.runLater(() -> {
            txtCaminho.requestFocus();
        });
    }

    public boolean verificaDados() {
        boolean res = true;
        txtCaminho.setEffect(null);

        if (txtCaminho.getText() == null || txtCaminho.getText().isEmpty()) {
            txtCaminho.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        return res;
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaDados()) {

            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_STATUS_TASK));
                Parent root = (Parent) fxmlLoader.load();
                FrmStatusTaskController controller = (FrmStatusTaskController) fxmlLoader.getController();

                controller.setTitulo("Importação");
                //altera a importação com os dados da tela
                importacao.setEndereco(txtCaminho.getText());
                importacao.setTipo(cbTipo.getSelectionModel().getSelectedIndex());
                controller.setTask(FactoryImportacao.getImportacaoByType(importacao, SessaoAtual.getUsuario(), true));

                controller.setOnClose((evento_controller) -> {
                    //TODO salva somente se der certo ou sempre ?
                    if (controller.getTask().getValue()) {
                        try {
                            importacao.setEndereco(txtCaminho.getText());
                            importacao.setTipo(cbTipo.getSelectionModel().getSelectedIndex());
                            daoImportacao.edit(importacao);
                        } catch (Exception ex) {
                            Logger.getLogger(FrmImportacaoManualController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                FormHelper.getFormMenuController().empilhaFormulario(root);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    void handleBtProcurar(ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Selecione o diretorio dos arquivos para importação");
        if (txtCaminho.getText() != null && !txtCaminho.getText().isEmpty()) {
            directoryChooser.setInitialDirectory(new File(txtCaminho.getText()));
        }
        final File selectedDirectory = directoryChooser.showDialog(FormHelper.getFrmPrincipal().getStage());
        if (selectedDirectory != null) {
            txtCaminho.setText(selectedDirectory.getAbsolutePath());
        }
    }
}
