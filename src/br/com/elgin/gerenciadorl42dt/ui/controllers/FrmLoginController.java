/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.LojaJpaController;
import br.com.elgin.gerenciadorl42dt.DAO.UsuarioJpaController;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.utils.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 *
 */
public class FrmLoginController implements Initializable {

    @FXML
    private Label lbDescLoja;

    @FXML
    private Pane painel;

    @FXML
    private Button btLogin;

    @FXML
    private TextField txtUsuario;

    @FXML
    private PasswordField txtSenha;

    @FXML
    private Label lbMensagem;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    private final UsuarioJpaController usuarioADO = new UsuarioJpaController(emf);
    private final LojaJpaController daoLoja = new LojaJpaController(emf);

    @FXML
    void handleBtLogin(ActionEvent event) {
        //System.out.println(Utilidades.toSHA256(txtSenha.getText()));
        //* Paulo Gervilla - 1.3.3
        //Usuario user = usuarioADO.verificarSenha(txtUsuario.getText(), Utilidades.toSHA256(txtSenha.getText()));
        Usuario user=null;
        
        if (!Configuracao.isLoginAutomatico()) {
            user = usuarioADO.verificarSenha(txtUsuario.getText(), Utilidades.toSHA256(txtSenha.getText()));
        } else {
            user = new Usuario();
            user.setAdm(1);
            user.setNome("Sistema");
        }
        //*
        
        if (user != null) {

            SessaoAtual.setUsuario(user);
            //Deixando a loja como fixa
            SessaoAtual.setLoja(daoLoja.findLoja(1));

            FormHelper.gotoMenu();
        } else {
            lbMensagem.setVisible(true);
        }
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbMensagem.setVisible(false);
        if (!Configuracao.isMultiloja()) {
            //Se for modo local, exibir nome do software
            lbDescLoja.setText("Gerenciador L42DT");
        } else {
            lbDescLoja.setText(Configuracao.getDescLoja());
        }
        Platform.runLater(() -> {
            //* Paulo Gervilla - 1.3.5
            //txtUsuario.requestFocus();
            if (!Configuracao.isLoginAutomatico()) {
                txtUsuario.requestFocus();
            } else {
                handleBtLogin(null);
            }
            //*
        });
    }

}
