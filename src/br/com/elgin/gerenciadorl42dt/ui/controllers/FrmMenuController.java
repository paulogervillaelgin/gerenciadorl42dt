/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.importacao.ServiceImportacao;
import br.com.elgin.gerenciadorl42dt.model.Usuario;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Configuracao;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmMenuController implements Initializable {

    private Usuario usuario;

    private Timeline mostraPainelMenu;
    private Timeline extendePainelMenu;
    private Timeline ocultaPainelMenu;

    private Timeline mostraUserPanel;
    private Timeline ocultaUserPanel;

    @FXML
    private VBox pnMenu;

    @FXML
    private VBox pnMenuLateral;

    @FXML
    private BorderPane painel;

    @FXML
    private StackPane painelStack;

    @FXML
    private AnchorPane painelUser;

    @FXML
    private AnchorPane painelMenu;

    @FXML
    private Label lbUsuarioNome;

    @FXML
    private Label lbUsuarioTipo;

    @FXML
    private ImageView imgUsuarioTipo;

    @FXML
    private ImageView imgCadastroUsuario;

    @FXML
    private ImageView imgLogOff;

    @FXML
    private ImageView imgSair;

    @FXML
    private Label mnuMensagemPropaganda;

    @FXML
    private Label mnuCadastroTeclado;

    @FXML
    private Label mnuCadastroFlexibarcode;

    @FXML
    private Label mnuCadastroImagem;

    @FXML
    private Label mnuImportacaoAutomatica;

    @FXML
    private Label mnuImportacaoManual;

    @FXML
    private Label mnuVisualizarLog;

    @FXML
    private Label mnuPesagemUnitarios;

    @FXML
    private AnchorPane pnProgresso;

    @FXML
    private ProgressBar pbProgresso;

    @FXML
    private Label lbProgresso;

    @FXML
    private Label lbLoja;

    @FXML
    private Button btTrocaLoja;

    @FXML
    private StackPane pnConexaoPerdida;

    @FXML
    private StackPane pnVisualizaImagem;

    @FXML
    private ImageView imgImagem;
    
    //* Paulo Gervilla - 1.3.5
    @FXML
    private Label lblFlagLoginAutomatico;
    //*

    private volatile FrmJanelaController janelaAlteraSenha;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //* Paulo Gervilla - 1.3.5
        imgLogOff.setVisible(!Configuracao.isLoginAutomatico());
        imgCadastroUsuario.setVisible(imgLogOff.isVisible());
        lblFlagLoginAutomatico.setVisible(!imgLogOff.isVisible());
        //*

//////////////// Animacao UserPanel
        this.ocultaUserPanel = new Timeline();
        this.ocultaUserPanel.setAutoReverse(false);
        KeyValue kvUserH = new KeyValue(painelUser.prefHeightProperty(), 56, Interpolator.EASE_BOTH);
        KeyValue kvUserW = new KeyValue(painelUser.prefWidthProperty(), 56, Interpolator.EASE_BOTH);

        KeyFrame kfUser = new KeyFrame(Duration.millis(300), new KeyValue[]{kvUserH, kvUserW});
        ocultaUserPanel.getKeyFrames().add(kfUser);

        mostraUserPanel = new Timeline();
        mostraUserPanel.setAutoReverse(false);
        KeyValue kvUserMH = new KeyValue(painelUser.prefHeightProperty(), 90, Interpolator.EASE_BOTH);
        KeyValue kvUserMW = new KeyValue(painelUser.prefWidthProperty(), 200, Interpolator.EASE_BOTH);

        KeyFrame kfUserM = new KeyFrame(Duration.millis(100), new KeyValue[]{kvUserMH, kvUserMW});
        mostraUserPanel.getKeyFrames().add(kfUserM);

/////////////////////////// Animacao Painel Menu
        ocultaPainelMenu = new Timeline();
        ocultaPainelMenu.setAutoReverse(false);
        KeyValue kvMen = new KeyValue(painelMenu.prefHeightProperty(), 56, Interpolator.EASE_BOTH);
        KeyValue kvMenOcl = new KeyValue(painelMenu.prefWidthProperty(), 56, Interpolator.EASE_BOTH);
        KeyFrame kfMen = new KeyFrame(Duration.millis(300), new KeyValue[]{kvMen, kvMenOcl});
        ocultaPainelMenu.getKeyFrames().add(kfMen);

        extendePainelMenu = new Timeline();
        extendePainelMenu.setAutoReverse(false);
        KeyValue kvMen1ex = new KeyValue(painelMenu.prefWidthProperty(), 200, Interpolator.EASE_BOTH);
        KeyValue kvMen2ex = new KeyValue(painelMenu.prefHeightProperty(), 76 + ((pnMenu.getChildren().size()) * 35), Interpolator.EASE_BOTH);
        extendePainelMenu.setDelay(Duration.millis(200));
        KeyFrame kfMen1ex = new KeyFrame(Duration.millis(200), new KeyValue[]{kvMen1ex, kvMen2ex});
        extendePainelMenu.getKeyFrames().add(kfMen1ex);

        mostraPainelMenu = new Timeline();
        mostraPainelMenu.setAutoReverse(false);
        KeyValue kvMen1 = new KeyValue(painelMenu.prefWidthProperty(), 200, Interpolator.EASE_BOTH);
        KeyFrame kfMen1 = new KeyFrame(Duration.millis(100), new KeyValue[]{kvMen1});
        mostraPainelMenu.getKeyFrames().add(kfMen1);

//////////////////////
        painelUser.setOnMouseEntered((event) -> {
            ocultaUserPanel.stop();
            mostraUserPanel.play();
        });

        painelUser.setOnMouseExited((event) -> {
            mostraUserPanel.stop();
            ocultaUserPanel.play();
        });

        painelMenu.setOnMouseEntered((event) -> {
            ocultaPainelMenu.stop();
            mostraPainelMenu.play();
        });

        painelMenu.setOnMouseExited((event) -> {
            mostraPainelMenu.stop();
            extendePainelMenu.stop();
            ocultaPainelMenu.play();
        });

        mostraPainelMenu.setOnFinished((event) -> {
            extendePainelMenu.play();
        });

        mostraUserPanel.setOnFinished((event) -> {
            lbUsuarioTipo.setVisible(true);
            //* Paulo Gervilla - 1.3.5
            lblFlagLoginAutomatico.setVisible(Configuracao.isLoginAutomaticoTmp());
            //*
        });

        ocultaUserPanel.setOnFinished((event) -> {
            lbUsuarioTipo.setVisible(false);
            //* Paulo Gervilla - 1.3.5
            lblFlagLoginAutomatico.setVisible(false);
            //*
        });

        imgSair.setOnMouseClicked((event) -> {
            if (FormHelper.isTravado()) {
                Dialog.showInfo("Aguarde!", "Aguarde a rotina atual finalizar.");
                return;
            }
            if (mostraUserPanel.getStatus() != Animation.Status.RUNNING) {
                Dialog.buildConfirmation("LabelNet", "Deseja realmente sair do sistema ?").addYesButton((evento) -> {
                    FormHelper.closeSystem();
                }).addNoButton(null).build().show();
            }
        });

        imgLogOff.setOnMouseClicked((event) -> {
            if (FormHelper.isTravado()) {
                Dialog.showInfo("Aguarde!", "Aguarde a rotina atual finalizar.");
                return;
            }
            if (mostraUserPanel.getStatus() != Animation.Status.RUNNING) {
                Dialog.buildConfirmation("LabelNet", "Deseja efetuar logoff desse usuario ?").addYesButton((evento) -> {
                    //* Paulo Gervilla - 1.3.3
                    Configuracao.setLoginautomaticoTmp(Configuracao.isLoginAutomatico());
                    //*
                    FormHelper.logoff();
                }).addNoButton(null).build().show();
            }
        }
        );

        imgCadastroUsuario.setOnMouseClicked((event) -> {
            if (mostraUserPanel.getStatus() != Animation.Status.RUNNING) {
                if (SessaoAtual.getUsuario().getAdm() == Usuario.USUARIO_TIPO_ADM) {
                    FormHelper.carregaFormulario(FormHelper.FORM_CADASTRO_USUARIOS);
                } else {
                    try {
                        FXMLLoader fxmlLoader = new FXMLLoader();
                        fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_ALTERA_SENHA));
                        Parent root = (Parent) fxmlLoader.load();
                        empilhaFormulario(root);
                    } catch (IOException ex) {
                        Logger.getLogger(FrmMenuController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        trataUsuarioLogado();

        if (!Configuracao.isServidor()) {
            pnMenuLateral.getChildren().remove(btTrocaLoja);
        }

        ocultaPainelMenu.play();
        ocultaUserPanel.play();
        closeVisualizaImagem();
        pnVisualizaImagem.setOnMouseClicked((event) -> {
            closeVisualizaImagem();
        });
        imgImagem.setOnMouseClicked((event) -> {
            closeVisualizaImagem();
        });

    }

    public void closeVisualizaImagem() {
        pnVisualizaImagem.setVisible(false);
        pnVisualizaImagem.toBack();
    }

    public void visualizaImagem(Image imagem) {
        imgImagem.setFitWidth(imagem.getWidth());
        imgImagem.setFitHeight(imagem.getHeight());
        imgImagem.setImage(imagem);
        pnVisualizaImagem.toFront();
        pnVisualizaImagem.setVisible(true);
    }

    public void setBarraMenuVisible(boolean bool) {
        if (bool) {
            mostraPainelMenu.play();
        } else {
            ocultaPainelMenu.play();
        }
    }

    public void showConexaoPerdida(boolean show) {
        if (show) {
            pnConexaoPerdida.toFront();
        } else {
            pnConexaoPerdida.toBack();
        }
        pnConexaoPerdida.setVisible(show);
    }

    @FXML
    void handleConsultaDepartamento(ActionEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_CONSULTA_DEPARTAMENTO);
    }

    @FXML
    void handleCadastroProduto(ActionEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_CONSULTA_PRODUTO);
    }

    @FXML
    void handleMnuMonitoramento(ActionEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_MONITORAMENTO);
    }

    @FXML
    void handleMnuConfigLoja(ActionEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_CONFIG_LOJA);
    }

    @FXML
    void handleMnuTransmissao(ActionEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_TRANSMISSAO);
    }

    public void empilhaFormulario(Node nodo) {
        painelStack.getChildren().stream().map((node) -> {
            node.setDisable(true);
            return node;
        }).forEachOrdered((node) -> {
            node.setOpacity(0.5);
        });
        painelStack.getChildren().add(nodo);
    }

    public void desempilhaFormulario() {
        painelStack.getChildren().remove(painelStack.getChildren().size() - 1);
        if (painelStack.getChildren().size() >= 1) {
            Node node = painelStack.getChildren().get(painelStack.getChildren().size() - 1);
            node.setOpacity(1);
            node.setDisable(false);
        }
    }

    public void exibeFormulario(Node nodo) {

        if (painelStack.getChildren().size() > 0) {
            SequentialTransition pt = new SequentialTransition();
            for (int i = painelStack.getChildren().size() - 1; i >= 0; i--) {
                Node node = painelStack.getChildren().get(i);
                pt.getChildren().add(Animacoes.animateFadeOUT((Pane) node));
            }
            pt.setOnFinished((event) -> {
                animateNodeIn(nodo);
            });
            pt.play();
        } else {
            animateNodeIn(nodo);
        }

        //setLateralVisible(false);
    }

    private void animateNodeIn(Node nodo) {
        painelStack.getChildren().clear();
        painelStack.getChildren().add(nodo);
        //Animation animacao_in = Animacoes.animateTranslateX((Pane) nodo, Duration.millis(200), Duration.ZERO, Animacoes.DirecaoAnimacao.IN);
        Animation animacao_in = Animacoes.animateFadeIN((Pane) nodo);
        animacao_in.play();
    }

    public void showPainelProgress() {
        Animacoes.animateNodeWidth(pnProgresso, 350);
    }

    public void bindTaskProgress() {
        lbProgresso.textProperty().bind(ServiceImportacao.getInstance().messageProperty());
        pbProgresso.progressProperty().bind(ServiceImportacao.getInstance().progressProperty());
        ServiceImportacao.getInstance().stateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(State.RUNNING)) {
                pbProgresso.setStyle("-fx-accent:-cor-base;");
                lbProgresso.textProperty().bind(ServiceImportacao.getInstance().messageProperty());
                pbProgresso.progressProperty().bind(ServiceImportacao.getInstance().progressProperty());
            } else if (newValue.equals(State.READY)) {
                lbProgresso.textProperty().unbind();
                pbProgresso.progressProperty().unbind();
                if (ServiceImportacao.getInstance().getLastValue() != null) {
                    if (ServiceImportacao.getInstance().getLastValue()) {
                        lbProgresso.setText("Importação realizada com sucesso!");
                        pbProgresso.setStyle("-fx-accent:green;");
                        pbProgresso.setProgress(1);
                    } else {
                        lbProgresso.setText("Ocorreu um erro na importação!");
                        pbProgresso.setStyle("-fx-accent:red;");
                        pbProgresso.setProgress(1);
                    }
                    Animacoes.animateNodeWidth(pnProgresso, 0, Duration.seconds(4));
                } else {
                    Animacoes.animateNodeWidth(pnProgresso, 0);
                }
            }
        });
    }

    public void trataUsuarioLogado() {
        usuario = SessaoAtual.getUsuario();
        if (usuario != null) {
            imgUsuarioTipo.getStyleClass().clear();
            if (usuario.getAdm() == Usuario.USUARIO_TIPO_ADM) {
                lbUsuarioTipo.setText("Administrador");
                imgUsuarioTipo.setImage(SessaoAtual.USUARIO_IMAGEM_ADMINISTRADOR);
                imgUsuarioTipo.getStyleClass().add("sombra_yellow");
            } else {
                lbUsuarioTipo.setText("Operador");
                imgUsuarioTipo.setImage(SessaoAtual.USUARIO_IMAGEM_OPERADOR);
            }
            lbUsuarioNome.setText(usuario.getNome());
        }
    }

    @FXML
    void handleMnuCadastroFlexibarcode(MouseEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_CONSULTA_FLEXIBARCODE);
    }

    @FXML
    void handleMnuConsultaEtiqueta(MouseEvent event) {
        //FormHelper.carregaFormulario(FormHelper.FORM_CONSULTA_ETIQUETA);
    }

    @FXML
    void handleMnuImportacaoAutomatica(MouseEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_IMPORTACAO_AUTOMATICA);
    }

    @FXML
    void handleMnuImportacaoManual(MouseEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_IMPORTACAO_MANUAL);
    }

    @FXML
    void handleMnuVisualizarLog(MouseEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_VISUALIZA_LOG);
    }

    @FXML
    void handleMnuPesagemUnitarios(MouseEvent event) {
        FormHelper.carregaFormulario(FormHelper.FORM_PESAGEM_UNITARIOS);
    }

}
