/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.DAO.ImportacaoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.Permissoes;
import br.com.elgin.gerenciadorl42dt.importacao.FactoryImportacao;
import br.com.elgin.gerenciadorl42dt.importacao.ServiceImportacao;
import br.com.elgin.gerenciadorl42dt.model.Importacao;
import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.utils.Dialog;
import com.sun.javafx.PlatformUtil;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmConfigImportacaoAutomaticaController implements Initializable {

    @FXML
    private Label lbTitulo;

    @FXML
    private ComboBox<String> cbTipo;

    @FXML
    private TextField txtCaminho;

    @FXML
    private Button btProcurar;

    @FXML
    private CheckBox ckLimparBase;

    @FXML
    private CheckBox ckUppercase;

    @FXML
    private AnchorPane pnPreImportacao;

    @FXML
    private RadioButton rbAlteracao;

    @FXML
    private ToggleGroup tgPreImportacao;

    @FXML
    private RadioButton rbArquivo;

    @FXML
    private Pane pnPreInterno;

    @FXML
    private TextField txtArquivoPre;

    @FXML
    private Button btProcurarPre;

    @FXML
    private AnchorPane pnExecPrograma;

    @FXML
    private CheckBox ckExecPrograma;

    @FXML
    private Pane pnProgramaInterno;

    @FXML
    private TextField txtPrograma;

    @FXML
    private Button btProcurarPrograma;

    @FXML
    private TextField txtParametros;

    @FXML
    private AnchorPane pnPosImportacao;

    @FXML
    private RadioButton rbRenomear;

    @FXML
    private ToggleGroup tgPosImportacao;

    @FXML
    private RadioButton rbMover;

    @FXML
    private CheckBox ckPosImportacao;

    @FXML
    private CheckBox ckPreImportacao;

    @FXML
    private Pane pnExecPosInterno;

    @FXML
    private Pane pnExecPreInterno;

    @FXML
    private Pane pnPosInterno;

    @FXML
    private TextField txtLocalPos;

    @FXML
    private Button btProcurarPos;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    ImportacaoJpaController daoImportacao = new ImportacaoJpaController(emf);

    private Importacao importacao;

    public Importacao getImportacao() {
        return importacao;
    }

    public void setImportacao(Importacao importacao) {
        this.importacao = importacao;
    }

    private boolean verificaDados() {
        boolean res = true;

        txtCaminho.setEffect(null);
        txtArquivoPre.setEffect(null);
        txtPrograma.setEffect(null);
        txtLocalPos.setEffect(null);

        if (txtCaminho.getText() == null || txtCaminho.getText().isEmpty()) {
            txtCaminho.setEffect(Animacoes.getSombraErro());
            res = false;
        }

        if (ckPreImportacao.isSelected()) {
            if (tgPreImportacao.getSelectedToggle().equals(rbArquivo)) {
                if (txtArquivoPre.getText() == null || txtArquivoPre.getText().isEmpty()) {
                    txtArquivoPre.setEffect(Animacoes.getSombraErro());
                    res = false;
                }
            }
        }

        if (ckExecPrograma.isSelected()) {
            if (txtPrograma.getText() == null || txtPrograma.getText().isEmpty()) {
                txtPrograma.setEffect(Animacoes.getSombraErro());
                res = false;
            }
        }

        if (ckPosImportacao.isSelected()) {
            if (tgPosImportacao.getSelectedToggle().equals(rbMover)) {
                if (txtLocalPos.getText() == null || txtLocalPos.getText().isEmpty()) {
                    txtLocalPos.setEffect(Animacoes.getSombraErro());
                    res = false;
                }
            }
        }

        return res;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ///TODO verificar o que fazer com a tabela com um so registro de importação
        setImportacao(daoImportacao.findImportacao(1));

        cbTipo.setItems(FXCollections.observableArrayList(FactoryImportacao.LISTA_TIPOS_IMPORTACAO));

        ckPreImportacao.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                pnExecPreInterno.setVisible(false);
                pnPreInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnPreImportacao, 45d);
            } else {
                pnExecPreInterno.setVisible(true);
                if (tgPreImportacao.getSelectedToggle().equals(rbAlteracao)) {
                    pnPreInterno.setVisible(false);
                    Animacoes.animateNodeHeight(pnPreImportacao, 75d);
                } else {
                    pnPreInterno.setVisible(true);
                    Animacoes.animateNodeHeight(pnPreImportacao, 128d);
                }
            }
        });

        tgPreImportacao.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(rbAlteracao)) {
                pnPreInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnPreImportacao, 75d);
            } else {
                pnPreInterno.setVisible(true);
                Animacoes.animateNodeHeight(pnPreImportacao, 128d);
            }
        });

        ckExecPrograma.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                pnProgramaInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnExecPrograma, 45d);
            } else {
                pnProgramaInterno.setVisible(true);
                Animacoes.animateNodeHeight(pnExecPrograma, 136d);
            }
        });

        ckPosImportacao.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                pnExecPosInterno.setVisible(false);
                pnPosInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnPosImportacao, 45d);
            } else {
                pnExecPosInterno.setVisible(true);
                if (tgPosImportacao.getSelectedToggle().equals(rbRenomear)) {
                    pnPosInterno.setVisible(false);
                    Animacoes.animateNodeHeight(pnPosImportacao, 75d);
                } else {
                    pnPosInterno.setVisible(true);
                    Animacoes.animateNodeHeight(pnPosImportacao, 118d);
                }
            }
        });

        tgPosImportacao.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(rbRenomear)) {
                pnPosInterno.setVisible(false);
                Animacoes.animateNodeHeight(pnPosImportacao, 75d);
            } else {
                pnPosInterno.setVisible(true);
                Animacoes.animateNodeHeight(pnPosImportacao, 118d);
            }
        });

        carregarDados();

        Platform.runLater(() -> {
            txtCaminho.requestFocus();
        });

    }

    private void carregarDados() {

        if (importacao != null) {

            ckLimparBase.setSelected(importacao.isLimparBase());
            ckUppercase.setSelected(importacao.isToUpperCase());

            //TODO separar configuracao da importação manual da automatica ?
            cbTipo.getSelectionModel().select(importacao.getTipo());
            txtCaminho.setText(importacao.getEndereco());

            //para forçar uma atualizacao do valor :
            ckPreImportacao.setSelected(!importacao.isAutomatica());
            ckPreImportacao.setSelected(importacao.isAutomatica());

            tgPreImportacao.getToggles().get(importacao.getTipoPre()).setSelected(true);
            txtArquivoPre.setText(importacao.getPreArquivo());

            ckExecPrograma.setSelected(importacao.isExecPrograma());
            txtPrograma.setText(importacao.getPrograma());
            txtParametros.setText(importacao.getParametros());

            tgPosImportacao.getToggles().get(importacao.getTipoPos()).setSelected(true);
            txtLocalPos.setText(importacao.getPosDiretorio());

            //para forçar uma atualizacao do valor :
            ckPosImportacao.setSelected(!importacao.isExecPos());
            ckPosImportacao.setSelected(importacao.isExecPos());
        }
        if (!Permissoes.verificarAdm()) {
            btConfirmar.setDisable(true);
            btProcurar.setDisable(true);
            btProcurarPos.setDisable(true);
            btProcurarPre.setDisable(true);
            btProcurarPrograma.setDisable(true);
            pnPreImportacao.setDisable(true);
            pnPosImportacao.setDisable(true);
            pnExecPrograma.setDisable(true);
            cbTipo.setDisable(true);
            ckLimparBase.setDisable(true);
            ckUppercase.setDisable(true);
        }
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        if (verificaDados()) {
            try {
                importacao.setEndereco(txtCaminho.getText());
                importacao.setTipo(cbTipo.getSelectionModel().getSelectedIndex());

                importacao.setLimparBase(ckLimparBase.isSelected());
                importacao.setTouppercase(ckUppercase.isSelected());

                importacao.setAutomatica(ckPreImportacao.isSelected());
                importacao.setTipoPre(tgPreImportacao.getToggles().indexOf(tgPreImportacao.getSelectedToggle()));
                importacao.setPreArquivo(txtArquivoPre.getText());

                importacao.setExecPrograma(ckExecPrograma.isSelected());
                importacao.setPrograma(txtPrograma.getText());
                importacao.setParametros(txtParametros.getText());

                importacao.setExecPos(ckPosImportacao.isSelected());
                importacao.setTipoPos(tgPosImportacao.getToggles().indexOf(tgPosImportacao.getSelectedToggle()));
                importacao.setPosDiretorio(txtLocalPos.getText());

                daoImportacao.edit(importacao);

                ServiceImportacao.getInstance().setImportacao(importacao);

                /*
                //TODO quando alterar o servico para buscar sempre o valor no banco, nao vai mais ser necessario esse codigo :               
                //TODO verificar casos onde a importacao muda o valor do automatico para true e para false
                TransmissaoAutomatica.carregarServicos();
                ImportacaoAutomatica.carregarServicos();
                FormHelper.getFormMenuController().trataComponenteImportacaoAutomatica();
                 */
                Dialog.showInfo("Concluído", "Informações salvas com sucesso.");

                /*
                Dialog.buildConfirmation("Importação", "Deseja executar uma importação agora ?").addYesButton((evento) -> {
                    try {
                        FXMLLoader fxmlLoader = new FXMLLoader();
                        fxmlLoader.setLocation(FormHelper.class.getResource(FormHelper.FORM_STATUS_IMPORTACAO));
                        Parent root = (Parent) fxmlLoader.load();
                        FrmStatusImportacaoController controller = (FrmStatusImportacaoController) fxmlLoader.getController();

                        controller.setTask(FactoryImportacao.getImportacaoByType(importacao.getImportacaoTipo(), new File(Configuracao.getImportacaoCaminho()), true));

                        FormHelper.getFormMenuController().empilhaFormulario(root);

                    } catch (IOException ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                    }
                }).addNoButton(null).build().show();
                 */
                FormHelper.getFormMenuController().desempilhaFormulario();
            } catch (Exception e) {
                Dialog.showInfo("Erro", "Erro ao salvar informações.");
            }
        }
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        FormHelper.getFormMenuController().desempilhaFormulario();
    }

    @FXML
    void handleBtProcurar(ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Selecione o diretorio dos arquivos para importação");
        if (txtCaminho.getText() != null && !txtCaminho.getText().isEmpty()) {
            directoryChooser.setInitialDirectory(new File(txtCaminho.getText()));
        }
        final File selectedDirectory = directoryChooser.showDialog(FormHelper.getFrmPrincipal().getStage());
        if (selectedDirectory != null) {
            txtCaminho.setText(selectedDirectory.getAbsolutePath());
        }
    }

    @FXML
    void handleBtProcurarPos(ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Selecione o diretorio para mover os arquivos após a importação");
        if (txtLocalPos.getText() != null && !txtLocalPos.getText().isEmpty()) {
            directoryChooser.setInitialDirectory(new File(txtLocalPos.getText()));
        }
        final File selectedDirectory = directoryChooser.showDialog(FormHelper.getFrmPrincipal().getStage());
        if (selectedDirectory != null) {
            txtLocalPos.setText(selectedDirectory.getAbsolutePath());
        }
    }

    @FXML
    void handleBtProcurarPre(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecione o arquivo que deseja monitorar");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Todos arquivos", "*.*"));
        if (txtArquivoPre.getText() != null && !txtArquivoPre.getText().isEmpty()) {
            fileChooser.setInitialDirectory(new File(txtArquivoPre.getText()).getParentFile());
        }
        final File selectedFile = fileChooser.showOpenDialog(FormHelper.getFrmPrincipal().getStage());
        if (selectedFile != null) {
            txtArquivoPre.setText(selectedFile.getAbsolutePath());
        }
    }

    @FXML
    void handleBtProcurarPrograma(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecione programa que deseja executar antes da importação");
        if (PlatformUtil.isWindows()) {
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Executáveis", "*.exe"));
        }
        if (txtPrograma.getText() != null && !txtPrograma.getText().isEmpty()) {
            fileChooser.setInitialDirectory(new File(txtPrograma.getText()).getParentFile());
        }
        final File selectedFile = fileChooser.showOpenDialog(FormHelper.getFrmPrincipal().getStage());
        if (selectedFile != null) {
            try {

                //TODO testar tipo de arquivos no LINUX
                //System.out.println(Files.probeContentType(selectedFile.toPath()));
                if (Files.isExecutable(selectedFile.toPath())) {
                    txtPrograma.setText(selectedFile.getAbsolutePath());
                } else {
                    Dialog.showError("Erro", "O arquivo selecionado não é uma aplicação ou não possui permissão de execução.");
                }
            } catch (Exception e) {
                Dialog.showError("Erro", "O arquivo selecionado não é uma aplicação ou não possui permissão de execução.");
            }
        }
    }

}
