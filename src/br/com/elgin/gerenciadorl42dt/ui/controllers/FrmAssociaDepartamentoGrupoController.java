/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;


import br.com.elgin.gerenciadorl42dt.DAO.GrupoprodutoJpaController;
import br.com.elgin.gerenciadorl42dt.GerenciadorBaseDados;
import br.com.elgin.gerenciadorl42dt.SessaoAtual;
import br.com.elgin.gerenciadorl42dt.model.Grupoproduto;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import br.com.elgin.gerenciadorl42dt.ui.ItemSelecionavel;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javax.persistence.EntityManagerFactory;

/**
 * FXML Controller class
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class FrmAssociaDepartamentoGrupoController implements Initializable, Formulario {

    @FXML
    private FlowPane pnGrupos;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    @FXML
    private AnchorPane pnTodas;

    @FXML
    private AnchorPane pnLimpar;

    EntityManagerFactory emf = GerenciadorBaseDados.getEntityManagerFactory();
    GrupoprodutoJpaController daoGrupoproduto = new GrupoprodutoJpaController(emf);

    private Set<Grupoproduto> selecionados;

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    public Set<Grupoproduto> getSelecionados() {
        return selecionados;
    }

    public void setSelecionados(Set<Grupoproduto> selecionados) {
        this.selecionados = selecionados;
        carregaGrupos();
    }

    //altera o estado dos componentes quando sao clicados
    private final EventHandler<MouseEvent> handleItemClicked = (MouseEvent event) -> {
        if (event.getSource() instanceof ItemSelecionavel) {
            ItemSelecionavel item = (ItemSelecionavel) event.getSource();
            item.setSelecionado(!item.isSelecionado());
        }
    };

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        carregaGrupos();
        
        pnTodas.setOnMouseClicked((event) -> {
            setAllGrupos(true);
        });
        
        pnLimpar.setOnMouseClicked((event) -> {
            setAllGrupos(false);
        });
        
    }

    private void carregaGrupos() {
        pnGrupos.getChildren().clear();
        for (Grupoproduto grupo : daoGrupoproduto.findGrupoprodutoEntities()) {
            ItemSelecionavel<Grupoproduto> item = new ItemSelecionavel<>(grupo, grupo.getDescricao());
            if (getSelecionados() != null && getSelecionados().contains(grupo)) {
                item.setSelecionado(true);
            }
            item.setOnMouseClicked(handleItemClicked);
            pnGrupos.getChildren().add(item);
        }
    }

    //função que altera todos os estados dos componentes 
    private void setAllGrupos(boolean valor) {
        for (Node nodo : pnGrupos.getChildren()) {
            if (nodo instanceof ItemSelecionavel) {
                ItemSelecionavel<Grupoproduto> item = (ItemSelecionavel) nodo;
                item.setSelecionado(valor);                
            }
        }
    }
    
    private LinkedHashSet<Grupoproduto> updateGruposSelecionados() {
        LinkedHashSet<Grupoproduto> lista = new LinkedHashSet<>();
        for (Node nodo : pnGrupos.getChildren()) {
            if (nodo instanceof ItemSelecionavel) {
                ItemSelecionavel<Grupoproduto> item = (ItemSelecionavel) nodo;
                if (item.isSelecionado()) {
                    lista.add(item.getObjeto());
                }
            }
        }
        return lista;
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        // enviar as selecionadas
        setSelecionados(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        setSelecionados(updateGruposSelecionados());
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

}
