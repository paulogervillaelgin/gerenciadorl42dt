/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui.controllers;

import br.com.elgin.gerenciadorl42dt.ui.Animacoes;
import br.com.elgin.gerenciadorl42dt.ui.FormHelper;
import br.com.elgin.gerenciadorl42dt.ui.Formulario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Paulo
 */
public class FrmCadastroGrupoProdutoController implements Initializable, Formulario {

    @FXML
    private TextField txtDescricao;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btCancelar;

    private String Descricao;

    private EventHandler<ActionEvent> onClose;

    public EventHandler<ActionEvent> getOnClose() {
        return onClose;
    }

    public void setOnClose(EventHandler<ActionEvent> onClose) {
        this.onClose = onClose;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
        txtDescricao.setText(Descricao);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
            txtDescricao.requestFocus();
        });
    }

    @Override
    public void onClose() {
        if (getOnClose() != null) {
            onClose.handle(new ActionEvent());
        }
    }

    @FXML
    void handleBtCancelar(ActionEvent event) {
        setDescricao(null);
        FormHelper.getFormMenuController().desempilhaFormulario();
        onClose();
    }

    @FXML
    void handleBtConfirmar(ActionEvent event) {
        txtDescricao.setEffect(null);
        if (txtDescricao.getText() != null && !txtDescricao.getText().isEmpty()) {
            setDescricao(txtDescricao.getText());
            FormHelper.getFormMenuController().desempilhaFormulario();
            onClose();
        } else {
            txtDescricao.setEffect(Animacoes.getSombraErro());
        }
    }

}
