/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import br.com.elgin.gerenciadorl42dt.ui.controllers.FrmJanelaController;
import java.io.IOException;
import java.util.Properties;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 *
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public abstract class AbstractWizard {

    private boolean done;
    private Stage stage;
    private final FrmJanelaController janela;
    private Properties valores = new Properties();
    Stack<String> pilha = new Stack();

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    
    
    public Properties getValores() {
        return valores;
    }

    public void setValores(Properties valores) {
        this.valores = valores;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public AbstractWizard(Stage _stage, String titulo) {
        stage = _stage;
        janela = FormHelper.createJanelaController(stage);
        janela.setTitulo(titulo);
        done=false;
    }

    public void loadFromStack() {
        pilha.pop();
        loadFxml(pilha.pop());
    }

    public void loadFxml(String fxml) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(FormHelper.class.getResource(fxml));
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Node root = (Node) fxmlLoader.load(fxmlLoader.getLocation().openStream());
            show(root);
            pilha.push(fxml);
            FormularioWizard controller = fxmlLoader.getController();
            controller.setWizard(this);
            controller.setValores(getValores());
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro", e);
        }
    }

    public void show(Node nodo) {
        janela.setRoot(nodo);
    }

    public FrmJanelaController getJanela() {
        return janela;
    }

}
