/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.elgin.gerenciadorl42dt.ui;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Paulo Cesar <paulo.junior at elgin.com.br>
 */
public class ItemSimplesFlow extends AnchorPane {

    private final Label lbDescricao = new Label();

    public ItemSimplesFlow(String descricao) {

        getStyleClass().add("frame_radius_all");
        getStyleClass().add("item");
        getStyleClass().add("item_drag_borderless");
        
        /*
        setStyle("-fx-border-color:-cor-base;"
                + "-fx-border-radius:15;");
        */
        setVisible(true);

        lbDescricao.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        //lbDescricao.setTextFill(Color.WHITE);
        lbDescricao.setStyle("-fx-text-fill:-cor-base;");
        lbDescricao.setText(descricao);
        AnchorPane.setLeftAnchor(lbDescricao, 10.0);
        AnchorPane.setRightAnchor(lbDescricao, 10.0);
        AnchorPane.setTopAnchor(lbDescricao, 5.0);
        AnchorPane.setBottomAnchor(lbDescricao, 5.0);
        lbDescricao.setVisible(true);

        getChildren().add(lbDescricao);

    }

}
