/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.elgin.gerenciadorl42dt.ui;

import java.util.Properties;

/**
 * 
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public abstract class FormularioWizard {

    private AbstractWizard wizard;
    
    private Properties valores; 

    public Properties getValores() {
        return valores;
    }

    public AbstractWizard getWizard() {
        return wizard;
    }

    public void setWizard(AbstractWizard wizard) {
        this.wizard = wizard;
    }
    
    public void setValores(Properties valores) {
        this.valores = valores;
    }
    
    public abstract boolean verificaDados();
    
    public abstract void Voltar();
    
    public abstract void Avancar();
    
}
