/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.elgin.gerenciadorl42dt;

/**
 * 
 * @author Paulo Cesar Gomes Junior <paulo.junior at elgin.com.br>
 */
public class ConexaoBDCliente extends AbstractConexaoBD {

    public ConexaoBDCliente(String _caminho) {
        super(_caminho);
    }

    @Override
    public String getDriverString() {
        return "org.apache.derby.jdbc.ClientDriver";
    }
                
    @Override
    public String getURLString() {
        return "jdbc:derby:"+caminho+"GerenciadorL42DT;user=elgin;password=123;create=true";
    }



}
